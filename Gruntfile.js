
module.exports = function (grunt) {

    /**
     * This is the project's master Gruntfile, which is responsible only for running
     * a local dev server, doing unit tests and triggering other Gruntfiles using 'grunt-hub'.
     * If you simply write `grunt` on the command-line, it will also write out a list of possible commands.
     */

	
    // main build
    grunt.registerTask("build", [ "js", "css", "misc" ]);
    grunt.registerTask("b", [ "build" ]);


    // update project dependencies
    grunt.registerTask("update",[ "shell:grunt_update", "shell:bower_update" ]);
	grunt.registerTask("up",    [ "update" ]);
    

    // js options
    grunt.registerTask("js", [ "hub:rjs" ]);
    

    // css options
    grunt.registerTask("css",       [ "hub:sass.both" ]);
    grunt.registerTask("css.debug", [ "hub:sass.debug" ]);
    grunt.registerTask("css.min",   [ "hub:sass.min" ]);
    grunt.registerTask("css.debugapp", [ "hub:sass.debugapp" ]);

    grunt.registerTask("css.watch",       [ "hub:sass.wboth" ]);
    grunt.registerTask("css.debug.watch", [ "hub:sass.wbug" ]);
    grunt.registerTask("css.min.watch",   [ "hub:sass.wmin" ]);
    grunt.registerTask("css.debugapp.watch", [ "hub:sass.wdebugapp" ]);


    // svg tasks
    grunt.registerTask("svg",  [ "hub:svg" ]);


    // other tasks
    grunt.registerTask("misc",  [ "hub:misc" ]);


    // local dev server
    grunt.registerTask("local",  [ "connect:dev", "watch"]);
    grunt.registerTask("local.gf",  [ "hub:gardenflip"]);
	grunt.registerTask("local.dist", [ "connect:dist"]);


    // Unit tests
	grunt.registerTask("test", [ "karma:unit" ]);


	/**
     * Writes out a list of options for this project to the command-line
     * if someone just comes in and writes `grunt`.
     */
	grunt.registerTask("default", function() {

        grunt.log.header( "Hi there. What would you like to run?".yellow.bold );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt build".yellow);
        grunt.log.writeln("Will build a production ready version of your project.".grey);


        grunt.log.writeln( ""); // LINE BREAK
                    
        
        grunt.log.writeln( "grunt update".yellow + " or ".grey + "grunt up".yellow );
        grunt.log.writeln("Will update your grunt and bower dependencies.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt local.dev".yellow );
        grunt.log.writeln( "Will run a local development server.".grey );
                    
        
        grunt.log.writeln( ""); // LINE BREAK

        
        grunt.log.writeln( "grunt local.dist".yellow );
        grunt.log.writeln( "Will run a local server with production-ready 'distilled' resources.".grey );
                    

        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt js".yellow );
        grunt.log.writeln( " - Will compile your js. ".grey + "--dev".yellow + " will skip the minification process.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt css".yellow );
        grunt.log.writeln( "Will compile your app's scss to css (both debug & minified versions) & generate any compass sprites. \nOther variations are as follows:".grey );
        grunt.log.writeln( "grunt css.debug".yellow + "         - Debug only".grey );
        grunt.log.writeln( "grunt css.debugapp".yellow + "      - Quick debug of app.scss only".grey );
        grunt.log.writeln( "grunt css.min".yellow + "           - Min only".grey );
        grunt.log.writeln( "grunt css.watch".yellow + "         - Both, plus watch".grey );
        grunt.log.writeln( "grunt css.debug.watch".yellow + "   - Debug only, plus watch".grey );
        grunt.log.writeln( "grunt css.min.watch".yellow + "     - Min only, plus watch".grey );
                    

        grunt.log.writeln( ""); // LINE BREAK
        

        grunt.log.writeln( "grunt svg".yellow );
        grunt.log.writeln( "Will run svg tasks for minification and definitions.".grey );

        
        grunt.log.writeln( ""); // LINE BREAK
        

        grunt.log.writeln( "grunt misc".yellow );
        grunt.log.writeln( "Will run any additional tasks for the project, such as copying images and libraries from '_dev' into '_dist'.".grey );

        
        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt test".yellow );
        grunt.log.writeln( "Will run all unit tests.".grey );
	});



    grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-hub');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-shell');



    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json')

        ,paths: {
            dev: "_dev"
            ,dist: "_dist"
            ,devDomain: 'http://<%= connect.options.hostname %>:<%= connect.dev.options.port %>/'
            ,distDomain: 'http://<%= connect.options.hostname %>:<%= connect.dist.options.port %>/'
        }
        

        /**
         * Using grunt-hub to call other Gruntfiles so we can keep our builds organised and portable.
         */
        ,hub: {
            options: {
                concurrent: 3
            }
            // run RequireJS optimiser and minify js
            ,rjs: {
                src: ["grunt_tasks/rjs/Gruntfile.js"]
                ,tasks: [ "default" ]
            }
            // svg tasks for minification and definitions
            ,svg: {
                src: ["grunt_tasks/svg/Gruntfile.js"]
                ,tasks: [ "default" ]
            }
            // other miscellaneous tasks, such as copying files
            ,misc: {
                src: ["grunt_tasks/misc/Gruntfile.js"]
                ,tasks: [ "default" ]
            }


            /**
             * Options for Sass
             */

            // Both debug & min css 
            ,"sass.both": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:both" ]
            }
            // Debug only
            ,"sass.debug": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:debug" ]
            }
            // Min only
            ,"sass.min": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:min" ]
            }


            // Quick debug of app.scss only
            ,"sass.debugapp": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:debugapp" ]
            }

            ,"sass.wdebugapp": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wdebugapp" ]
            }


            // Both debug & min css, plus watch
            ,"sass.wboth": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wboth" ]
            }
            // Debug & watch
            ,"sass.wbug": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wbug" ]
            }
            // Min & watch
            ,"sass.wmin": {
                src: ["grunt_tasks/sass/Gruntfile.js"]
                ,tasks: [ "default:wmin" ]
            }


            // externals
            ,"gardenflip": {
                src: ["_dev/deps/externals/ngui/GardenFlip/_example/Gruntfile.js"]
                ,tasks: ["local"]
            }
        }

        

        /**
         * 'keepalive' should be false if 'livereload' is true.
         * Using a custom port seems to screw up livereload as well, so just stick to value of true.
         */
        ,connect: {
            options: {
                hostname: 'localhost'
            },
            dev: {
                options: {
                    keepalive: false,
                    livereload: true,
                    port: 8001,
                    base: "",
                    open: '<%= paths.devDomain %>_dev/app/'
                }
            },
            dist: {
                options: {
                    port: 8000,
                    keepalive: true,
                    livereload: false,
                    base: '<%= paths.dist %>/deploy/',
                    open: '<%= paths.distDomain %>'
                }
            }
        },


        watch: {
            html: {
                options: {
                    livereload: true
                },
                files: [
                    '<%= paths.dist %>/forcereload.txt' // change this whenever you want to force a reload
                ]
            }
        }


        ,shell: {
            grunt_update: {
                command: [
                     "npm --prefix grunt_tasks/rjs install"
                    ,"npm --prefix grunt_tasks/sass install"
                    ,"npm --prefix grunt_tasks/misc install"
                    ,"npm --prefix grunt_tasks/svg install"
                    ,"npm --prefix grunt_tasks/generators install"
                ].join('&&')
            }
            ,bower_update: {
                command: "bower install"
            }
        }


        ,karma: {
            unit: {
                 configFile: 'test/karma.conf.js'
                ,autoWatch: false
                ,singleRun: true
            }
        }

    });
};