define('CORE/js/Constants',[],function() {

	var  nameCC = "sunshineFDC"
		,nameSC = "Sunshine FDC"
		,nameUC = "SUNSHINE_FDC"
		,nameLC = "sunshinefdc"
		,nameShort = "sfdc";

	return {
		 baseName: nameShort
		,rootPath: "/"
		,imgPath: "/deploy/img"
		,isLocalDev: (window.location.hostname.indexOf("localhost") != -1)
		,cssSelectors: {
			app: "#" + nameShort
		}
	}
});

define( 'EX_SERVICES/ModuleNameFetcher',[ "lodash" ], function( _ ) {
    

    /**
     * TODO
     */

    var NAME = "moduleNameFetcher"
    ,type = function ( test ) {
        return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
    }
    ,getName = function( $getObj ) {

        var name;

        if( type( $getObj ) === "array" ) {
            var meth = _.where( $getObj, function( item ) { return (typeof item === "function"); })[0];

            // console.log( meth().name );

            name = meth().name;
        } else {
            name = $getObj().name;
        }

        return name;
    }

    return {

        

        /** 
         * Don't inject any deps unless you really need to, because gets called before app has been initialised,
         * which means they need to be read without Angular.
         */
        $get: function() {


            return {

                // Return name for the Services Module to retrieve
                name: NAME

                ,getModuleNames: function( args, isService ) { // "isService" gets used in unit tests

                    var rtnArr = [];
                    _.forEach( args, function( arg ) {

                        // if( !arg ) console.log("no arg");

                        // Uses 'factory' to be sure that we are using an Angular module. Could use any module property though.
                        var modulePass = (arg && type( arg ) === "object" && arg.factory && arg.name);

                        if( modulePass ) {
                            rtnArr.push( arg.name );
                        } else if( isService === true && arg && type( arg ) === "object" && arg.$get ) {

                            // Uses 'name', if it exists. This is not a standard Angular Service convention though.
                            rtnArr.push( getName(arg.$get) );
                        }
                    });

                    return rtnArr;
                }


                ,getServiceName: function(arg) {

                    if( type(arg) === "object" && arg.$get )
                        return getName(arg.$get);

                    if(console && console.warn)
                        console.warn( "Sorry, you must provide a valid '$get' based service for this utility. "+ NAME +" -> getServiceName." );
                }
                

                ,isService: function( val ) {

                    return (type(val) === "object" && !!(val.$get));
                }
            }
        }
    }
});

define( 'EX_SERVICES/Util',["lodash"], function( _ ) {
    

    return {
        
        $get: ['$route', function( $route ) {
            return {

                name: "util"

                /**
                 * getRoute() allows for dynamic page urls to be changed globally
                 */
                ,getRoute: function( controllerName ) {

                    // console.log( "controllerName", controllerName );

                    //handle the case when the user has come from facebook with no hashbang
                    var route="#";
                    if (controllerName) {
                        _.each( $route.routes, function(name, val) {
                            if( val.controller === controllerName ) {
                                route += name
                                return false;
                            }
                        });
                    } else {
                        return route;
                    }

                    return route;
                }

                ,refreshSameArray: function( originalArr, newArr, addToExisting ) {

                    /**
                      * For cases where you need to clear your array and add new items,
                      * but you can't just do arr = [] because it has data-bindings that
                      * will be lost otherwise.
                      */

                    if( !addToExisting ) originalArr.length = 0;

                    var i=0,
                        len = newArr.length;
                    for( ; i<len; i++ ) {
                        originalArr.push( newArr[i] );
                    }
                }

                ,logWarning: function( warningMsg ) {
                    if( window.console && window.console.warn ) {
                        window.console.warn( warningMsg );
                    } else if( window.console && window.console.log ) {
                        window.console.log( warningMsg );
                    } else {
                        alert( warningMsg );
                    }
                }

                /**
                 * Below here taken from jquery.jimd.utils.common.v0.1.5.js
                 */

                ,type: function ( test ) {
                    return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
                }

                ,supports_canvas: function() {
                    return !!document.createElement('canvas').getContext;
                }

                ,supportsCSSAni: function() {
                    // taken from https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Using_CSS_animations/Detecting_CSS_animation_support
                    var animation = false,
                        animationstring = 'animation',
                        keyframeprefix = '',
                        domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
                        pfx  = '';

                    if( document.body.style.animationName ) { animation = true; }

                    if( animation === false ) {
                        for( var i = 0; i < domPrefixes.length; i++ ) {
                            if( document.body.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
                                pfx = domPrefixes[ i ];
                                animationstring = pfx + 'Animation';
                                keyframeprefix = '-' + pfx.toLowerCase() + '-';
                                animation = true;
                                break;
                            }
                        }
                    }

                    return animation;
                }

                ,canvasWrapText: function(context, text, x, y, maxWidth, lineHeight) {
                    // Creates automatic line breaks in canvas text, based on canvas width
                    var words = text.split(' ');
                    var line = '';

                    for(var n = 0; n < words.length; n++) {
                        var testLine = line + words[n] + ' ';
                        var metrics = context.measureText(testLine);
                        var testWidth = metrics.width;
                        if(testWidth > maxWidth) {
                            context.fillText(line, x, y);
                            line = words[n] + ' ';
                            y += lineHeight;
                        }
                        else {
                            line = testLine;
                        }
                    }
                    context.fillText(line, x, y);
                }

                ,isIOS: function(){

                    var i = 0,
                        iOS = false,
                        iDevice = ['iPad', 'iPhone', 'iPod'];

                    for ( ; i < iDevice.length ; i++ ) {
                        if( navigator.platform === iDevice[i] ){ iOS = true; break; }
                    }

                    return iOS;
                }

                // lum is a decimal - positive = lighter - negative = darker
                ,colorLuminance: function(hex, lum) {
                    // validate hex string
                    hex = String(hex).replace(/[^0-9a-f]/gi, '');
                    if (hex.length < 6) {
                        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
                    }
                    lum = lum || 0;
                    // convert to decimal and change luminosity
                    var rgb = "#", c, i;
                    for (i = 0; i < 3; i++) {
                        c = parseInt(hex.substr(i*2,2), 16);
                        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                        rgb += ("00"+c).substr(c.length);
                    }
                    return rgb;
                }

                ,setArgb: function(val) {
                    var valArr = val.split("(")[1].split(")")[0].split(","),
                        red     = _utils.toHex(valArr[0]),
                        green = _utils.toHex(valArr[1]),
                        blue    = _utils.toHex(valArr[2]),
                        alpha = _utils.toHex(valArr[3]*255);
                    return "#" + alpha + red + green + blue;
                }

                ,getHexFromRGB: function(val) {
                    var valArr = val.split("(")[1].split(")")[0].split(","),
                        red     = _utils.toHex(valArr[0]),
                        green = _utils.toHex(valArr[1]),
                        blue    = _utils.toHex(valArr[2]);
                    return "#" + red + green + blue;
                }

                ,setHex: function(val) {
                    var value = val.substring(1,9),
                        red = parseInt(value.substring(2,4), 16),
                        green = parseInt(value.substring(4,6), 16),
                        blue = parseInt(value.substring(6,8), 16),
                        alpha = Math.round((parseInt(value.substring(0,2), 16)/255)*10)/10;
                    return "rgba(" + red + "," + green + "," + blue + "," + alpha +")";
                }

                ,toHex: function(val) {
                    val = parseInt(val); // might need a radix of 16 (2nd param) - needs testing
                    val = Math.max(0,val);
                    val = Math.min(val,255);
                    val = Math.round(val);
                    return "0123456789ABCDEF".charAt((val-val%16)/16) + "0123456789ABCDEF".charAt(val%16);
                }

                ,sniffIE: function( $jq ) {

                    if( $jq && $jq.browser ) {
                        if( $jq.browser.msie )    return $jq.browser.version;
                        return false;
                    }

                    if( navigator.userAgent.toLowerCase().indexOf( 'msie' ) != -1 ) return document.documentMode;
                    return false;
                }
            }
        }]
    }
});

define( 'EX_SERVICES/ValidateModel',[ "angular", "lodash"], function( angular, _ ) {
    

    /**
     * Service to check validity of Models.
     * Only works in ES5 browsers, but then you're probably debugging in one, right?
     */

    return {

        /** 
         * Don't inject any deps unless you really need to, because Models should to be able to read this, 
         * which are not specifically Angular functions, meaning they'd then need to parse an array.
         */
        $get: function() {

            return {

                // Return name for the Services Module to retrieve
                name: "validateModel"

                /**
                 * Method to check that an 'inheritedModel', passed into a Directive's isolated scope, is a 
                 * perfect shallow match to the accompanying Model. This is to avoid dynamic Model properties 
                 * that the Directive isn't expecting. What you see in the Model is what you should expect. 
                 * Also prevents extensibility by throwing an error if you attempt to add a new property.
                 */
                ,directiveModel: function( Mdl, baseName, scope, oneWay, inhMdlName ) {

                    // Uses default 'inhMdlName' if custom one not specified
                    if( !inhMdlName ) inhMdlName = "inhMdl";

                    // Get the 'inheritedModel', using different syntax, based on a Directive's 'oneWay' isolated scope properties.
                    var inhMdl = (oneWay ? scope[ inhMdlName ]() : scope[ inhMdlName ]);

                    // If the Directive's scope has been passed a model, check that it's shallow Object keys are identical to the directive's model.
                    if( inhMdl && Object.keys ) {

                        var shallowComparePassed = _.every( Object.keys( Mdl ), function( key ) {
                            return key in inhMdl;
                        });

                        if( shallowComparePassed === false ) {
                            if( console && console.error ) {
                                console.error(  "Mdl comparison failed on Directive "+ baseName +"!" + 
                                                " 'inheritedModel' must have same keys as the 'Mdl'." );
                            }
                            return null;
                        }
                    }

                    var mdl = inhMdl || angular.copy( Mdl );

                    if(Object.preventExtensions)
                        Object.preventExtensions(mdl);
                    
                    scope.Mdl = mdl;
                    
                    return mdl;
                }


                /**
                 * Prevents extensibility by throwing an error if you attempt to add a new property.
                 */
                ,pageModel: function( Mdl, scope ) {

                    var mdl = angular.copy( Mdl );

                    if(Object.preventExtensions)
                        Object.preventExtensions(mdl);

                    scope.Mdl = mdl;

                    return mdl;
                }
            }
        }
    }
});

// probably get rid of this, in favour of the AppModel service and combine the two
define('CORE/js/AppMdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {    
    
    /**
     * Holds the non-extensible 'Model' for this page.
     * Nesting objects within 'Model' is also best practise for defining objects that will be declared as an 
     * 'inheritedModel' in child directives' scopes.
     */
    

    var Mdl = {

        pageState: ["is-from-game", "is-from-intro"]

        ,gflip: {
            paused: true // if true, will still show the overlay bg
            ,showIntroPanel: true // if false, will hide the intro panel, but still show the white overlay bg
            ,isActive: false // should be true when game is showing, regardless of paused state
            ,found: 0
            ,introComplete: false
            ,allTokensFound: false // set to true for debugging replay panel
            ,bgfallback: false
            ,countDownMax: 3
            ,isCounting: false
            ,countDownCurrent: -1
            ,hmTapRayOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 500 // kids aren't too acurate, so give them some slack
            }
            ,hmTapHandOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1200 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 400 // kids aren't too acurate, so give them some slack
            }
            ,hmTapBtnOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
            }
        }

        ,pagenav: {
            introComplete: false
            ,expanded: false
            ,navfallback: false
            ,notransforms3d: false
            ,hmTapCarriageOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
            }
            ,hmTapTitleOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 800 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 100 // kids aren't too acurate, so give them some slack
            }
        }

        ,contentOverlay: {
            gflipIsPlayable: true
            ,hmTapBgOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 500 // kids aren't too acurate, so give them some slack
            }
        }
    };


    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('CORE/js/AppCtrl',[ "angular", "./AppMdl", "modernizr" ], function( ng, mdl, Mdnzr ) {
    

    return [ '$rootScope', '$scope', '$window', '$timeout', '$element', '$log', 'appModelSetter', 'selectorUtil', 
    	function( $rootScope, $scope, $window, $timeout, $element, $log, ams, su ) {
        var Mdl = mdl( $scope ); // A non-extensible $scope.Mdl.
        
        // Sets AppMdl, so that rest of the App can edit and watch it via this service
        ams.setMdl( $scope, Mdl );

        // on app resize, load and page change scroll to top vertically & centre horizontal
        var resize = function() {
            var wind = su.getScrollableWindowEl();

            wind.scrollTop = 0;

            if( $element[0].clientWidth > document.body.clientWidth )
                wind.scrollLeft = Math.round( ($element[0].clientWidth - document.body.clientWidth) / 2 );

            $element[0].style.height = wind.innerHeight + "px";
        }

        $window.addEventListener( "resize", resize );
        resize();

        /*$scope.$on('$routeChangeStart', function() {
        	$window.onresize();

        	Mdl.pageState = "is-from-game";
        });*/
        
        // Mdl.pageState = [];

        $rootScope.$on("$locationChangeStart", function(event, next, current) { 
        	resize();
	        $log.info("location changing to:" + next); 
	    });
    }];
});


define('PG_HOME/js/Mdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {    
    

    var Mdl = {
		title: "home"
		,gflipBgFallback: false
		,giggleState: {
			stopGiggling: false
			,resetTimeOut: 3000 // ms until giggle starts again
			,loopCurrent: 0 // loop number
			,loopMax: 3 // max loops before complete stop
			,classes:[]
		}
        ,hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('PG_HOME/js/Ctrl',[ "./Mdl", "CORE/js/Constants" ],
    function( mdl, Constants ) {
    

    return [ '$rootScope', '$scope', '$timeout', 'appModelSetter', 'pageHelper', 
    function ( $rootScope, $scope, $timeout, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        ams.setWatcher( ams.types.GFLIP, "bgfallback", function(val) {
            pgMdl.gflipBgFallback = val;
        }, true);

        $scope.closePage = function( isClick ) {

            stopGiggling();
            pageHelper.closePage( isClick );
        }

        startGiggling();

        function stopGiggling() {
            pgMdl.giggleState.stopGiggling = true;
            pgMdl.giggleState.loopCurrent = 0;
            pgMdl.giggleState.classes.length = 0;
        }

        function startGiggling( fromIntv ) {

            pgMdl.giggleState.classes.push( "is-giggling" );
            pgMdl.giggleState.loopCurrent++;

            if( pgMdl.giggleState.loopCurrent > pgMdl.giggleState.loopMax ) {
                stopGiggling();
                return;
            }
            $timeout( function() {
                if( pgMdl.giggleState.stopGiggling ) return;

                startGiggling();
            }, pgMdl.giggleState.resetTimeOut );
        }        
        
    }];
});


define('text!PG_HOME/template.html',[],function () { return '<div class="l-page l-pghome">\r\n    <section>\r\n\r\n    \t<div class="l-page-outter">\r\n\t    \t<div class="pghome-content l-page-inner">\r\n\t        \t\r\n\t        \t<!-- \'.pghome-btnplay\' wrapped in a span because we want to keep the giggle state separate and there is only 1 transform property per element -->\r\n\t        \t<!-- <span class="transformable" ng-class="Mdl.giggleState.classes" ng-if="!Mdl.gflipBgFallback" >\r\n\t        \t\t<button class="l-pghome-btnplay pghome-btnplay btngo btnnormalize" hm-tap="closePage()">Play the game!</button>\r\n\t        \t</span> -->\r\n\r\n\t        \t<button class="l-pghome-btnplay pghome-btnplay btngo btnnormalize" \r\n\t        \t\t\thm-tap="closePage()" hm-tap-opts="Mdl.hmTapBtnOpts" \r\n\t        \t\t\tng-class="Mdl.giggleState.classes" ng-if="!Mdl.gflipBgFallback" >\r\n\t        \t\t<span class="btngo-inner ani" >\r\n\t        \t\t\tPlay the game!\r\n\t        \t\t</span>\r\n\t        \t</button>\r\n\r\n\t        \t<span class="l-page-inner-scrollable scrollable-y" ng-class="{ \'l-pghome-topgap\' : !Mdl.gflipBgFallback }">\r\n\t        \t\t<h2 class="pghome-heading heading heading1">Welcome to Sunshine Family Day Care!</h2>\r\n\t\t        \t<p class="pghome-body pg-body">Sunshine Family Day Care is a place where families and their children feel safe, secure and supported. It provides an unique experience that focuses on individual needs by providing friendly, homely and educational environments. \r\n\t\t        \t<br>\r\n\t\t\t\t\t"Miss Melinda" is a highly trained and motivated Early Childhood Teacher who guides and provides children with a secure and holistic approach to Early Childhood, which draws upon a wide range of philosophies and techniques.\r\n\t\t        \t<br>\r\n\t\t\t\t\tSunshine Family Day Care was established in January 2014 and welcomes all families and their children to its service. \r\n\t\t\t\t\t</p>\r\n\t        \t</span>\r\n\r\n\t        \t<button class="pg-btnclose pghome-btnclose btnclose-speechbubble btnnormalize"\r\n\t        \t\t\thm-tap="closePage()" hm-tap-opts="Mdl.hmTapBtnOpts"  \r\n\t        \t\t\tng-click="closePage(true)"\r\n\t        \t\t\t>\r\n\t        \t\t<span class="title">Close</span>\r\n\t\t        \t<svg class="bg">\r\n\t\t        \t\t<use style="fill:#82C350; stroke:#467F1A;" xlink:href="#core-speechbubble"></use>\r\n\t\t        \t</svg>\r\n\t        \t</button>\r\n\t        \t\r\n\t        \t<svg class="pghome-mushroomred transformable">\r\n\t        \t\t<use style="fill:#C9282E" xlink:href="#core-mushroom"></use>\r\n\t        \t</svg>\r\n\t        \t\r\n\t        \t<svg class="pghome-caterpillar transformable">\r\n\t        \t\t<use xlink:href="#core-caterpillar"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pghome-bug transformable">\r\n\t        \t\t<use xlink:href="#core-bug"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pghome-fish transformable">\r\n\t        \t\t<use style="fill:#E87B36" xlink:href="#core-fish"></use>\r\n\t        \t</svg>\r\n\r\n\r\n        \t</div>\r\n        </div>\r\n\r\n    </section>\r\n</div>';});


define('PG_GAME/js/Mdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    var Mdl = {
		
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('PG_GAME/js/Ctrl',[ "./Mdl" ], function( mdl ) {
    

    return [ '$scope', '$rootScope', 'appModelSetter', function ( $scope, $rootScope, AppModelSetter ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        console.log( "GAME" );
        $rootScope.$broadcast( "gflipStart" );
    }];
});


define('text!PG_GAME/template.html',[],function () { return '<div class="l-page l-pggame">\r\n    <section>\r\n    \t<!-- <h1>GAME</h1> -->\r\n\t\t<!-- meant to be empty, so we can play the game underneath it -->\r\n\t</section>\r\n</div>';});


define('PG_GALLERY/js/Mdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    var Mdl = {
		title: "gallery"		
		,slideIndex: 0
		// "loadedImages" doesn't get populated until the page transition has finished
		,loadedImages: []
		,imagesToLoad: [
			 {id:"bookcorner1"}
			,{id:"construction1"}
			,{id:"construction2"}
			,{id:"construction3"}
			,{id:"couches"}
			,{id:"deck1"}
			,{id:"kidstable"}
			,{id:"kitchen1"}
			,{id:"music1"}
			,{id:"music2"}
			,{id:"playmat1"}
			,{id:"playmat2"}
			,{id:"playmat3"}
			,{id:"shelf1"}
			,{id:"signin"}
			,{id:"valentinesday"}
		]
		,hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('PG_GALLERY/js/Ctrl',[ "./Mdl" ], function( mdl ) {
    

    return [ '$rootScope', '$scope', '$timeout', 'appModelSetter', 'pageHelper', 
    function ( $rootScope, $scope, $timeout, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $timeout( function() {
	        $scope.$apply( function() {
        		pgMdl.loadedImages = pgMdl.imagesToLoad;
	        });
        }, 900);

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }
        
    }];
});


define('text!PG_GALLERY/template.html',[],function () { return '<div class="l-page l-pggallery">\r\n    <section>\r\n\r\n        <div class="l-page-outter">\r\n\t    \t<div class="pggallery-content l-page-inner">\r\n\t        \t<span class="l-page-inner-scrollable scrollable-y">\r\n\t        \t\t<h2 class="pggallery-heading heading heading1">Gallery</h2>\r\n\t\t        \t<p class="pggallery-body pg-body"></p>\r\n\t        \t</span>\r\n\r\n\t        \t\r\n\t        \t<button class="pg-btnclose pggallery-btnclose btnclose-speechbubble btnnormalize"  \r\n\t\t    \t\t\thm-tap="closePage()" hm-tap-opts="Mdl.hmTapBtnOpts" \r\n\t\t    \t\t\tng-click="closePage(true)"\r\n\t\t    \t\t\t>\r\n\t        \t\t<span class="title">Close</span>\r\n\t\t        \t<svg class="bg">\r\n\t\t        \t\t<use style="fill:#FAEF8E; stroke:#9F9110;" xlink:href="#core-speechbubble"></use>\r\n\t\t        \t</svg>\r\n\t        \t</button>\r\n\t        \t\r\n\t        \t<svg class="pggallery-leaf pggallery-token transformable">\r\n\t        \t\t<use xlink:href="#core-leaf"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pggallery-chicken pggallery-token transformable">\r\n\t        \t\t<use xlink:href="#core-chicken"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pggallery-apple pggallery-token transformable">\r\n\t        \t\t<use xlink:href="#core-apple"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pggallery-carrot pggallery-token transformable">\r\n\t        \t\t<use style="fill:#E78A2D" xlink:href="#core-carrot"></use>\r\n\t        \t</svg>\r\n\r\n\t\t\t\t<div class="ng-cloak gallerythumbnails">\r\n\t\t            <button class="gallerythumbnails-btn" ng-repeat="img in Mdl.loadedImages" ng-click="Mdl.slideIndex=$index" ng-class="{\'is-active\': (Mdl.slideIndex==$index)}">\r\n\t\t            \t<span class="gallerythumbnails-img" ng-class="\'appsprite-\' + img.id"></span>\r\n\t\t            </button>\r\n\t\t        </div>\r\n\r\n\t        \t<ul rn-carousel rn-carousel-index="Mdl.slideIndex" class="image">\r\n\t\t\t\t  <li ng-repeat="img in Mdl.loadedImages" ng-style="{\'background-image\': \'url(/_dist/deploy/img/\' + img.id + \'.jpg)\'}">\r\n\t\t\t\t  </li>\r\n\t\t\t\t</ul>\r\n        \t</div>\r\n        </div>\r\n\r\n    </section>\r\n</div>';});


define('PG_NEWSLETTERS/js/Mdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    var Mdl = {
        hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('PG_NEWSLETTERS/js/Ctrl',[ "./Mdl" ], function( mdl ) {
    

    return [ '$rootScope', '$scope', 'appModelSetter', 'pageHelper'
        ,function ( $rootScope, $scope, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }
        
    }];
});


define('text!PG_NEWSLETTERS/template.html',[],function () { return '<div class="l-page l-pgnewsletters">\r\n\r\n  <div class="l-page-outter">\r\n  \t<div class="pgnewsletters-content l-page-inner">\r\n    \t<span class="l-page-inner-scrollable scrollable-y">\r\n    \t\t<section>\r\n\t    \t\t<h2 class="pgnewsletters-heading heading heading1">About Me</h2>\r\n\t\t    \t<p class="pgnewsletters-body pg-body">\r\n\t\t    \t"Miss Melinda" has finished her Early Childhood Studies at The Open University, London (UK), whilst she was working as a pre-school teacher at the Barbican Playgroup in London. She comes from a family full of primary and pre-school teachers and has been teaching various age groups since she was eighteen. Melinda is a keen learner and a dedicated person when it comes to her professional development. She is a fan of the Montessori teaching techniques and also enjoys trying new learning experiences from Reggio Emilia as well.\r\n\t\t    \t</p>\r\n\t\t\t\r\n\t    \t\t<h2 class="pgnewsletters-heading heading heading1">Philosophy</h2>\r\n\t\t\t    \t<p class="pgnewsletters-body pg-body">Sunshine Family Day Care aims to provide high quality care and education for children under school age. Its service will be enriched by positive relationships with parents, the coordination unit, the community and the environment. Its goal to deliver a prompt, flexible, inclusive and caring service that meets the diverse needs of families.\r\n\t\t\t    \t<br>\r\n\t\t\t    \tIt believes children learn best through play and are capable of building knowledge and understanding from their world.\r\n\t\t\t    \t<br>\r\n\t\t\t    \tIt believes children need to feel loved, accepted and to be part of a group regardless of differences, to foster self-esteem and maximise the probability of reaching their full potential... \'Belonging, Being and Becoming\'.\r\n\t\t\t    \t<br>\r\n\t\t\t    \tIt believes in providing a welcoming atmosphere, fun play environments and challenging yet safe learning experiences that will evoke curiosity, experimentation and exploration.\r\n\t\t\t    \t<br>\r\n\t\t\t    \tSunshine Family Day Care respects every child and their family. It aims to promote healthy growing, happiness, curiosity, as well as loving and caring for others and nature.\r\n\t\t\t    \t<br>\r\n\t\t\t    \tIts target is to encourage children to become independent, self-confident, adventurous, respectful, open-minded and to encourage them to explore.\r\n\t\t    \t</p>\r\n\t\t\t</section>\r\n    \t</span>\r\n\r\n    \t<button class="pg-btnclose pgnewsletters-btnclose btnclose-speechbubble btnnormalize" \r\n    \t\t\thm-tap="closePage()" hm-tap-opts="Mdl.hmTapBtnOpts" \r\n                ng-click="closePage(true)"\r\n    \t\t\t>\r\n    \t\t<span class="title">Close</span>\r\n\t    \t<svg class="bg">\r\n\t    \t\t<use style="fill:#FA9A8E; stroke:#9F2110;" xlink:href="#core-speechbubble"></use>\r\n\t    \t</svg>\r\n    \t</button>\r\n    \t\r\n    \t<svg class="pgnewsletters-pear transformable">\r\n    \t\t<use xlink:href="#core-pear"></use>\r\n    \t</svg>\r\n    \t<svg class="pgnewsletters-butterfly transformable">\r\n    \t\t<use xlink:href="#core-butterfly"></use>\r\n    \t</svg>\r\n    \t<svg class="pgnewsletters-ladybird transformable">\r\n    \t\t<use xlink:href="#core-ladybird"></use>\r\n    \t</svg>\r\n    \t<svg class="pgnewsletters-moon transformable">\r\n    \t\t<use xlink:href="#core-moon"></use>\r\n    \t</svg>\r\n  \t</div>\r\n  </div>\r\n\r\n</div>';});


define('PG_CONTACT/js/Mdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    var Mdl = {
		hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});

define('PG_CONTACT/js/Ctrl',[ "./Mdl" ], function( mdl ) {
    

    return [ '$location', '$rootScope', '$scope', 'appModelSetter', 'pageHelper'
        ,function ( $location, $rootScope, $scope, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }

        $scope.map = {
		    center: {
		        latitude: -33.498643
		        ,longitude: 151.320778
		    }
		    ,zoom: 18
		};
        
    }];
});


define('text!PG_CONTACT/template.html',[],function () { return '<div class="l-page l-pgcontact">\r\n    <section>\r\n\r\n        <div class="l-page-outter">\r\n\t    \t<div class="pgcontact-content l-page-inner">\r\n\t        \t<span class="l-page-inner-scrollable scrollable-y">\r\n\t        \t\t<h2 class="pgcontact-heading heading heading1">Contact</h2>\r\n\t\t        \t<p class="pgcontact-body pg-body">\r\n\t\t        \tIf you would like to get in contact please either phone me, on 0414 938 773, or email me at <a class="bodylink" href="mailto:melindaweigert@yahoo.com.au">melindaweigert@yahoo.com.au</a>.\r\n\r\n\t\t        \t<h3 class="pgcontact-heading heading heading2">Location</h3>\r\n\t\t        \tWe are located on Mascot Street, in Woy Woy*, just off Ocean Beach Road.\r\n\t\t        \t</p>\r\n\t\t        \t<p class="pgcontact-discl">* Exact address isn\'t shown for privacy purposes.</p>\r\n\t        \t</span>\r\n\r\n\t        \t<google-map center="map.center" zoom="map.zoom" draggable="true", options="{\'mapTypeId\': \'hybrid\'}" ></google-map>\r\n\r\n\t        \t<button class="pg-btnclose pgcontact-btnclose btnclose-speechbubble btnnormalize" \r\n\t\t    \t\t\thm-tap="closePage()" hm-tap-opts="Mdl.hmTapBtnOpts" \r\n\t\t    \t\t\tng-click="closePage(true)"\r\n\t\t    \t\t\t>\r\n\t        \t\t<span class="title">Close</span>\r\n\t\t        \t<svg class="bg">\r\n\t\t        \t\t<use style="fill:#7F9ED1; stroke:#0F316A;" xlink:href="#core-speechbubble"></use>\r\n\t\t        \t</svg>\r\n\t        \t</button>\r\n\t        \t\r\n\t        \t<svg class="pgcontact-flower transformable">\r\n\t        \t\t<use xlink:href="#core-flower"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pgcontact-fish transformable">\r\n\t        \t\t<use style="fill:#38517A" xlink:href="#core-fish"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pgcontact-mushroom transformable">\r\n\t        \t\t<use style="fill:#1D3679" xlink:href="#core-mushroom"></use>\r\n\t        \t</svg>\r\n\t        \t<svg class="pgcontact-parsnip transformable">\r\n\t        \t\t<use style="fill:#fff" xlink:href="#core-carrot"></use>\r\n\t        \t</svg>\r\n        \t</div>\r\n        </div>\r\n\r\n    </section>\r\n</div>';});


// change name to AppMdlManager
define( 'SERVICES/AppModelSetter',["lodash"], function(_) {
    

    /**
     * Service for injecting the AppModel.
     * Delierately has no getMdl method, as all properties should be set via setVal.
     * This also restricts the ability to add a property to the callee's scope, forcing
     * them to use the "setWatcher" method, which gives this service more control.
     */

    // constants
    var NAME = "appModelSetter"
        ,types = {
            GFLIP: "gflip"
            ,PAGE_NAV: "pagenav"
            ,CONTENT_OVERLAY: "contentOverlay"
        };

    return {
        
        $get: function() {

            var AppMdl
                ,watchers = [];

            var warning = function( methName, type, prop ) {
                    if( console && console.warn )
                        console.warn( "WARNING: Could not set property '" + prop + "' on AppMdl object '" + type + "' in method "+methName+"()." );
                }
    
            return {

                name: NAME
                ,types: types
                
                ,setMdl: function( appScope, mdl ) {
                    AppMdl = mdl;

                    // perhaps could try using $watchCollection, as may be less expensive, or have a seperate watcher or each type

                    // always watches the app model on the app controller scope
                    appScope.$watch( "Mdl", function( newVal, oldVal ) {
                        
                        _.forEach( watchers, function(w) {
                            
                            // console.log( newVal[ w.type ][ w.prop ], oldVal[ w.type ][ w.prop ] );

                            if( newVal[ w.type ][ w.prop ] !== oldVal[ w.type ][ w.prop ] ) {
                                w.callback( newVal[ w.type ][ w.prop ] );
                            }
                        });
                    }, true);
                }

                ,setVal: function( type, prop, val ) {

                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {
                        AppMdl[ type ][ prop ] = val;
                    } else {
                        warning( 'setVal', type, prop );
                    }
                }

                // Just for debugging. Don't to give access with a 'getVal' method, but sometimes need to log it.
                ,logVal: function( type, prop ) {

                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {

                        // gets passed grunt-remove-logging task
                        if( console && console.log )
                            console['log']( AppMdl[ type ][ prop ] );
                    } else {
                        warning( 'logVal', type, prop );
                    }
                }

                ,setWatcher: function( type, prop, callback, doSet ) {
                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {
                        watchers.push( { type: type, prop: prop, callback: callback } );
                        if( doSet ) callback( AppMdl[ type ][ prop ] );
                    } else {
                        warning( 'setWatcher', type, prop );
                    }
                }
            }
        }
    }
});

define( 'SERVICES/PageHelper',['require'],function(_) {
    

    /**
     * For functions that are the same across multiple pages
     */

    // constants
    var NAME = "pageHelper";

    return {
        
        $get: ['$location', 'appModelSetter', 'util', function($location, ams, util) {

            return {

                name: NAME
                
                ,closePage: function( isClick ) {
                    // clicks are there just for ie8
                    if( isClick === true ) {
                        if( !util.sniffIE() || util.sniffIE() > 8 ) return;
                    }

                    $location.path("game");
                    ams.setVal( ams.types.PAGE_NAV, "expanded", false );
                }
            }
        }]
    }
});

define( 'EX_SERVICES/SelectorUtil',["angular"], function(ng) {
    

    /**
     * Utility methods for selectors.
     */

    var NAME = "selectorUtil"
        ,prevEl;

    return {
        
        $get: function() {

            return {

                name: NAME

                /**
                 * Using HTML5 "querySelectorAll", creates a convenience method similar to jQuery.find.
                 * Stores 'prevEl' so that you can omit it if you want to keep using tha same root element.
                 * Defaults to just show you the first item in the selector's array result, but you can override this with a true value on 'allSel' arg.
                 */
                ,find: function(qu, el, allSel, noNGWrap) {

                    prevEl = el;

                    if(!el) {
                        if(console && console.warn)
                            console.warn("Sorry, but you must provide an element as 2nd parameter. " + NAME + " -> find");
                        return;
                    }

                    var mainEl = el[0] || el
                        ,sel = mainEl.querySelectorAll( qu );
                    if( allSel === true ) {
                        
                        // Return the HTML elements
                        if( noNGWrap === true ) return sel;

                        // Wrap HTML elements in an Angular jQuery Lite object
                        var selArr = [];
                        ng.forEach(sel, function(el) {
                            selArr.push( ng.element(el) );
                        });
                        return selArr;
                    } else {

                        // Return the HTML element
                        if( noNGWrap === true ) return sel[0];

                        // Wrap HTML element in an Angular jQuery Lite object
                        return ng.element( sel[0] );
                    }
                }

                ,getScrollableWindowEl: function() {
                    var docTop = document.documentElement.scrollTop
                        ,docLeft = document.documentElement.scrollLeft
                        ,bodyTop = document.documentElement.scrollTop
                        ,bodyLeft = document.documentElement.scrollLeft;

                    if( docLeft === 0 && bodyLeft > 0 ||
                        docTop  === 0 && bodyTop  > 0 )
                        return document.body;
                    
                    if( docLeft > 0 && bodyLeft === 0 || 
                        docTop  > 0 && bodyTop  === 0 )
                        return document.documentElement;

                    document.documentElement.scrollTop = 1;
                    if( document.documentElement.scrollTop === 1 ) {
                        document.documentElement.scrollTop = 0;
                        return document.documentElement
                    }

                    document.body.scrollTop = 1;
                    if( document.body.scrollTop === 1 )
                        document.body.scrollTop = 0;
                    
                    return document.body;
                }
            }
        }
    }
});

define('EX_SERVICES/AnimationUtil',['modernizr'], function( Modernizr ) {
    

    /**
     * Utility methods for animations.
     */

    var NAME = "animationUtil"
        ,prevEl;

    return {
        
        $get: ["$timeout", function($timeout) {
    
            return {

                name: NAME

                // if $scope is passed, function will wrap callback in a $scope.$apply
                ,prefixedEvent: function(element, type, callback, $scope, allowChildEvents) {

                    if( !element.addEventListener ) {
                        $timeout( function() {
                            if( console && console.warn )
                                console.warn(NAME + " -> prefixedEvent() 'element' does not have property 'addEventListener'.");
                            callback( null );
                        }, 0);
                        return;
                    }

                    var pfx = ["webkit", "moz", "MS", "o", ""];
                    for (var p = 0; p < pfx.length; p++) {
                        if (!pfx[p]) type = type.toLowerCase();

                        // needs to be true for FF and IE. Something to do with bubbling.
                        var useCapture = true;

                        element.addEventListener(pfx[p]+type, function(evt) {

                            // stops children events
                            if( !allowChildEvents && evt.target !== element ) return;

                            if( $scope ) $scope.$apply(function() {callback(evt)});
                            else         callback(evt);

                        }, useCapture);

                    }

                    // so they can be tested
                    return pfx;
                }

                ,removeClassOnAnimationComplete: function( $el, aniClasses, isTransition, $scope, callback, allowChildEvents, removeFromTarget ) {

                    // removes the class from the target or throws warning if doesn't exist
                    var removeCls = function(cls, tgt) {
                        if( tgt.classList.contains(cls) ) {
                            tgt.classList.remove(cls);
                            
                            if( callback ) callback( cls );
                        } else if( console && console.warn ) {

                            // returns 'false' for 'removed class', then tells you which class it means
                            if( callback ) callback( false, cls );
                        }
                    }

                    var determineClasses = function(tgt) {
                        if( typeof aniClasses === "string" ) {
                            // if 'aniClasses'is a string, then we assume there is a single class to remove
                            removeCls( aniClasses, tgt );
                        } else {
                            // if 'aniClasses'is an array, then we assume there are multiple classes to remove
                            _.forEach( aniClasses, function(cls, tgt) {
                                removeCls( cls );
                            });
                        }
                    }


                    // return early if css animations aren't supported
                    if( !Modernizr.cssanimations ) {
                        determineClasses( $el );
                        return;
                    }


                    this.prefixedEvent( $el, (isTransition ? "TransitionEnd" : "AnimationEnd"), function(evt) {

                        // Whether to use the target animated, which could be a child, or the specific element passed in.
                        determineClasses( removeFromTarget === true ? evt.target : $el );

                    }, $scope, allowChildEvents);
                }
            }

            /*,animationStartAddClass: function() {

            }*/
        }]
    }
});

define( 'EX_SERVICES/StringUtil',[ "angular", "lodash"], function( ng, _ ) {
    

    /**
     * Utility methods for strings and text related things.
     */

    var NAME = "stringUtil";;

    return {
        
        $get: [ "selectorUtil", function( su ) {
    
                return {
    
                    name: NAME
        
                    /**
                     * Returns the inner html of the selector arg with each character wrapped in it's own span tag.
                     * If you actually intend on using the markup returned, Angular 1.2.x uses $sce 
                     * (Scrict Contextual Escaping) by default, which needs the js dep 'ngSanitize' module to be included 
                     * in the project and 'ng-bind-html' to be used in the markup.
                     */
                    ,getSeparateChars: function( el, selector ) {

                        // if length is undefined, means that the element is not wrapped in jQuery
                        if( el.length != undefined ) el = el[0];

                        var targetEl = ( selector ? su.find( selector, el, false, true ) : el )
                            ,markup = ""
                            ,chars;

                        _.forEach( targetEl.innerHTML, function( ch, i ) {

                            chars = ch.split(" ").join("&nbsp;");
                            markup += '<span class="ch'+(i+1)+'">'+chars+'</span>';
                        });

                        return markup;
                    }


                    // jquery dependency for trim. Could be easily replaced.
                    ,countWords: function( text, $jq ) {
                        var regex = /\s+/gi;
                        var wordCount = $jq.trim(text) // use jquery because String.trim is ES5 only
                                            .replace(regex, ' ')
                                            .replace(/\.,/g,"") // remove dots
                                            .replace(/ +(?= )/g,'') //remove double spaces
                                            .split(' ').length;
                        return wordCount;
                    }


                    // jquery dependency for trim and each. Could be easily replaced.
                    ,getTextUpToWordLimit: function( text, wordLimit, $jq ) {

                        var regex = /\s+/gi;
                        var wordArr = $jq.trim(text) // use jquery because String.trim is ES5 only
                                            .replace(regex, ' ')
                                            .replace(/\.,/g,"") // remove dots
                                            .replace(/ +(?= )/g,'') //remove double spaces
                                            .split(' ');

                        var rtnText = "";
                        $jq.each( wordArr, function(key, val) {

                            if( key < wordLimit )   rtnText += ' ' + val;
                            else                    return false; // breaks loop
                        });

                        return rtnText + ' ';
                    }

                    ,replaceStringBetween: function( rxStart, rxEnd, originalString, replacementString ) {
                        // Notes:
                        // 1. In this expression "\\d\\D" makes sure line breaks are included
                        // 2. And "*?" means that everything will be replaced between
                        // 3. "g" stands for global, which means it will look through the entire string
                        // 4. If you want to escape a "/", you must make it "\/"
                        // 5. If you want to escape an "*", you must make it "\\*"

                        var rx = new RegExp( rxStart + "[\\d\\D]*?" + rxEnd, "g");
                        return originalString.replace( rx, replacementString );
                    }

                    ,toSentenceCase: function(str) {
                        return str.substr(0,1).toUpperCase() + str.substr(1).toLowerCase();
                    }

                    ,toCamelCase: function(str, makeTitleCase, includeSpaces){
                        return str.split(/\s+/).map(function(word, index){
                            word = word.toLowerCase();
                            if(index !== 0 || makeTitleCase === true){
                                return word.split('').map(function(char, i){
                                            if(i===0){
                                                return char.toUpperCase();
                                            }
                                            return char;
                                            
                                        }).join('')
                            }
                            return word;
                            
                        }).join(includeSpaces === true ? ' ' : '');
                    }
                }
            }]
    }
});

define( 'EX_SERVICES/DebugUtil',[],function() {
    // 

    /**
     * Utility methods for debugging.
     */

    var NAME = "debugUtil";

    return {
        
        $get: function() {
    
            return {

                name: NAME

                // priority 1 (important)
                ,p1: function( msg1, msg2, msg3, msg4 ) {

                    var logArr  = [ msg1 ];

                    if( msg2 ) logArr.push( msg2 );
                    if( msg3 ) logArr.push( msg3 );
                    if( msg4 ) logArr.push( msg4 );

                    // gets passed grunt-remove-logging task
                    if( console && console.log )
                        console['log']( logArr );
                }
            }
        }
    }
});
define('CORE/js/Services',[ "angular", "lodash", "CORE/js/Constants"
    ,"EX_SERVICES/ModuleNameFetcher"
    ,"SERVICES/AppModelSetter"
    ,"SERVICES/PageHelper"
    ,"EX_SERVICES/ValidateModel"
    ,"EX_SERVICES/Util"
    ,"EX_SERVICES/SelectorUtil"
    ,"EX_SERVICES/AnimationUtil"
    ,"EX_SERVICES/StringUtil"
    ,"EX_SERVICES/DebugUtil"
    // ,"HH_SERVICES/SnapShotService/Service"
    // ,"HH_SERVICES/MetaDataService/Service"
], function ( ng, _, Constants, ModuleNameFetcher ) {

    var services = ng.module( Constants.baseName + "ServicesModule", []);

	// services.provider allows us to use $provide in unit tests and write service arguments exactly the same as they are written
    // eg: MyProjectAppSnapShotService - however, this won't build in the rjs optimiser, so just used for unit tests.

    _.forEach( arguments, function(arg) {
        if( ModuleNameFetcher.$get().isService(arg) === true )
            services.provider( ModuleNameFetcher.$get().getServiceName(arg), arg );
    });

    // console.log( services._invokeQueue[0][2][0] );

	return services;
});

define('D_GARDEN_FLIP/js/DefMdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "gardenFlip"; // must be camelCase
    
    var Mdl = {
        paused: true // if true, will still show the overlay bg
        ,showIntroPanel: false // if false, will hide the intro panel, but still show the white overlay bg
        ,isActive: false // should be true when game is showing, regardless of paused state
        ,found:0
        ,introComplete: false
        ,allTokensFound: true // set to true for debugging replay panel
        ,bgfallback: false
        ,countDownMax: 3
        ,isCounting: false
        ,countDownCurrent: -1
        ,hmTapRayOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
        ,hmTapHandOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1200 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 400 // kids aren't too acurate, so give them some slack
        }
        ,hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
    };

    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME; // if no args, just return BASE_NAME
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});

define('D_GARDEN_FLIP/js/MainCtrl',[ "angular", "./DefMdl", "lodash", "modernizr" ], function( ng, mdl, _, Mdnzr ) {
    

    return [ '$scope', '$rootScope', '$element', '$timeout', '$window', "selectorUtil", "stringUtil", "animationUtil", "debugUtil", 
        function ( $scope, $rootScope, $element, $timeout, $window, selUtil, strUtil, aniUtil, du ) {
        var Mdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        // make lodash available on the template
        $scope._ = _;

        // Tells the directive to only show the fallback bg image if any of the conditions fail
        if( !Mdnzr.cssanimations || !Mdnzr.csstransforms || !Mdnzr.csstransitions || !Mdnzr.svg ) {
            Mdl.bgfallback = true;

            // Don't want the rest of the logic.
            return;
        }


        var TokenMap = $scope.TokenMap = [
             { id:"LadyBird",       map:"gflip-raynth0",    wander: "med" }
            ,{ id:"Parsnip",        map:"gflip-raynth1",    wander: "med" }
            ,{ id:"FishRight",      map:"gflip-raynth2",    wander: "long" }
            ,{ id:"Pear",           map:"gflip-raynth3",    wander: "short" }
            ,{ id:"Moon",           map:"gflip-raynth4",    wander: "short" }
            ,{ id:"MushroomBlue",   map:"gflip-raynth5",    wander: "short" }
            ,{ id:"Carrot",         map:"gflip-raynth6",    wander: "short" }
            ,{ id:"Bug",            map:"gflip-raynth7",    wander: "short" }
            ,{ id:"FishLeft",       map:"gflip-raynth8",    wander: "med" }
            ,{ id:"Apple",          map:"gflip-raynth9",    wander: "short" }
            ,{ id:"MushroomRed",    map:"gflip-hand-left",  wander: "short" }
            ,{ id:"Chicken",        map:"gflip-hand-right", wander: "long" }
        ]

        var wanderInvt
            ,wanderIncr
            ,wanderAutoCount
            ,wanderAutoMaxCount = 2
            ,wanderOrder = [];

        _.forEach( TokenMap, function(map, i) { wanderOrder[i] = i; });


        init();

        $scope.handClick = function(name) {
            if( Mdl.paused ) return;

            // $scope[name+'ArmAni'] = '';

            // adds a tiny delay between removing and adding again. Couldn't use $apply because it's already in progress.
            // $timeout(function() {
                $scope[name+'ArmAni'] = 'arm-is-raising';
            // }, 0);

            var map = _.where( TokenMap, { map:"gflip-hand-"+name })[0];
            tokenTargeted( map, ( name == 'left' ? '10' : "11" ) );
        }


        $scope.rayClick = function(i) {
            if( Mdl.paused ) return;

            // $scope['rayAni'+i] = '';
            $scope['rayAni'+i] = 'ray-is-opening';

            // adds a tiny delay between removing and adding again. Couldn't use $apply because it's already in progress.
            /*$timeout(function() {
                $scope['rayAni'+i] = 'ray-is-opening';
            }, 0);*/

            var map = _.where( TokenMap, { map:"gflip-raynth"+i })[0];
            tokenTargeted( map, i );
        }


        function init() {
            // separates characters into span tags so that they can be rotated individually
            $scope.heading1Text = strUtil.getSeparateChars( $element, ".gflip-mainheading .line1" );
            $scope.heading2Text = strUtil.getSeparateChars( $element, ".gflip-mainheading .line2" );
            manageHandDepths( 'left' );
            manageHandDepths( 'right' );

            removeWandersOnComplete();
            $timeout( removeRayHandlerOnComplete, 0); // needs timeout because classes are created dynamically

            $rootScope.$on( "gflipStart", function(evt) {

                Mdl.isActive = true;

                if( Mdl.introComplete === true ) {
                    // resume game
                    Mdl.showIntroPanel = false;
                    Mdl.paused = false;
                    startWanderTicker( false, true, true );
                } else {

                    // start afresh
                    startWanderTicker( true );
                    $rootScope.$broadcast( "gflipIntroStart" );
                }
            });



            $rootScope.$on( "gflipIntroPause", function(evt, data) {

                // if game not showing, no nothing
                if( Mdl.isActive === false ) return;

                if( !data.showIntroPanel ) Mdl.isActive = false;

                // if already paused, the watch on Mdl.pause won't get triggered, so we trigger the same function
                if( Mdl.paused === true )   onPause();
                else                        Mdl.paused = true;
            });


            /**
             * Shouldn't add any game start logic watchers, just things like interval cancellers and non-infinite css anis.
             * Things can get messy if too much logic is placed on a watch.
             */

            $scope.$watch( "Mdl.found", function(val) {
                if( val === 0 ) resetTokenClasses();
            });

            $scope.$watch( "Mdl.paused", function(val) {
                if( val === true ) onPause();
            });
        }


        function onPause() {
            removeWanderingAni( true );
            stopWanderTicker();
        }

        // when a consealer (eg. a hand or sun ray) is clicked
        function tokenTargeted( map, i ) {

            if( map.isFound ) {
                // no need for any token changes if token has already been found
                return;
            } else {
                Mdl.found++;
                map.isFound = true;
            }

            $scope[ map.tokenCls ].push( "token-is-found-slot"+i ); //overrides wander
            $scope[ map.tokenCls + "Outter" ].push( "token-is-found" );

            removeWanderingAni( true );

            var delayedRestart = Mdl.found < TokenMap.length;
            stopWanderTicker( true, delayedRestart );

            if( !delayedRestart ) {
                du.p1( "All tokens found" );
                Mdl.allTokensFound = true;
            }
        }


        // sets some initial bindings
        function resetTokenClasses() {
            _.forEach( TokenMap, function(map, i) {

                // creates new properties on map
                map.tokenId = "ngId" + map.id;
                map.tokenCls = "ngCls" + map.id;

                map.isFound = false;

                $scope[ map.tokenId ] = map.id;
                $scope[ map.tokenCls ] = [];
                $scope[ map.tokenCls + "Outter" ] = ["gflip-" + map.id.toLowerCase() ]; // this will become obselete
            });
        }


        /** 
          * Sets a handler for when the wander animations have finished, this finds the parent element and 
          * fetches its "data-id" value, which is saved in the TokenMap, before triggering removeWanderingAni().
          */
        function removeWandersOnComplete() {
            
            var el = selUtil.find(".gflip-rays", $element, false, true);
            aniUtil.prefixedEvent( el, "AnimationEnd", function(evt) {

                // feels like this could be improved

                var tgt = evt.target
                    ,prnt;

                if( tgt.classList.contains("inner") ) {
                    if( tgt.parentElement.classList.contains("gflip-token") ) {
                        prnt = tgt.parentElement;
                    } else if( tgt.parentElement.parentElement.classList.contains("gflip-token") ) {
                        prnt = tgt.parentElement.parentElement;
                    }
                    
                    removeWanderingAni( false, prnt.parentElement.attributes["data-id"].value );
                }
                
            }, $scope);
        }


        
        // If doAll is passed, removes ani from all tokens, otherwise uses id for single token
        function removeWanderingAni( doAll, id ) {

            var mappedToken,arr;

            if( doAll ) {
                arr = TokenMap;
            } else if( id )   {
                arr = [ _.where( TokenMap, {id:id})[0] ];
            }

            if( !arr ) {
                if( console && console.warn )
                    console.warn("gflip -> MainCtrl -> removeWanderingAni -> token arr is empty");
                return;
            }

            _.forEach( arr, function(mappedToken, i) {
                
                // removes "token-is-wandering-..." ani classes from the token
                _.remove( $scope[ mappedToken.tokenCls ], function(item) {
                    return item.indexOf("token-is-wandering-") === 0;
                });
            });
        }


        // Stops the ticker controlling the wander animation
        function stopWanderTicker( doReset, delayedRestart ) {

            if( doReset ) {
                wanderAutoCount = 0;
                wanderIncr = 0;
            }

            if( wanderInvt ) clearInterval( wanderInvt );

            if( delayedRestart ) {
                wanderIncr = -1; // wait this many seconds before restart
                startWanderTicker();
            }
        }


        // Starts the ticker controlling the wander animation
        function startWanderTicker( doReset, startImmediately, skipShuffle ) {
            
            if( doReset ) {
                wanderAutoCount = 0;
                wanderIncr = 0;
            }
            
            // Choose NOT to reshuffle the token order with "skipShuffle"
            if( !skipShuffle ) wanderOrder = _.shuffle(wanderOrder);

            // Choose to start immediately, rather than wait for the initial timeout to trigger
            if( startImmediately ) wanderTicker();

            // Make sure the interval has been cleared before setting it again to avoid lost interval defs
            if( wanderInvt ) clearInterval( wanderInvt );
            wanderInvt = setInterval( function() {
                $scope.$apply( wanderTicker );
            }, 3000);
        }


        function wanderMaxIterationReached() {
            // if the maximum amount of "auto" wanders is reached, stop the ticker
            if( wanderAutoCount >= wanderAutoMaxCount ) {
                stopWanderTicker( true );
                du.p1( "auto wander stopped" );
                return
            }
            
            // otherwise keep ticking
            wanderAutoCount++;
            wanderIncr = 0;
        }


        function wanderTicker() {
            
            // if negative, delay the wander until positive
            if( wanderIncr < 0 ) {
                wanderIncr++;
                return;
            }

            // If all available tokens have been iterated through, 
            if( wanderIncr >= wanderOrder.length ) {
                wanderMaxIterationReached();
                return;
            }

            var map = TokenMap[ wanderOrder[wanderIncr] ];

            // if the token has already been found, skip to the next token in the list, without delay
            if( map.isFound ) {
                wanderIncr++;
                stopWanderTicker();
                startWanderTicker( false, true, true );
                return;
            }

            // Add the wander class if it doesn't exist already
            if( $scope[ map.tokenCls ].indexOf("token-is-wandering-"+map.wander ) === -1 )
                $scope[ map.tokenCls ].push( "token-is-wandering-"+map.wander );

            // tick
            wanderIncr++;
        }


        /**
         * Listens for animation events and adds a class to manage depth.
         * Needs a timeout because the events are non-angular
         */
        function manageHandDepths( name ) {
            
            var $el = selUtil.find(".gflip-hand-"+name, $element, false, true);

            var allowChildEvents = true; // Because hits areas are child events, we must allow them
            aniUtil.prefixedEvent( $el, "AnimationStart", function() {
                
                // couldn't use $scope.$apply here because says digest is already in progress (FF)
                $timeout( function() {
                    $scope[name+'ArmAniDepth'] = 'arm-raising-hidepth';
                }, 0);    
            }, null, allowChildEvents);


            /**
             * Didn't use 'aniUtil.removeClassOnAnimationComplete' because the classes needed to be removed
             * from bound variables. Last 'true' param allows child events, because hit area is a child.
             */
            aniUtil.prefixedEvent($el, "AnimationEnd", function(evt) {
                $scope[name+'ArmAniDepth'] = "";
                $scope[name+'ArmAni'] = "";
            }, $scope, true );
        }


        function removeRayHandlerOnComplete() {

            /**
             * Didn't use 'aniUtil.removeClassOnAnimationComplete' because the classes needed to be removed
             * from bound variables. Last 'true' param allows child events, because hit area is a child.
             */
            _.forEach( TokenMap, function( tknMap, i ) {

                // don't want to do the hands. They are handled in 'manageHandDepths()'.
                if( i < 10 ) {
                    var $el = selUtil.find( "." + tknMap.map, $element, false, true );
                    // console.log( $el.length, tknMap.map );

                    aniUtil.prefixedEvent($el, "AnimationEnd", function(evt) {

                        // console.log( "ray target", i,  evt.target )
                        $scope['rayAni'+i] = "";
                    }, $scope, true );
                }
            });

        }

    }];
});


define('D_GARDEN_FLIP/js/IntroCtrl',[ "./DefMdl", "lodash", 'modernizr' ], function( mdl, _, Modernizr ) {
    

    return [ '$scope', '$rootScope', '$element', '$sce', '$timeout', '$animate', 'selectorUtil', "animationUtil", "debugUtil"
        ,function ( $scope, $rootScope, $element, $sce, $timeout, $animate, su, au, du ) {
        var Mdl = $scope.Mdl = $scope.inhMdl; // A non-extensible $scope.Mdl.

        // make lodash available on the template
        $scope._ = _;

        if( Mdl.bgfallback ) {

            /**
             * If the 'bgfallback' is true, game is disabled.
             * However, the bg of the intro panel still needs to hide and show.
             * This happens on events being called from the parent app, which we listen to here.
             */

            $rootScope.$on( "gflipIntroPause", function(evt, data) {
                if( data.showIntroPanel === false ) Mdl.paused = true;
            });

            $rootScope.$on( "gflipStart", function(evt) {
                Mdl.paused = false;
            });

            // Don't want the rest of the logic.
            return;
        }


        var TokenMap = $scope.TokenMap = [
             { tokenCls:"ngClsLadybird" }
            ,{ tokenCls:"ngClsParsnip" }
            ,{ tokenCls:"ngClsFishRight" }
            ,{ tokenCls:"ngClsPear" }
            ,{ tokenCls:"ngClsMoon" }
            ,{ tokenCls:"ngClsMushroomBlue" }
            ,{ tokenCls:"ngClsCarrot" }
            ,{ tokenCls:"ngClsBug" }
            ,{ tokenCls:"ngClsFishLeft" }
            ,{ tokenCls:"ngClsApple" }
            ,{ tokenCls:"ngClsMushroomRed" }
            ,{ tokenCls:"ngClsChicken" }
        ]

        var msgTypes = {
            RESUME: "msgTypes.RESUME"
            ,PLAY: "msgTypes.PLAY"
            ,REPLAY: "msgTypes.REPLAY"
            ,COUNTDOWN_START: "msgTypes.COUNTDOWN_START"
            ,COUNTDOWN_COUNTING: "msgTypes.COUNTDOWN_COUNTING"
            ,COUNTDOWN_COMPLETE: "msgTypes.COUNTDOWN_COMPLETE"
        }

        var countDownIntv, replayTimeout;

        init();

        function init() {

            /**
             * Because nested ngShow wasn't working we force the element to show, with 'panelShow = true', when animation starts.
             * Then, inside 'setNestedShowHandler' we hide the element with 'panelShow = false', after it has ended.
             */
            $scope.$watch( "Mdl.showIntroPanel", function(val) {
                if( Mdl.isActive === true && val === true ) {
                    $scope.panelShow = true;
                    $scope.introShow = true;
                }
                
            });
            
            setNestedShowHandler();

            $scope.ngClsBtn1 = [];

            $rootScope.$on( "gflipIntroStart", function() {
                resetCountdown( true );
            });

            $rootScope.$on( "gflipStart", function(evt) {
                $scope.introShow = false;
                console.log( "gflipStart" );
            });

            // gets called after same event handler in MainCtrl.js
            $rootScope.$on( "gflipIntroPause", function(evt, data) {

                Mdl.showIntroPanel = data.showIntroPanel || false;

                console.log("gflipIntroPause");
                // if( Mdl.isActive === true ) {
                // if( Mdl.showIntroPanel === false ) {
                    $scope.introShow = true;
                // }

                // console.log( "intro Mdl.isActive", Mdl.isActive );
                // console.log( "intro Mdl.showIntroPanel", Mdl.showIntroPanel );
                // console.log( "intro Mdl.paused", Mdl.paused );

                // if game not showing, no nothing
                if( Mdl.isActive === false ) return;

                // if Mdl.allTokensFound is true, it means the "Replay?" panel is showing, so we don't want to change anything
                if( Mdl.allTokensFound === true ) return;

                if( Mdl.isCounting === true ) {
                    resetCountdown( false, false, data.skipPageNavCollapse || false );

                    //needs timeout because counter has one inside "resetCountdown" method
                    // $timeout( function() { 
                        setMsg( msgTypes.PLAY );
                    // }, 0);
                } else {
                    setMsg( msgTypes.RESUME );
                }
            });

            /**
             * Should add any game start logic watchers, just things like interval cancellers and non-infinite css anis.
             * Things can get messy if too much logic is placed on a watch.
             */

            $scope.$watch( "Mdl.allTokensFound", function(val) {
                if( val )   onAllTokensFound();
                // else        setMsg( msgTypes.COUNTDOWN_START );
            });

            
        }


        // This button can be either "Play", "Replay" or "OK" (resume).
        $scope.btn1Clicked = function() {

            /** 
             * If count down is over, meaning that the game has started, 
             * but not all the tokens have been found yet, resume the game.
             */
            if( Mdl.isCounting === false && Mdl.allTokensFound === false ) {
                resumeGame();
                return;
            }

            /**
             * "Play" and "Replay" do the same thing.
             * "Play" shows up only if paused before count down has finished.
             * "Replay" shows up when all the tokens have been found.
             */
            newGame();
        }


        $scope.btn2Clicked = function() {
            // So far, btn2 is always a "reset" btn
            newGame();
        }


        function newGame() {
            du.p1( "New Game" );
            resetTokens();
            
            _.remove( $scope.ngClsBtn1, function(cls) {
                return cls === "is-twirling";
            });

            resetCountdown( true );
        }


        function resumeGame() {
            du.p1( "Resuming Game" );
            Mdl.paused = false;
            Mdl.showIntroPanel = false;
            resetCountdown();
        }


        function onAllTokensFound() {
            setMsg( msgTypes.REPLAY );

            Mdl.paused = true;
            Mdl.showIntroPanel = true;
            Mdl.introComplete = false;
        }



        function resetCountdown( doStart, resetDelay, skipPageNavCollapse ) {
            if( countDownIntv ) clearInterval( countDownIntv );

            Mdl.countDownCurrent = Mdl.countDownMax;
            setMsg( msgTypes.COUNTDOWN_COUNTING, Mdl.countDownMax );

            if( !skipPageNavCollapse ) collapsePageNav();

            if( !doStart ) {
                Mdl.isCounting = false;
                return;
            }

            setMsg( msgTypes.COUNTDOWN_START, Mdl.countDownMax );
            resetTokens();
            Mdl.isCounting = true;
            Mdl.showIntroPanel = true;
            $scope.panelShow = true;
            $scope.introShow = true;

            countDownIntv = setInterval( function() {
                $scope.$apply(function() { countByIncr(); });
            }, 1000 );

        }


        function resetTokens() {
            Mdl.found = 0;
            Mdl.allTokensFound = false;
        }


        function collapsePageNav() {
            // debugger
            $rootScope.$broadcast( "expandPageNav", { expandState: false } );
        }


        function countDownComplete() {
            setMsg( msgTypes.COUNTDOWN_COMPLETE );

            Mdl.showIntroPanel = false;
            Mdl.introComplete = true;
            Mdl.paused = false;
        }


        function countByIncr( incr ) {

            if( !incr ) incr = -1;

            Mdl.countDownCurrent += incr;

            if( Mdl.countDownCurrent > 0 )
                setMsg( msgTypes.COUNTDOWN_COUNTING, Mdl.countDownCurrent );
            else
                countDownComplete()

            if( Mdl.countDownCurrent === -1 ) resetCountdown( false, 3000 );
        }


        function setMsg( type, msg ) {

            if( msg ) msg = ( typeof msg === "string" ? msg : msg.toString() );

            switch( type ) {
                case msgTypes.RESUME:
                    $scope.mainMsg = "Continue playing?";
                    $scope.playBtnMsg = $sce.trustAsHtml( "OK" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnplay", "btnmed", "is-twirling" );
                    $scope.showBtn2 = true;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return cls === "l-gflipintro-btnresume" || cls === "l-gflipintro-btnreplay" || cls === "btnbig";
                    });

                    du.p1( "RESUME", $scope.ngClsBtn1 );
                    break;

                case msgTypes.PLAY:
                    $scope.mainMsg = "Start playing?";
                    $scope.playBtnMsg = $sce.trustAsHtml( "OK" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnresume", "btnbig", "is-twirling" );
                    $scope.showBtn2 = false;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return cls === "l-gflipintro-btnplay" || cls === "l-gflipintro-btnreplay" || cls === "btnmed";
                    });

                    du.p1( "PLAY", $scope.ngClsBtn1 );
                    break;

                case msgTypes.REPLAY:
                    $scope.mainMsg = "Woohoo! All tokens found!!";
                    $scope.playBtnMsg = $sce.trustAsHtml( "Replay?" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnreplay", "is-twirling" );
                    $scope.showBtn2 = false;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return  cls === "l-gflipintro-btnplay" || cls === "l-gflipintro-btnresume" || 
                                cls === "btnbig" || cls === "btnmed";
                    });

                    du.p1( "REPLAY", $scope.ngClsBtn1 );
                    break;

                case msgTypes.COUNTDOWN_START:
                    $scope.ngClsBtn1.length = 0;
                    $scope.mainMsg = "Find the tokens!!";
                    $scope.showBtn2 = false;
                    $scope.ngClsCountDownMsg = "";

                    du.p1( "COUNTDOWN_START", $scope.ngClsBtn1 );
                    break;

                case msgTypes.COUNTDOWN_COUNTING:
                    $scope.ngClsBtn1.length = 0;
                    $scope.countDownMsg = $sce.trustAsHtml( msg );

                    du.p1( "COUNTDOWN_COUNTING", $scope.ngClsBtn1, msg );
                    break;

                case msgTypes.COUNTDOWN_COMPLETE:
                    $scope.ngClsBtn1.length = 0;
                    $scope.countDownMsg = $sce.trustAsHtml( "Go!!" );
                    $scope.ngClsCountDownMsg = "go-title";
                    
                    du.p1( "COUNTDOWN_COMPLETE", $scope.ngClsBtn1 );
                    break;
            }
        }

        // Removes animation classes after they have completed so that reverse class can be added
        function setNestedShowHandler() {            
            var $el = su.find(".gflipintro-panel", $element, false, true);
            au.removeClassOnAnimationComplete( $el, "nested-show", false, $scope);
            au.removeClassOnAnimationComplete( $el, "nested-hide", false, $scope, function(clsRemoved, failedClsRemove) {
                if( clsRemoved && clsRemoved === "nested-hide" ) {
                    $scope.panelShow = false;
                    
                    if( Mdl.isActive ) $scope.introShow = false;
                }
            });
        }
    }];
});


define('text!D_GARDEN_FLIP/mainTemplate.html',[],function () { return '<section class="gflip l-gflip-constrained" \r\n\t\t ng-class="{\'gflip-completed\': Mdl.found === TokenMap.length, \'bgfallback\': Mdl.bgfallback}">\r\n\r\n\t<div class="gflip-playarea">\r\n    \r\n    \t<h1 class="gflip-mainheading gflip-heading1 gflip-heading">\r\n    \t\t<span ng-bind-html="heading1Text" class="line1">Sunshine</span>\r\n    \t\t<span ng-bind-html="heading2Text" class="line2">Family Day Care</span>\r\n    \t</h1>\r\n\r\n\t    <div class="gflip-heading2-container">\r\n\t    \t<h2 class="gflip-heading2 gflip-heading transformable">Find the <span class="portrait-hide">Sunshine</span> Secrets!!!</h2>\r\n\t    </div>\r\n\t    \r\n\t    <div class="gflip-score">\r\n\t    \t<h3 ng-if="Mdl.found === TokenMap.length" class="gflip-heading3 gflip-heading">You\'ve found <br> all the <span class="portrait-hide">Sunshine </span>Secrets!!!</h3>\r\n\t    \t<h3 ng-if="Mdl.found < TokenMap.length" class="gflip-heading3 gflip-heading">Secrets found <br>{{ Mdl.found }} / {{ TokenMap.length }}</h3>\r\n\t    </div>\r\n\r\n    \t<div class="gflip-sun">\r\n\t    \t\r\n    \t\t<div class="gflip-tokenspot-container transformable">\r\n\t\t\t\t<span ng-repeat="tkn in TokenMap" class="gflip-tokenspot transformable" ng-class="\'gflip-tokenspot\' + $index" ></span>\r\n    \t\t</div>\r\n\r\n\t    \t<svg class="transformable gflip-bg">\r\n\t\t\t  <use xlink:href="#gflip-bg"></use>\r\n\t\t\t</svg>\r\n\r\n\t\t\t\r\n\t\t\t<span class="portrait-hide">\r\n\t\t\t\t<svg class="transformable gflip-butterfly">\r\n\t\t\t\t  <use xlink:href="#core-butterfly"></use>\r\n\t\t\t\t</svg>\r\n\r\n\t\t\t\t<svg class="transformable gflip-tree">\r\n\t\t\t\t  <use xlink:href="#core-tree"></use>\r\n\t\t\t\t</svg>\r\n\r\n\t\t\t\t<svg class="transformable gflip-caterpillar">\r\n\t\t\t\t  <use xlink:href="#core-caterpillar"></use>\r\n\t\t\t\t</svg>\r\n\r\n\t\t\t\t<svg class="transformable gflip-leaf">\r\n\t\t\t\t  <use xlink:href="#core-leaf"></use>\r\n\t\t\t\t</svg>\r\n\r\n\t\t\t\t<svg class="transformable gflip-flower">\r\n\t\t\t\t  <use xlink:href="#core-flower"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\r\n\r\n\t\t\t<!-- Couldn\'t use this approach because \'xlink:href\' doesn\'t get parsed. Need to look for a fix!\r\n\t\t\t<span ng-repeat="item in Mdl.rayItems">\r\n\t\t\t\t<svg class="transformable gflip-rayitem" ng-class="\'gflip-\' + item.cls" >\r\n\t\t\t\t\t<use fill="{{item.fill}}" stroke="{{item.stroke}}" xlink:href="#core-{{item.id}}"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t\t-->\r\n\r\n\t\t\t<!-- <span class="transformable gflip-hands"> -->\r\n\r\n\t\t\t\r\n\r\n\r\n\t\t\t<!-- </span> -->\r\n\r\n\t\t\t<!-- rays. Needed to use a span element as the hit because ngTouch and svgs throw blur error. -->\r\n\t\t\t<span class="transformable gflip-rays">\r\n\r\n\t\t\t    <span class="gflip-depthmod" ng-attr-data-id="{{ngIdMushroomRed}}" ng-class="ngClsMushroomRedOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsMushroomRed">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t\t<use style="fill:#C9282E" xlink:href="#core-mushroom"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdChicken}}" ng-class="ngClsChickenOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsChicken">\r\n\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t  <use xlink:href="#core-chicken"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\t\t\t\t\r\n\t\t\t\t<!-- Put the face in the rays span so they are on the same 3d plane and so depths can be adjested. -->\r\n\t\t\t\t<span \tclass="transformable gflip-hand gflip-hand-right arm-raising-ani" \r\n\t\t\t\t\t\tng-class="[rightArmAni, rightArmAniDepth]">\r\n\t\t\t\t\t<span class="ani">\r\n\t\t\t\t\t\t<span \tclass="gflip-hand-hitarea" \r\n\t\t\t\t\t\t\t\thm-tap="handClick(\'right\')" \r\n\t\t\t\t\t\t\t\thm-tap-opts="Mdl.hmTapHandOpts" ></span>\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t\t  <use xlink:href="#gflip-hand"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span \tclass="transformable gflip-hand gflip-hand-left arm-raising-ani" \r\n\t\t\t\t\t\tng-class="[leftArmAni, leftArmAniDepth]" >\r\n\t\t\t\t\t<span class="ani">\r\n\t\t\t\t\t\t<span \tclass="gflip-hand-hitarea" \r\n\t\t\t\t\t\t\t\thm-tap="handClick(\'left\')" \r\n\t\t\t\t\t\t\t\thm-tap-opts="Mdl.hmTapHandOpts" ></span>\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t  \t\t<use xlink:href="#gflip-hand"></use>\r\n\t\t\t\t\t  \t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<!-- START tokens -->\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdLadyBird}}" ng-class="ngClsLadyBirdOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsLadyBird">\r\n\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t  <use xlink:href="#core-ladybird"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdParsnip}}" ng-class="ngClsParsnipOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsParsnip">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t  <use style="fill:#fff" stroke="#ccc" xlink:href="#core-carrot"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdFishRight}}" ng-class="ngClsFishRightOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsFishRight">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t  <use style="fill:#38517A" xlink:href="#core-fish"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdPear}}" ng-class="ngClsPearOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsPear">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t\t<use xlink:href="#core-pear"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdMoon}}" ng-class="ngClsMoonOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsMoon">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t  <use xlink:href="#core-moon"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdMushroomBlue}}" ng-class="ngClsMushroomBlueOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsMushroomBlue">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t\t<use style="fill:#1D3679" xlink:href="#core-mushroom"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdCarrot}}" ng-class="ngClsCarrotOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsCarrot">\t\t\t\t\t\t\r\n\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t  <use style="fill:#E78A2D" xlink:href="#core-carrot"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdBug}}" ng-class="ngClsBugOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsBug">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t  <use xlink:href="#core-bug"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdFishLeft}}" ng-class="ngClsFishLeftOutter">\r\n\t\t\t\t\t<span class="transformable gflip-token" ng-class="ngClsFishLeft">\r\n\t\t\t\t\t\t<span class="custom">\r\n\t\t\t\t\t\t\t<svg class="inner">\r\n\t\t\t\t\t\t\t  <use style="fill:#E87B36" xlink:href="#core-fish"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t\t</span>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\t\t\t\t\r\n\t\t\t\t<span class="gflip-depthmod" ng-attr-data-id="{{ngIdApple}}" ng-class="ngClsAppleOutter">\r\n\t\t\t        <span class="transformable gflip-token" ng-class="ngClsApple">\r\n\t\t\t        \t<span class="custom">\r\n\t\t\t\t\t        <svg class="inner">\r\n\t\t\t\t\t\t\t  <use xlink:href="#core-apple"></use>\r\n\t\t\t\t\t\t\t</svg>\r\n\t\t\t        \t</span>\r\n\t\t\t        </span>\r\n\t\t\t    </span>\r\n\t\t\t\t<!-- END tokens -->\r\n\r\n\r\n\t\t\t\t<span \tclass="transformable" \r\n\t\t\t\t\t\tng-repeat="n in _.range(0,10)" \r\n\t\t\t\t\t\tng-class="\'gflip-raynth\' + $index"\r\n\t\t\t\t\t\tng-class-even="\'gflip-ray2\'" \r\n\t\t\t\t\t\tng-class-odd="\'gflip-ray1\'"\r\n\t\t\t\t\t\t>\r\n\t\t\t\t\t<span \tclass="gflip-ray-hitarea" \r\n\t\t\t\t\t\t\thm-tap="rayClick( $index )" \r\n\t\t\t\t\t\t\thm-tap-opts="Mdl.hmTapRayOpts" ></span>\r\n\t\t\t\t\t<svg class="core-svgani ray-open-ani" ng-class="rayAni{{$index}}"  >\r\n\t\t\t\t\t  <use fill="{{ $even ? \'#F7E335\' : \'#E78A2D\' }}" xlink:href="#gflip-sunray"></use>\r\n\t\t\t\t\t</svg>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<!-- Put the face in the rays span so they are on the same 3d plane and so depths can be adjested. -->\r\n\t\t\t\t<svg class="gflip-face">\r\n\t\t\t\t  <use xlink:href="#gflip-sunface"></use>\r\n\t\t\t\t</svg>\r\n\r\n\t\t\t</span>\r\n\t\t\t\r\n    \t</div><!-- end gflip-sun -->\r\n\r\n\t</div><!-- end gflip-playarea -->\r\n\r\n\t<div gflip-intro mdl="Mdl" ></div>\r\n\r\n</section>';});


define('text!D_GARDEN_FLIP/introTemplate.html',[],function () { return '<section class="gflipintro l-gflipintro" ng-show="introShow">\r\n\t<!-- \'do-animate\' class must be added to any animations you want in this module -->\r\n\t\r\n\t<span class="gflipintro-bg transformable l-gflipintro-fillscreen do-animate" ng-show="Mdl.paused" ></span>\r\n\t\r\n\t<span class="l-gflipintro-centreoutter">\r\n\t\t<span \tclass="l-gflipintro-centreinner l-gflipintro-posoffset transformable gflipintro-panel" \r\n\t\t\t\tng-class="{ \'nested-show\': Mdl.showIntroPanel && Mdl.isActive, \r\n\t\t\t\t\t\t\t\'nested-hide\': !Mdl.showIntroPanel || !Mdl.isActive }"\r\n\t\t\t\tng-show="panelShow"\r\n\t\t\t\t>\r\n\r\n\t\t\t<h1 class="gflipintro-heading heading2 heading" ng-bind-html="mainMsg" ></h1>\r\n\r\n\t\t\t<p \tng-if="Mdl.isCounting" \r\n\t\t\t\tclass="gflipintro-countdown heading" \r\n\t\t\t\tng-bind-html="countDownMsg"\r\n\t\t\t\tng-class="ngClsCountDownMsg" ></p>\r\n\r\n\t\t\t<button class="gflipintro-btn1 btngo btnnormalize" \r\n\t\t\t\t\tng-show="Mdl.paused && Mdl.isActive && !Mdl.isCounting" \r\n\t\t\t\t\thm-tap="btn1Clicked()" \r\n\t\t\t\t\thm-tap-opts="Mdl.hmTapBtnOpts"\r\n\t\t\t\t\tng-class="ngClsBtn1" >\r\n\t\t\t\t<span class="btngo-inner ani" ng-bind-html="playBtnMsg"></span>\r\n\t\t\t</button>\r\n\r\n\r\n\t\t\t<button class="gflipintro-btn2 btngo btnsml btnnormalize l-gflipintro-btnreset" \r\n\t\t\t\t\tng-show="showBtn2" \r\n\t\t\t\t\thm-tap="btn2Clicked()"\r\n\t\t\t\t\thm-tap-opts="Mdl.hmTapBtnOpts" >\r\n\t\t\t\t<span class="btngo-inner ani" >Reset?</span>\r\n\t\t\t</button>\r\n\r\n\t\t\t<div class="l-gflipintro-tokens gflipintro-tokens-ani">\r\n\t\t\t\t\r\n\t\t\t\t<span class="gflipintro-mushroomred transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t\t<use style="fill:#C9282E" xlink:href="#core-mushroom"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-chicken transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t  <use xlink:href="#core-chicken"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-ladybird transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t  <use xlink:href="#core-ladybird"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\r\n\t\t\t\t<span class="gflipintro-parsnip transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t\t  <use style="fill:#fff" stroke="#ccc" xlink:href="#core-carrot"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\r\n\t\t\t\t<span class="gflipintro-fishright transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t\t  <use style="fill:#38517A" xlink:href="#core-fish"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-pear transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t\t<use xlink:href="#core-pear"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-moon transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t\t  <use xlink:href="#core-moon"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-mushroomblue transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg>\r\n\t\t\t\t\t\t\t<use style="fill:#1D3679" xlink:href="#core-mushroom"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-carrot transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\t\t\t\t\t\t\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t  <use style="fill:#E78A2D" xlink:href="#core-carrot"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-bug transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t  <use xlink:href="#core-bug"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\r\n\t\t\t\t<span class="gflipintro-fishleft transformable">\r\n\t\t\t\t\t<span class="transformable gflipintro-token" >\r\n\t\t\t\t\t\t<svg >\r\n\t\t\t\t\t\t  <use style="fill:#E87B36" xlink:href="#core-fish"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t\t\t</span>\r\n\t\t\t\t</span>\r\n\t\t\t\t\r\n\t\t\t\t<span class="gflipintro-apple transformable">\r\n\t\t\t        <span class="transformable gflipintro-token" >\r\n\t\t\t\t        <svg >\r\n\t\t\t\t\t\t  <use xlink:href="#core-apple"></use>\r\n\t\t\t\t\t\t</svg>\r\n\t\t\t        </span>\r\n\t\t\t    </span>\r\n\r\n\t\t\t</div><!-- .l-gflipintro-tokens -->\r\n\r\n\t\t</span><!-- .l-gflipintro-centreinner -->\r\n\t</span>\r\n\r\n</section>';});


define('D_GARDEN_FLIP/js/Directive',[ "./MainCtrl", "./IntroCtrl", "./DefMdl" // relative js
	,"text!D_GARDEN_FLIP/mainTemplate.html", "text!D_GARDEN_FLIP/introTemplate.html" // html partials
	,"angular-animate", 'angular-sanitize', "angular-gestures" // js deps
	], function( MainCtrl, IntroCtrl, Mdl, mainTemplate, introTemplate ) {

	var introModuleName = "gflipIntroModule";
	angular.module( introModuleName, [ "ngAnimate", "ngSanitize", "angular-gestures"] ).directive( "gflipIntro", function() {

		return {
			controller: IntroCtrl
			,template: introTemplate
			,replace: true
			,scope: {
				inhMdl: "=mdl" // inherited model - two-way binding "="
			}
		}
	});


	return angular.module( Mdl() + "Module", [ "ngAnimate", "ngSanitize", "angular-gestures", introModuleName ] ).directive( Mdl(), function() {
	        return {
				 controller: MainCtrl
				,template: mainTemplate
				,replace: true
				,scope: {
					inhMdl: "=mdl" // inherited model - two-way binding "="
				}
	        }
		});
});

define('D_PAGE_NAV/js/DefMdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "pageNav"; // must be camelCase
    
    var Mdl = {
        introComplete: false
        ,expanded: false
        ,navfallback: false
        ,notransforms3d: false
        ,hmTapCarriageOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            // ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
            // ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
        ,hmTapTitleOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            // ,tap_max_touchtime: 800 // kids can take awhile to click stuff, so give them plenty of time
            // ,tap_max_distance: 100 // kids aren't too acurate, so give them some slack
        }
    };

    // If no args, just return BASE_NAME, otherwise returns a non-extensible copy of Mdl (ES5 only)
    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME;
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});

define('D_PAGE_NAV/js/Ctrl',[ "./DefMdl", "CORE/js/Constants", "angular", "modernizr" ], function( DefMdl, Constants, ng, Mdnzr ) {
    
    var $ = ng.element;

    return [ '$scope', '$rootScope', '$element', '$timeout', '$location', 'animationUtil', 'selectorUtil', 'appModelSetter', 'util',
        function ( $scope, $rootScope, $element, $timeout, $location, au, su, ams, util ) {
        
        var Mdl = DefMdl( $scope, true ); // A non-extensible $scope.Model.

        $scope.ngClsMovingTrain = [];

        if( !Mdnzr.svg )
            Mdl.navfallback = true;

        // IE9 rotation isn't supported in transform prop & neither is 3d, so detecting with 3d
        if( !Mdnzr.csstransforms3d )
            Mdl.notransforms3d = true;

        $scope.toggleExpanded = function() {
            console.log( "toggleExpanded" );
            // return
            Mdl.expanded = !Mdl.expanded;
        }

        $scope.openPage = function( url, isClick ) {
            
            // clicks are there just for ie8
            if( isClick === true ) {
                if( !util.sniffIE() || util.sniffIE() > 8 ) return;
            }

            $scope.toggleExpanded();

            if( Mdl.expanded ) {
                $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: true, skipPageNavCollapse: true } );
            } else {
                $location.path( url );
            }
        }

        $rootScope.$on("expandPageNav", function(evt, data) {

            Mdl.expanded = data.expandState;
        });

        /*ams.setWatcher( ams.types.GFLIP, "isActive", function(val) {
            if( val === true ) Mdl.expanded = false;
        }, false );*/


        // $scope.ngClsMovingTrain = "intro-ani";
        $timeout( function() {
            $scope.$apply( startTrainShowHead );

            $scope.$watch( "Mdl.expanded", function( val ) {

                var arr = ['train-is-moving'];
                arr.push( val ? "train-is-showing-full" : "train-is-showing-head" );
                
                $scope.ngClsMovingTrain = arr;
            });
        }, 500);

        /**
         * Waits until the main transition has ended and removes the class "train-is-moving", which
         * affects the wheels. 
         * Only problem here is that it seems to listen to all events for child transitions as well. Should
         * be some solution for stopping propagation/bubbling somewhere, somehow.
         */
        var group = su.find( ".l-pagenav-pos", $element, false, true );
        au.prefixedEvent( group, "TransitionEnd", function(evt) {

            // for some reason doesn't get removed from "ngClsMovingTrain" array.
            if( !Mdl.introComplete )
                group.classList.remove("intro-ani");

            Mdl.introComplete = true;

            $scope.$apply(function() {

                var index = $scope.ngClsMovingTrain.indexOf('train-is-moving');
                if( index !== -1 ) $scope.ngClsMovingTrain.splice( index, 1 );
            });
        });

        function startTrainCont() {
            $scope.ngClsMovingTrain = ['train-is-moving', 'train-is-moving-continuously'];
        }

        function startTrainShowHead() {
            $scope.ngClsMovingTrain = ['intro-ani', 'train-is-moving', 'train-is-showing-head'];
            $scope.ngClsPagenavShowing = "";
        }
    }];
});


define('text!D_PAGE_NAV/template.html',[],function () { return '<div class="l-pagenav-container pagenav-container" ng-class="{\'navfallback\': Mdl.navfallback, \'notransforms3d\': Mdl.notransforms3d}" >\r\n    <div class="l-pagenav-pos ani" ng-class="ngClsMovingTrain">\r\n\r\n    \t<nav class="pagenav-btnnav">\r\n    \t\t<!-- hammer tap doesn\'t do anything on ie8, so clicks added as well -->\r\n    \t\t<span class="pagenav-fallbackarrows" ng-click="toggleExpanded()"><<</span>\r\n\t    \t<span class="transformable">\r\n\t    \t\t<button class="btnnav" \tng-click="openPage( \'home\', true )" \r\n\t    \t\t\t\t\t\t\t\thm-tap="openPage( \'home\' )" hm-tap-opts="Mdl.hmTapTitleOpts">Home</button>\r\n\t    \t</span>\r\n\t    \t<span class="transformable">\r\n\t    \t\t<button class="btnnav" \tng-click="openPage( \'about-me\', true )" \r\n\t    \t\t\t\t\t\t\t\thm-tap="openPage( \'about-me\' )" hm-tap-opts="Mdl.hmTapTitleOpts">About&nbsp;Me</button>\r\n\t    \t</span>\r\n\t    \t<span class="transformable">\r\n\t    \t\t<button class="btnnav" \tng-click="openPage( \'gallery\', true )" \r\n\t    \t\t\t\t\t\t\t\thm-tap="openPage( \'gallery\' )" hm-tap-opts="Mdl.hmTapTitleOpts">Gallery</button>\r\n\t    \t</span>\r\n\t    \t<span class="transformable">\r\n\t    \t\t<button class="btnnav" \tng-click="openPage( \'contact\', true )" \r\n\t    \t\t\t\t\t\t\t\thm-tap="openPage( \'contact\' )" hm-tap-opts="Mdl.hmTapTitleOpts">Contact</button>\r\n\t    \t</span>\r\n    \t</nav>\r\n\r\n\t\t<svg class="transformable pagenav-trainlink">\r\n\t\t  <use xlink:href="#core-train-link"></use>\r\n\t\t</svg>\r\n\r\n\t\t<span class="transformable pagenav-trainfront" \r\n\t\t\t\thm-tap="openPage( \'home\' )"\r\n\t\t\t\thm-tap-opts="Mdl.hmTapCarriageOpts"\r\n\t\t\t\t>\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use xlink:href="#core-train-front"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-traincarriage-yellow" \r\n\t\t\t\thm-tap="openPage( \'about-me\' )"\r\n\t\t\t\thm-tap-opts="Mdl.hmTapCarriageOpts"\r\n\t\t\t\t>\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use xlink:href="#core-train-carriage-yellow"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\r\n\t\t<span class="transformable pagenav-traincarriage-red" \r\n\t\t\t\thm-tap="openPage( \'gallery\' )"\r\n\t\t\t\thm-tap-opts="Mdl.hmTapCarriageOpts"\r\n\t\t\t\t>\r\n\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use xlink:href="#core-train-carriage-red"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-traincarriage-green" \r\n\t\t\t\thm-tap="openPage( \'contact\' )"\r\n\t\t\t\thm-tap-opts="Mdl.hmTapCarriageOpts"\r\n\t\t\t\t>\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use xlink:href="#core-train-carriage-green"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-blue1">\r\n\t\t\t<span class="inner alt">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#628BD1" stroke="#0F316A" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-blue2">\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#628BD1" stroke="#0F316A" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-yellow1">\r\n\t\t\t<span class="inner alt">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#FBEC67" stroke="#A09212" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-yellow2">\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#FBEC67" stroke="#A09212" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-red1">\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#B9594C" stroke="#A02211" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-red2">\r\n\t\t\t<span class="inner alt">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#B9594C" stroke="#A02211" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-green1">\r\n\t\t\t<span class="inner alt">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#83C451" stroke="#34680D" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n\r\n\t\t<span class="transformable pagenav-trainwheel pagenav-trainwheel-green2">\r\n\t\t\t<span class="inner">\r\n\t\t\t\t<svg>\r\n\t\t\t\t  <use fill="#83C451" stroke="#34680D" xlink:href="#core-train-wheel"></use>\r\n\t\t\t\t</svg>\r\n\t\t\t</span>\r\n\t\t</span>\r\n    \t\r\n\t    </div><!-- .l-pagenav-pos -->\r\n</div>';});


define('D_PAGE_NAV/js/Directive',[ "./Ctrl", "./DefMdl", "text!D_PAGE_NAV/template.html"]
	,function( Ctrl, Mdl, template ) {

	return angular.module( Mdl() + "Module", ["angular-gestures"] ).directive( Mdl(), function() {
	        return {
				 controller: Ctrl
				,template: template
				,replace: true
				,scope: {
					inhMdl: "&mdl" // inherited model - one-way binding "&"
				}
	        }
		});
});

define('D_CONTENT_OVERLAY/js/DefMdl',["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "contentOverlay"; // must be camelCase
    
    var Mdl = {
        gflipIsPlayable: true
        ,hmTapBgOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            // ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
            // ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
    };

    // If no args, just return BASE_NAME, otherwise returns a non-extensible copy of Mdl (ES5 only)
    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME;
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});

define('D_CONTENT_OVERLAY/js/Ctrl',[ "./DefMdl" ], function( DefMdl ) {
    

    return [ '$scope', '$rootScope', '$element', '$location', 'appModelSetter', 
    function ($scope, $rootScope, $element, $location, ams ) {
        var Mdl = DefMdl( $scope, true ); // A non-extensible $scope.Model.
        
        ams.setWatcher( ams.types.GFLIP, "isActive", showOverlay, true );
        ams.setWatcher( ams.types.GFLIP, "bgfallback", enableGFlip, true );

        // makes it so the overlay is already visible on page load, so it doesn't animate into position
        $element.removeClass("onload-state");

        $scope.unpause = function() {
            $location.path("game");
            ams.setVal( ams.types.PAGE_NAV, "expanded", false );
            $rootScope.$broadcast( "gflipStart" );
            // ams.setVal( ams.types.GFLIP, "paused", false );
        }

        function showOverlay( val ) {
            $scope.ngClsShowing = ( val ? "" : "contentoverlay-is-showing" );
        }

        function enableGFlip( val ) {
            Mdl.gflipIsPlayable = !val;
        }
    }];
});


define('text!D_CONTENT_OVERLAY/template.html',[],function () { return '<div \tclass="l-contentoverlay-container onload-state" \r\n\t\tng-class="ngClsShowing" \r\n\t\thm-tap="unpause()" \r\n\t\thm-tap-opts="Mdl.hmTapBgOpts" >\r\n    \r\n    <span class="l-contentoverlay-obscure">\r\n    \t<p ng-if="Mdl.gflipIsPlayable" class="contentoverlay-titlepaused">Paused</p>\r\n    </span>\r\n    <!-- <span class="l-contentoverlay-center-outter">\r\n    \t<span class="l-contentoverlay-center-inner">\r\n\t\t\t<p ng-if="Mdl.gflipIsPlayable" class="contentoverlay-titlepaused">Paused</p>\r\n    \t</span>\r\n    </span> -->\r\n</div>';});


define('D_CONTENT_OVERLAY/js/Directive',[ "./Ctrl", "./DefMdl", "text!D_CONTENT_OVERLAY/template.html"]
	,function( Ctrl, Mdl, template ) {

	return angular.module( Mdl() + "Module", ["angular-gestures"] ).directive( Mdl(), function() {
	        return {
				 controller: Ctrl
				,template: template
				,replace: true
				,scope: {
					inhMdl: "&mdl" // inherited model - one-way binding "&"
				}
	        }
		});
});

define('CORE/js/Directives',[ "angular", "./Constants", "EX_SERVICES/ModuleNameFetcher"
  ,"D_GARDEN_FLIP/js/Directive"
  ,"D_PAGE_NAV/js/Directive"
  ,"D_CONTENT_OVERLAY/js/Directive"
  //{{GENERATOR_INSERT_DIRECTIVE_DEFS}}
], function ( ng, Constants, ModuleNameFetcher ) {

   return ng.module( Constants.baseName + "DirectivesModule", ModuleNameFetcher.$get().getModuleNames(arguments) );
});

define('CORE/js/App',["angular", "./Constants", "EX_SERVICES/ModuleNameFetcher", "EX_SERVICES/Util", "CORE/js/AppCtrl"
        ,"PG_HOME/js/Ctrl", "text!PG_HOME/template.html"
        ,"PG_GAME/js/Ctrl", "text!PG_GAME/template.html"
        ,"PG_GALLERY/js/Ctrl", "text!PG_GALLERY/template.html"
        ,"PG_NEWSLETTERS/js/Ctrl", "text!PG_NEWSLETTERS/template.html"
        ,"PG_CONTACT/js/Ctrl", "text!PG_CONTACT/template.html"
        //{{GENERATOR_INSERT_PAGE_IMPORTS}}
        ,"angular-route", "angular-animate", "angular-sanitize", "angular-touch"
        ,"angular-carousel", "angular-google-maps", "angular-gestures"
        , './Services', './Directives'
		], 
	function( ng, Constants, ModuleNameFetcher, Util, AppCtrl
        ,HomeCtrl,          HomePartial
        ,GameCtrl,          GamePartial
        ,GalleryCtrl,       GalleryPartial
        ,NewslettersCtrl, NewslettersPartial
        ,ContactCtrl, ContactPartial
        //{{GENERATOR_INSERT_PAGE_ARGS}}
		) {

    /**
     * App Module
     */

    var ieVersion = ( navigator.userAgent.toLowerCase().indexOf( 'msie' ) != -1 // checks if it's IE
            ? document.documentMode // gets the version of IE from the docmode
            : false ); // not IE

	var app = ng.module( Constants.baseName + "Module", [ "ngRoute", "ngAnimate", "ngSanitize"
                ,"ngTouch" // Supposed to remove 'touch' delay on click. I think 'angular-gestures' overrides it anyway.
                ,"angular-carousel", "angular-gestures" ]

            // 'angular-google-maps' breaks in IE8. Wouldn't normally browser sniff, but dunno what to feature detect. 
            .concat( ieVersion === false || ieVersion > 8 ? ["google-maps"] : [] ) 

            // gets the remaining angular module names from the arguments
            .concat( ModuleNameFetcher.$get().getModuleNames(arguments) )
        )
        .controller( Constants.baseName + "Ctrl", AppCtrl )
        .value('$anchorScroll', angular.noop ) //Stops page from jumping on hash change.
        .config([ '$routeProvider', '$locationProvider', '$animateProvider', 
            function($routeProvider, $locationProvider, $animateProvider) {

                
            $locationProvider.html5Mode(false).hashPrefix("!");
            // $locationProvider.html5Mode(true);


            /**
             * App Router.
             * Please see main README.md under "Router -> Deep links" for useful tips.
             */
            var HOME = "/home";

            $routeProvider.when( HOME,      { template: HomePartial,    controller: HomeCtrl });
            $routeProvider.when( "/game",   { template: GamePartial,    controller: GameCtrl });
            $routeProvider.when( "/gallery", { template: GalleryPartial, controller: GalleryCtrl });
            $routeProvider.when( "/about-me", { template: NewslettersPartial, controller: NewslettersCtrl });
            $routeProvider.when( "/contact", { template: ContactPartial, controller: ContactCtrl });
            //{{GENERATOR_INSERT_PAGE_CONF}}

            $routeProvider.otherwise( { redirectTo: HOME } );


            /**
             * Custom 'do-animate' class must be added to any animations you want in this module.
             * Stops nested elements getting animated unintentionally.
             * Had this problem with Garden Flip intro panel.
             */
            $animateProvider.classNameFilter(/do-animate/);
        }]);

    return app;
});
/**
 * This script gets used for both dev environment and Grunt RequireJS build.
 * File name must be "NGBootstrap.js".
 */

// Can't use relative paths on the bootstrap require
require( [ "domready", "angular", "CORE/js/Constants", "CORE/js/App"], 
	function( domReady, ng, 	   Constants, 		   App ) {
	

    /**
     * Only run logic and load app if not looking at PhantomJS snapshot.
     * If _escaped_fragment_ is in url, the page is only meant for bots.
     */
    if( window.location.pathname.indexOf("_escaped_fragment_") != -1 ) return;

    domReady(function() {
        
        /**
         * Adding 'data-status' attribute for PhantomJS to detect.
         * Using standard jQuery for 'body' selector, as jQueryLite doesn't include selectors,
         * only has $.find(".myselector") which is fine for most cases, but not for anything outside of the app.
         */
        document.body.setAttribute('data-status', 'pending');

        ng.bootstrap(document, [ App.name ]);
        ng.resumeBootstrap();
    });
});
define("../../_dev/app/NGBootstrap.js", function(){});

