# sunshinefdc.com.au


## Installation
Make sure you do `npm install` from command-line in appication root. 
Some of the testing dependencies need admin rights, so if on OSX, do `sudo npm install` and if Windows, run cmd as 'administator'.


## CSS dependencies
Sass 3.2.17 (not higher)
Compass 0.12.2 (not higher)


## Good polyfill resource
https://github.com/Modernizr/Modernizr/wiki/HTML5-Cross-Browser-Polyfills

## Unit tests
The RequireJS Config only gets configured once in the app here, but gets used in 3 places:
- Local dev environment
- Grunt RequireJS build and 
- Karma unit tests.

The RequireJS Config should live here '_dev/app/RJSConfig.js'. Changing the original directory structure will break the build and tests.



## Router

### Deep links

Inside "_dev/app/core/js/App.js" you have the router for the app and a line that says
```js
$locationProvider.html5Mode(false).hashPrefix("!");
```

If not using an "?escape_fragment" HTML snapshot service you should only use #, or else facebook will ignore all deeplinks.
"html5Mode" true removes the #! and # altogether, but doesn't work on ie8.
Must include this snippet in the document <head>, so that the "?escape_fragment" knows what to do.
```html
<meta name="fragment" content="!" />
```

If just using # url, rather than #!, you can use this snippet to detect if #! is used by accident and replace it.
```js
window.location.href = window.location.href.replace("!", "");
```

If using # urls, need to add trailing slashes to stop controllers being called twice.
Might have something to do with requirejs text! plugin.

Both these are good:
example.com.au/#/home/
example.com.au/#!/home


## Git subtrees
Uses Git Subtree for 'externals', including:
- angular-services-amd
- ngui/GardenFlip

### How to push/pull
Root has .bat files that contain the details for pushing and pulling each subtree.

### How to use Git Subtree
See root repo (link below) README.md for more info on this.
`https://jimdoyle82@bitbucket.org/jimdoyle82/sunshinefdc.com.au-root.git`