
define( [ "angular", "lodash"], function( ng, _ ) {
    'use strict';

    /**
     * Utility methods for strings and text related things.
     */

    var NAME = "stringUtil";;

    return {
        
        $get: [ "selectorUtil", function( su ) {
    
                return {
    
                    name: NAME
        
                    /**
                     * Returns the inner html of the selector arg with each character wrapped in it's own span tag.
                     * If you actually intend on using the markup returned, Angular 1.2.x uses $sce 
                     * (Scrict Contextual Escaping) by default, which needs the js dep 'ngSanitize' module to be included 
                     * in the project and 'ng-bind-html' to be used in the markup.
                     */
                    ,getSeparateChars: function( el, selector ) {

                        // if length is undefined, means that the element is not wrapped in jQuery
                        if( el.length != undefined ) el = el[0];

                        var targetEl = ( selector ? su.find( selector, el, false, true ) : el )
                            ,markup = ""
                            ,chars;

                        _.forEach( targetEl.innerHTML, function( ch, i ) {

                            chars = ch.split(" ").join("&nbsp;");
                            markup += '<span class="ch'+(i+1)+'">'+chars+'</span>';
                        });

                        return markup;
                    }


                    // jquery dependency for trim. Could be easily replaced.
                    ,countWords: function( text, $jq ) {
                        var regex = /\s+/gi;
                        var wordCount = $jq.trim(text) // use jquery because String.trim is ES5 only
                                            .replace(regex, ' ')
                                            .replace(/\.,/g,"") // remove dots
                                            .replace(/ +(?= )/g,'') //remove double spaces
                                            .split(' ').length;
                        return wordCount;
                    }


                    // jquery dependency for trim and each. Could be easily replaced.
                    ,getTextUpToWordLimit: function( text, wordLimit, $jq ) {

                        var regex = /\s+/gi;
                        var wordArr = $jq.trim(text) // use jquery because String.trim is ES5 only
                                            .replace(regex, ' ')
                                            .replace(/\.,/g,"") // remove dots
                                            .replace(/ +(?= )/g,'') //remove double spaces
                                            .split(' ');

                        var rtnText = "";
                        $jq.each( wordArr, function(key, val) {

                            if( key < wordLimit )   rtnText += ' ' + val;
                            else                    return false; // breaks loop
                        });

                        return rtnText + ' ';
                    }

                    ,replaceStringBetween: function( rxStart, rxEnd, originalString, replacementString ) {
                        // Notes:
                        // 1. In this expression "\\d\\D" makes sure line breaks are included
                        // 2. And "*?" means that everything will be replaced between
                        // 3. "g" stands for global, which means it will look through the entire string
                        // 4. If you want to escape a "/", you must make it "\/"
                        // 5. If you want to escape an "*", you must make it "\\*"

                        var rx = new RegExp( rxStart + "[\\d\\D]*?" + rxEnd, "g");
                        return originalString.replace( rx, replacementString );
                    }

                    ,toSentenceCase: function(str) {
                        return str.substr(0,1).toUpperCase() + str.substr(1).toLowerCase();
                    }

                    ,toCamelCase: function(str, makeTitleCase, includeSpaces){
                        return str.split(/\s+/).map(function(word, index){
                            word = word.toLowerCase();
                            if(index !== 0 || makeTitleCase === true){
                                return word.split('').map(function(char, i){
                                            if(i===0){
                                                return char.toUpperCase();
                                            }
                                            return char;
                                            
                                        }).join('')
                            }
                            return word;
                            
                        }).join(includeSpaces === true ? ' ' : '');
                    }
                }
            }]
    }
});