
define( function() {
    // 'use strict';

    /**
     * Utility methods for debugging.
     */

    var NAME = "debugUtil";

    return {
        
        $get: function() {
    
            return {

                name: NAME

                // priority 1 (important)
                ,p1: function( msg1, msg2, msg3, msg4 ) {

                    var logArr  = [ msg1 ];

                    if( msg2 ) logArr.push( msg2 );
                    if( msg3 ) logArr.push( msg3 );
                    if( msg4 ) logArr.push( msg4 );

                    // gets passed grunt-remove-logging task
                    if( console && console.log )
                        console['log']( logArr );
                }
            }
        }
    }
});