
define( ["lodash"], function( _ ) {
    'use strict';

    return {
        
        $get: ['$route', function( $route ) {
            return {

                name: "util"

                /**
                 * getRoute() allows for dynamic page urls to be changed globally
                 */
                ,getRoute: function( controllerName ) {

                    // console.log( "controllerName", controllerName );

                    //handle the case when the user has come from facebook with no hashbang
                    var route="#";
                    if (controllerName) {
                        _.each( $route.routes, function(name, val) {
                            if( val.controller === controllerName ) {
                                route += name
                                return false;
                            }
                        });
                    } else {
                        return route;
                    }

                    return route;
                }

                ,refreshSameArray: function( originalArr, newArr, addToExisting ) {

                    /**
                      * For cases where you need to clear your array and add new items,
                      * but you can't just do arr = [] because it has data-bindings that
                      * will be lost otherwise.
                      */

                    if( !addToExisting ) originalArr.length = 0;

                    var i=0,
                        len = newArr.length;
                    for( ; i<len; i++ ) {
                        originalArr.push( newArr[i] );
                    }
                }

                ,logWarning: function( warningMsg ) {
                    if( window.console && window.console.warn ) {
                        window.console.warn( warningMsg );
                    } else if( window.console && window.console.log ) {
                        window.console.log( warningMsg );
                    } else {
                        alert( warningMsg );
                    }
                }

                /**
                 * Below here taken from jquery.jimd.utils.common.v0.1.5.js
                 */

                ,type: function ( test ) {
                    return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
                }

                ,supports_canvas: function() {
                    return !!document.createElement('canvas').getContext;
                }

                ,supportsCSSAni: function() {
                    // taken from https://developer.mozilla.org/en-US/docs/Web/Guide/CSS/Using_CSS_animations/Detecting_CSS_animation_support
                    var animation = false,
                        animationstring = 'animation',
                        keyframeprefix = '',
                        domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
                        pfx  = '';

                    if( document.body.style.animationName ) { animation = true; }

                    if( animation === false ) {
                        for( var i = 0; i < domPrefixes.length; i++ ) {
                            if( document.body.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
                                pfx = domPrefixes[ i ];
                                animationstring = pfx + 'Animation';
                                keyframeprefix = '-' + pfx.toLowerCase() + '-';
                                animation = true;
                                break;
                            }
                        }
                    }

                    return animation;
                }

                ,canvasWrapText: function(context, text, x, y, maxWidth, lineHeight) {
                    // Creates automatic line breaks in canvas text, based on canvas width
                    var words = text.split(' ');
                    var line = '';

                    for(var n = 0; n < words.length; n++) {
                        var testLine = line + words[n] + ' ';
                        var metrics = context.measureText(testLine);
                        var testWidth = metrics.width;
                        if(testWidth > maxWidth) {
                            context.fillText(line, x, y);
                            line = words[n] + ' ';
                            y += lineHeight;
                        }
                        else {
                            line = testLine;
                        }
                    }
                    context.fillText(line, x, y);
                }

                ,isIOS: function(){

                    var i = 0,
                        iOS = false,
                        iDevice = ['iPad', 'iPhone', 'iPod'];

                    for ( ; i < iDevice.length ; i++ ) {
                        if( navigator.platform === iDevice[i] ){ iOS = true; break; }
                    }

                    return iOS;
                }

                // lum is a decimal - positive = lighter - negative = darker
                ,colorLuminance: function(hex, lum) {
                    // validate hex string
                    hex = String(hex).replace(/[^0-9a-f]/gi, '');
                    if (hex.length < 6) {
                        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
                    }
                    lum = lum || 0;
                    // convert to decimal and change luminosity
                    var rgb = "#", c, i;
                    for (i = 0; i < 3; i++) {
                        c = parseInt(hex.substr(i*2,2), 16);
                        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                        rgb += ("00"+c).substr(c.length);
                    }
                    return rgb;
                }

                ,setArgb: function(val) {
                    var valArr = val.split("(")[1].split(")")[0].split(","),
                        red     = _utils.toHex(valArr[0]),
                        green = _utils.toHex(valArr[1]),
                        blue    = _utils.toHex(valArr[2]),
                        alpha = _utils.toHex(valArr[3]*255);
                    return "#" + alpha + red + green + blue;
                }

                ,getHexFromRGB: function(val) {
                    var valArr = val.split("(")[1].split(")")[0].split(","),
                        red     = _utils.toHex(valArr[0]),
                        green = _utils.toHex(valArr[1]),
                        blue    = _utils.toHex(valArr[2]);
                    return "#" + red + green + blue;
                }

                ,setHex: function(val) {
                    var value = val.substring(1,9),
                        red = parseInt(value.substring(2,4), 16),
                        green = parseInt(value.substring(4,6), 16),
                        blue = parseInt(value.substring(6,8), 16),
                        alpha = Math.round((parseInt(value.substring(0,2), 16)/255)*10)/10;
                    return "rgba(" + red + "," + green + "," + blue + "," + alpha +")";
                }

                ,toHex: function(val) {
                    val = parseInt(val); // might need a radix of 16 (2nd param) - needs testing
                    val = Math.max(0,val);
                    val = Math.min(val,255);
                    val = Math.round(val);
                    return "0123456789ABCDEF".charAt((val-val%16)/16) + "0123456789ABCDEF".charAt(val%16);
                }

                ,sniffIE: function( $jq ) {

                    if( $jq && $jq.browser ) {
                        if( $jq.browser.msie )    return $jq.browser.version;
                        return false;
                    }

                    if( navigator.userAgent.toLowerCase().indexOf( 'msie' ) != -1 ) return document.documentMode;
                    return false;
                }
            }
        }]
    }
});