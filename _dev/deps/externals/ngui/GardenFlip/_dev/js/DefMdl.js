
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "gardenFlip"; // must be camelCase
    
    var Mdl = {
        paused: true // if true, will still show the overlay bg
        ,showIntroPanel: false // if false, will hide the intro panel, but still show the white overlay bg
        ,isActive: false // should be true when game is showing, regardless of paused state
        ,found:0
        ,introComplete: false
        ,allTokensFound: true // set to true for debugging replay panel
        ,bgfallback: false
        ,countDownMax: 3
        ,isCounting: false
        ,countDownCurrent: -1
        ,hmTapRayOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
        ,hmTapHandOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1200 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 400 // kids aren't too acurate, so give them some slack
        }
        ,hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
    };

    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME; // if no args, just return BASE_NAME
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});