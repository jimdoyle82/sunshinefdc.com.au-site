
define([ "./DefMdl", "lodash", 'modernizr' ], function( mdl, _, Modernizr ) {
    "use strict";

    return [ '$scope', '$rootScope', '$element', '$sce', '$timeout', '$animate', 'selectorUtil', "animationUtil", "debugUtil"
        ,function ( $scope, $rootScope, $element, $sce, $timeout, $animate, su, au, du ) {
        var Mdl = $scope.Mdl = $scope.inhMdl; // A non-extensible $scope.Mdl.

        // make lodash available on the template
        $scope._ = _;

        if( Mdl.bgfallback ) {

            /**
             * If the 'bgfallback' is true, game is disabled.
             * However, the bg of the intro panel still needs to hide and show.
             * This happens on events being called from the parent app, which we listen to here.
             */

            $rootScope.$on( "gflipIntroPause", function(evt, data) {
                if( data.showIntroPanel === false ) Mdl.paused = true;
            });

            $rootScope.$on( "gflipStart", function(evt) {
                Mdl.paused = false;
            });

            // Don't want the rest of the logic.
            return;
        }


        var TokenMap = $scope.TokenMap = [
             { tokenCls:"ngClsLadybird" }
            ,{ tokenCls:"ngClsParsnip" }
            ,{ tokenCls:"ngClsFishRight" }
            ,{ tokenCls:"ngClsPear" }
            ,{ tokenCls:"ngClsMoon" }
            ,{ tokenCls:"ngClsMushroomBlue" }
            ,{ tokenCls:"ngClsCarrot" }
            ,{ tokenCls:"ngClsBug" }
            ,{ tokenCls:"ngClsFishLeft" }
            ,{ tokenCls:"ngClsApple" }
            ,{ tokenCls:"ngClsMushroomRed" }
            ,{ tokenCls:"ngClsChicken" }
        ]

        var msgTypes = {
            RESUME: "msgTypes.RESUME"
            ,PLAY: "msgTypes.PLAY"
            ,REPLAY: "msgTypes.REPLAY"
            ,COUNTDOWN_START: "msgTypes.COUNTDOWN_START"
            ,COUNTDOWN_COUNTING: "msgTypes.COUNTDOWN_COUNTING"
            ,COUNTDOWN_COMPLETE: "msgTypes.COUNTDOWN_COMPLETE"
        }

        var countDownIntv, replayTimeout;

        init();

        function init() {

            /**
             * Because nested ngShow wasn't working we force the element to show, with 'panelShow = true', when animation starts.
             * Then, inside 'setNestedShowHandler' we hide the element with 'panelShow = false', after it has ended.
             */
            $scope.$watch( "Mdl.showIntroPanel", function(val) {
                if( Mdl.isActive === true && val === true ) {
                    $scope.panelShow = true;
                    $scope.introShow = true;
                }
                
            });
            
            setNestedShowHandler();

            $scope.ngClsBtn1 = [];

            $rootScope.$on( "gflipIntroStart", function() {
                resetCountdown( true );
            });

            $rootScope.$on( "gflipStart", function(evt) {
                $scope.introShow = false;
                console.log( "gflipStart" );
            });

            // gets called after same event handler in MainCtrl.js
            $rootScope.$on( "gflipIntroPause", function(evt, data) {

                Mdl.showIntroPanel = data.showIntroPanel || false;

                console.log("gflipIntroPause");
                // if( Mdl.isActive === true ) {
                // if( Mdl.showIntroPanel === false ) {
                    $scope.introShow = true;
                // }

                // console.log( "intro Mdl.isActive", Mdl.isActive );
                // console.log( "intro Mdl.showIntroPanel", Mdl.showIntroPanel );
                // console.log( "intro Mdl.paused", Mdl.paused );

                // if game not showing, no nothing
                if( Mdl.isActive === false ) return;

                // if Mdl.allTokensFound is true, it means the "Replay?" panel is showing, so we don't want to change anything
                if( Mdl.allTokensFound === true ) return;

                if( Mdl.isCounting === true ) {
                    resetCountdown( false, false, data.skipPageNavCollapse || false );

                    //needs timeout because counter has one inside "resetCountdown" method
                    // $timeout( function() { 
                        setMsg( msgTypes.PLAY );
                    // }, 0);
                } else {
                    setMsg( msgTypes.RESUME );
                }
            });

            /**
             * Should add any game start logic watchers, just things like interval cancellers and non-infinite css anis.
             * Things can get messy if too much logic is placed on a watch.
             */

            $scope.$watch( "Mdl.allTokensFound", function(val) {
                if( val )   onAllTokensFound();
                // else        setMsg( msgTypes.COUNTDOWN_START );
            });

            
        }


        // This button can be either "Play", "Replay" or "OK" (resume).
        $scope.btn1Clicked = function() {

            /** 
             * If count down is over, meaning that the game has started, 
             * but not all the tokens have been found yet, resume the game.
             */
            if( Mdl.isCounting === false && Mdl.allTokensFound === false ) {
                resumeGame();
                return;
            }

            /**
             * "Play" and "Replay" do the same thing.
             * "Play" shows up only if paused before count down has finished.
             * "Replay" shows up when all the tokens have been found.
             */
            newGame();
        }


        $scope.btn2Clicked = function() {
            // So far, btn2 is always a "reset" btn
            newGame();
        }


        function newGame() {
            du.p1( "New Game" );
            resetTokens();
            
            _.remove( $scope.ngClsBtn1, function(cls) {
                return cls === "is-twirling";
            });

            resetCountdown( true );
        }


        function resumeGame() {
            du.p1( "Resuming Game" );
            Mdl.paused = false;
            Mdl.showIntroPanel = false;
            resetCountdown();
        }


        function onAllTokensFound() {
            setMsg( msgTypes.REPLAY );

            Mdl.paused = true;
            Mdl.showIntroPanel = true;
            Mdl.introComplete = false;
        }



        function resetCountdown( doStart, resetDelay, skipPageNavCollapse ) {
            if( countDownIntv ) clearInterval( countDownIntv );

            Mdl.countDownCurrent = Mdl.countDownMax;
            setMsg( msgTypes.COUNTDOWN_COUNTING, Mdl.countDownMax );

            if( !skipPageNavCollapse ) collapsePageNav();

            if( !doStart ) {
                Mdl.isCounting = false;
                return;
            }

            setMsg( msgTypes.COUNTDOWN_START, Mdl.countDownMax );
            resetTokens();
            Mdl.isCounting = true;
            Mdl.showIntroPanel = true;
            $scope.panelShow = true;
            $scope.introShow = true;

            countDownIntv = setInterval( function() {
                $scope.$apply(function() { countByIncr(); });
            }, 1000 );

        }


        function resetTokens() {
            Mdl.found = 0;
            Mdl.allTokensFound = false;
        }


        function collapsePageNav() {
            // debugger
            $rootScope.$broadcast( "expandPageNav", { expandState: false } );
        }


        function countDownComplete() {
            setMsg( msgTypes.COUNTDOWN_COMPLETE );

            Mdl.showIntroPanel = false;
            Mdl.introComplete = true;
            Mdl.paused = false;
        }


        function countByIncr( incr ) {

            if( !incr ) incr = -1;

            Mdl.countDownCurrent += incr;

            if( Mdl.countDownCurrent > 0 )
                setMsg( msgTypes.COUNTDOWN_COUNTING, Mdl.countDownCurrent );
            else
                countDownComplete()

            if( Mdl.countDownCurrent === -1 ) resetCountdown( false, 3000 );
        }


        function setMsg( type, msg ) {

            if( msg ) msg = ( typeof msg === "string" ? msg : msg.toString() );

            switch( type ) {
                case msgTypes.RESUME:
                    $scope.mainMsg = "Continue playing?";
                    $scope.playBtnMsg = $sce.trustAsHtml( "OK" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnplay", "btnmed", "is-twirling" );
                    $scope.showBtn2 = true;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return cls === "l-gflipintro-btnresume" || cls === "l-gflipintro-btnreplay" || cls === "btnbig";
                    });

                    du.p1( "RESUME", $scope.ngClsBtn1 );
                    break;

                case msgTypes.PLAY:
                    $scope.mainMsg = "Start playing?";
                    $scope.playBtnMsg = $sce.trustAsHtml( "OK" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnresume", "btnbig", "is-twirling" );
                    $scope.showBtn2 = false;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return cls === "l-gflipintro-btnplay" || cls === "l-gflipintro-btnreplay" || cls === "btnmed";
                    });

                    du.p1( "PLAY", $scope.ngClsBtn1 );
                    break;

                case msgTypes.REPLAY:
                    $scope.mainMsg = "Woohoo! All tokens found!!";
                    $scope.playBtnMsg = $sce.trustAsHtml( "Replay?" );
                    $scope.ngClsBtn1.push( "l-gflipintro-btnreplay", "is-twirling" );
                    $scope.showBtn2 = false;

                    _.remove( $scope.ngClsBtn1, function(cls) {
                        return  cls === "l-gflipintro-btnplay" || cls === "l-gflipintro-btnresume" || 
                                cls === "btnbig" || cls === "btnmed";
                    });

                    du.p1( "REPLAY", $scope.ngClsBtn1 );
                    break;

                case msgTypes.COUNTDOWN_START:
                    $scope.ngClsBtn1.length = 0;
                    $scope.mainMsg = "Find the tokens!!";
                    $scope.showBtn2 = false;
                    $scope.ngClsCountDownMsg = "";

                    du.p1( "COUNTDOWN_START", $scope.ngClsBtn1 );
                    break;

                case msgTypes.COUNTDOWN_COUNTING:
                    $scope.ngClsBtn1.length = 0;
                    $scope.countDownMsg = $sce.trustAsHtml( msg );

                    du.p1( "COUNTDOWN_COUNTING", $scope.ngClsBtn1, msg );
                    break;

                case msgTypes.COUNTDOWN_COMPLETE:
                    $scope.ngClsBtn1.length = 0;
                    $scope.countDownMsg = $sce.trustAsHtml( "Go!!" );
                    $scope.ngClsCountDownMsg = "go-title";
                    
                    du.p1( "COUNTDOWN_COMPLETE", $scope.ngClsBtn1 );
                    break;
            }
        }

        // Removes animation classes after they have completed so that reverse class can be added
        function setNestedShowHandler() {            
            var $el = su.find(".gflipintro-panel", $element, false, true);
            au.removeClassOnAnimationComplete( $el, "nested-show", false, $scope);
            au.removeClassOnAnimationComplete( $el, "nested-hide", false, $scope, function(clsRemoved, failedClsRemove) {
                if( clsRemoved && clsRemoved === "nested-hide" ) {
                    $scope.panelShow = false;
                    
                    if( Mdl.isActive ) $scope.introShow = false;
                }
            });
        }
    }];
});
