
define([ "angular", "./DefMdl", "lodash", "modernizr" ], function( ng, mdl, _, Mdnzr ) {
    "use strict";

    return [ '$scope', '$rootScope', '$element', '$timeout', '$window', "selectorUtil", "stringUtil", "animationUtil", "debugUtil", 
        function ( $scope, $rootScope, $element, $timeout, $window, selUtil, strUtil, aniUtil, du ) {
        var Mdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        // make lodash available on the template
        $scope._ = _;

        // Tells the directive to only show the fallback bg image if any of the conditions fail
        if( !Mdnzr.cssanimations || !Mdnzr.csstransforms || !Mdnzr.csstransitions || !Mdnzr.svg ) {
            Mdl.bgfallback = true;

            // Don't want the rest of the logic.
            return;
        }


        var TokenMap = $scope.TokenMap = [
             { id:"LadyBird",       map:"gflip-raynth0",    wander: "med" }
            ,{ id:"Parsnip",        map:"gflip-raynth1",    wander: "med" }
            ,{ id:"FishRight",      map:"gflip-raynth2",    wander: "long" }
            ,{ id:"Pear",           map:"gflip-raynth3",    wander: "short" }
            ,{ id:"Moon",           map:"gflip-raynth4",    wander: "short" }
            ,{ id:"MushroomBlue",   map:"gflip-raynth5",    wander: "short" }
            ,{ id:"Carrot",         map:"gflip-raynth6",    wander: "short" }
            ,{ id:"Bug",            map:"gflip-raynth7",    wander: "short" }
            ,{ id:"FishLeft",       map:"gflip-raynth8",    wander: "med" }
            ,{ id:"Apple",          map:"gflip-raynth9",    wander: "short" }
            ,{ id:"MushroomRed",    map:"gflip-hand-left",  wander: "short" }
            ,{ id:"Chicken",        map:"gflip-hand-right", wander: "long" }
        ]

        var wanderInvt
            ,wanderIncr
            ,wanderAutoCount
            ,wanderAutoMaxCount = 2
            ,wanderOrder = [];

        _.forEach( TokenMap, function(map, i) { wanderOrder[i] = i; });


        init();

        $scope.handClick = function(name) {
            if( Mdl.paused ) return;

            // $scope[name+'ArmAni'] = '';

            // adds a tiny delay between removing and adding again. Couldn't use $apply because it's already in progress.
            // $timeout(function() {
                $scope[name+'ArmAni'] = 'arm-is-raising';
            // }, 0);

            var map = _.where( TokenMap, { map:"gflip-hand-"+name })[0];
            tokenTargeted( map, ( name == 'left' ? '10' : "11" ) );
        }


        $scope.rayClick = function(i) {
            if( Mdl.paused ) return;

            // $scope['rayAni'+i] = '';
            $scope['rayAni'+i] = 'ray-is-opening';

            // adds a tiny delay between removing and adding again. Couldn't use $apply because it's already in progress.
            /*$timeout(function() {
                $scope['rayAni'+i] = 'ray-is-opening';
            }, 0);*/

            var map = _.where( TokenMap, { map:"gflip-raynth"+i })[0];
            tokenTargeted( map, i );
        }


        function init() {
            // separates characters into span tags so that they can be rotated individually
            $scope.heading1Text = strUtil.getSeparateChars( $element, ".gflip-mainheading .line1" );
            $scope.heading2Text = strUtil.getSeparateChars( $element, ".gflip-mainheading .line2" );
            manageHandDepths( 'left' );
            manageHandDepths( 'right' );

            removeWandersOnComplete();
            $timeout( removeRayHandlerOnComplete, 0); // needs timeout because classes are created dynamically

            $rootScope.$on( "gflipStart", function(evt) {

                Mdl.isActive = true;

                if( Mdl.introComplete === true ) {
                    // resume game
                    Mdl.showIntroPanel = false;
                    Mdl.paused = false;
                    startWanderTicker( false, true, true );
                } else {

                    // start afresh
                    startWanderTicker( true );
                    $rootScope.$broadcast( "gflipIntroStart" );
                }
            });



            $rootScope.$on( "gflipIntroPause", function(evt, data) {

                // if game not showing, no nothing
                if( Mdl.isActive === false ) return;

                if( !data.showIntroPanel ) Mdl.isActive = false;

                // if already paused, the watch on Mdl.pause won't get triggered, so we trigger the same function
                if( Mdl.paused === true )   onPause();
                else                        Mdl.paused = true;
            });


            /**
             * Shouldn't add any game start logic watchers, just things like interval cancellers and non-infinite css anis.
             * Things can get messy if too much logic is placed on a watch.
             */

            $scope.$watch( "Mdl.found", function(val) {
                if( val === 0 ) resetTokenClasses();
            });

            $scope.$watch( "Mdl.paused", function(val) {
                if( val === true ) onPause();
            });
        }


        function onPause() {
            removeWanderingAni( true );
            stopWanderTicker();
        }

        // when a consealer (eg. a hand or sun ray) is clicked
        function tokenTargeted( map, i ) {

            if( map.isFound ) {
                // no need for any token changes if token has already been found
                return;
            } else {
                Mdl.found++;
                map.isFound = true;
            }

            $scope[ map.tokenCls ].push( "token-is-found-slot"+i ); //overrides wander
            $scope[ map.tokenCls + "Outter" ].push( "token-is-found" );

            removeWanderingAni( true );

            var delayedRestart = Mdl.found < TokenMap.length;
            stopWanderTicker( true, delayedRestart );

            if( !delayedRestart ) {
                du.p1( "All tokens found" );
                Mdl.allTokensFound = true;
            }
        }


        // sets some initial bindings
        function resetTokenClasses() {
            _.forEach( TokenMap, function(map, i) {

                // creates new properties on map
                map.tokenId = "ngId" + map.id;
                map.tokenCls = "ngCls" + map.id;

                map.isFound = false;

                $scope[ map.tokenId ] = map.id;
                $scope[ map.tokenCls ] = [];
                $scope[ map.tokenCls + "Outter" ] = ["gflip-" + map.id.toLowerCase() ]; // this will become obselete
            });
        }


        /** 
          * Sets a handler for when the wander animations have finished, this finds the parent element and 
          * fetches its "data-id" value, which is saved in the TokenMap, before triggering removeWanderingAni().
          */
        function removeWandersOnComplete() {
            
            var el = selUtil.find(".gflip-rays", $element, false, true);
            aniUtil.prefixedEvent( el, "AnimationEnd", function(evt) {

                // feels like this could be improved

                var tgt = evt.target
                    ,prnt;

                if( tgt.classList.contains("inner") ) {
                    if( tgt.parentElement.classList.contains("gflip-token") ) {
                        prnt = tgt.parentElement;
                    } else if( tgt.parentElement.parentElement.classList.contains("gflip-token") ) {
                        prnt = tgt.parentElement.parentElement;
                    }
                    
                    removeWanderingAni( false, prnt.parentElement.attributes["data-id"].value );
                }
                
            }, $scope);
        }


        
        // If doAll is passed, removes ani from all tokens, otherwise uses id for single token
        function removeWanderingAni( doAll, id ) {

            var mappedToken,arr;

            if( doAll ) {
                arr = TokenMap;
            } else if( id )   {
                arr = [ _.where( TokenMap, {id:id})[0] ];
            }

            if( !arr ) {
                if( console && console.warn )
                    console.warn("gflip -> MainCtrl -> removeWanderingAni -> token arr is empty");
                return;
            }

            _.forEach( arr, function(mappedToken, i) {
                
                // removes "token-is-wandering-..." ani classes from the token
                _.remove( $scope[ mappedToken.tokenCls ], function(item) {
                    return item.indexOf("token-is-wandering-") === 0;
                });
            });
        }


        // Stops the ticker controlling the wander animation
        function stopWanderTicker( doReset, delayedRestart ) {

            if( doReset ) {
                wanderAutoCount = 0;
                wanderIncr = 0;
            }

            if( wanderInvt ) clearInterval( wanderInvt );

            if( delayedRestart ) {
                wanderIncr = -1; // wait this many seconds before restart
                startWanderTicker();
            }
        }


        // Starts the ticker controlling the wander animation
        function startWanderTicker( doReset, startImmediately, skipShuffle ) {
            
            if( doReset ) {
                wanderAutoCount = 0;
                wanderIncr = 0;
            }
            
            // Choose NOT to reshuffle the token order with "skipShuffle"
            if( !skipShuffle ) wanderOrder = _.shuffle(wanderOrder);

            // Choose to start immediately, rather than wait for the initial timeout to trigger
            if( startImmediately ) wanderTicker();

            // Make sure the interval has been cleared before setting it again to avoid lost interval defs
            if( wanderInvt ) clearInterval( wanderInvt );
            wanderInvt = setInterval( function() {
                $scope.$apply( wanderTicker );
            }, 3000);
        }


        function wanderMaxIterationReached() {
            // if the maximum amount of "auto" wanders is reached, stop the ticker
            if( wanderAutoCount >= wanderAutoMaxCount ) {
                stopWanderTicker( true );
                du.p1( "auto wander stopped" );
                return
            }
            
            // otherwise keep ticking
            wanderAutoCount++;
            wanderIncr = 0;
        }


        function wanderTicker() {
            
            // if negative, delay the wander until positive
            if( wanderIncr < 0 ) {
                wanderIncr++;
                return;
            }

            // If all available tokens have been iterated through, 
            if( wanderIncr >= wanderOrder.length ) {
                wanderMaxIterationReached();
                return;
            }

            var map = TokenMap[ wanderOrder[wanderIncr] ];

            // if the token has already been found, skip to the next token in the list, without delay
            if( map.isFound ) {
                wanderIncr++;
                stopWanderTicker();
                startWanderTicker( false, true, true );
                return;
            }

            // Add the wander class if it doesn't exist already
            if( $scope[ map.tokenCls ].indexOf("token-is-wandering-"+map.wander ) === -1 )
                $scope[ map.tokenCls ].push( "token-is-wandering-"+map.wander );

            // tick
            wanderIncr++;
        }


        /**
         * Listens for animation events and adds a class to manage depth.
         * Needs a timeout because the events are non-angular
         */
        function manageHandDepths( name ) {
            
            var $el = selUtil.find(".gflip-hand-"+name, $element, false, true);

            var allowChildEvents = true; // Because hits areas are child events, we must allow them
            aniUtil.prefixedEvent( $el, "AnimationStart", function() {
                
                // couldn't use $scope.$apply here because says digest is already in progress (FF)
                $timeout( function() {
                    $scope[name+'ArmAniDepth'] = 'arm-raising-hidepth';
                }, 0);    
            }, null, allowChildEvents);


            /**
             * Didn't use 'aniUtil.removeClassOnAnimationComplete' because the classes needed to be removed
             * from bound variables. Last 'true' param allows child events, because hit area is a child.
             */
            aniUtil.prefixedEvent($el, "AnimationEnd", function(evt) {
                $scope[name+'ArmAniDepth'] = "";
                $scope[name+'ArmAni'] = "";
            }, $scope, true );
        }


        function removeRayHandlerOnComplete() {

            /**
             * Didn't use 'aniUtil.removeClassOnAnimationComplete' because the classes needed to be removed
             * from bound variables. Last 'true' param allows child events, because hit area is a child.
             */
            _.forEach( TokenMap, function( tknMap, i ) {

                // don't want to do the hands. They are handled in 'manageHandDepths()'.
                if( i < 10 ) {
                    var $el = selUtil.find( "." + tknMap.map, $element, false, true );
                    // console.log( $el.length, tknMap.map );

                    aniUtil.prefixedEvent($el, "AnimationEnd", function(evt) {

                        // console.log( "ray target", i,  evt.target )
                        $scope['rayAni'+i] = "";
                    }, $scope, true );
                }
            });

        }

    }];
});
