
define([ "./MainCtrl", "./IntroCtrl", "./DefMdl" // relative js
	,"text!D_GARDEN_FLIP/mainTemplate.html", "text!D_GARDEN_FLIP/introTemplate.html" // html partials
	,"angular-animate", 'angular-sanitize', "angular-gestures" // js deps
	], function( MainCtrl, IntroCtrl, Mdl, mainTemplate, introTemplate ) {

	var introModuleName = "gflipIntroModule";
	angular.module( introModuleName, [ "ngAnimate", "ngSanitize", "angular-gestures"] ).directive( "gflipIntro", function() {

		return {
			controller: IntroCtrl
			,template: introTemplate
			,replace: true
			,scope: {
				inhMdl: "=mdl" // inherited model - two-way binding "="
			}
		}
	});


	return angular.module( Mdl() + "Module", [ "ngAnimate", "ngSanitize", "angular-gestures", introModuleName ] ).directive( Mdl(), function() {
	        return {
				 controller: MainCtrl
				,template: mainTemplate
				,replace: true
				,scope: {
					inhMdl: "=mdl" // inherited model - two-way binding "="
				}
	        }
		});
});