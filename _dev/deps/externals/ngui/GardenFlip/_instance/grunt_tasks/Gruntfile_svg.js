
module.exports = function(grunt) {

    /**
     * Makes everything relative to the "deps" directory, so node_modules can go in "deps" folder.
     */
    // grunt.file.setBase( process.cwd() + "/deps" );

    
    grunt.initConfig({

        pkg: grunt.file.readJSON("../package.json")

        ,vars: {
            root:       "../"
            // ,appdist:   "<%= vars.root %>../../../../../_dist/" //will need fixing
            ,dev:       "<%= vars.root %>_dev/"
            ,tmp:       "../_tmp/"
            ,svgdefsmin: "<%= vars.dev %>img/svgdefs.min.svg"
        }


        ,clean: {
            options: { force: true }
            ,svg:  [ "<%= vars.tmp %>" ]
        }


        ,svgstore: {
            all: {
                options: {
                    prefix : 'gflip-'
                }
                ,files: {
                    "<%= vars.svgdefsmin %>": "<%= vars.tmp %>min/*.svg"
                }
            }
        }


        ,svgmin: {
            all: {
                files: [{
                    expand: true,
                    cwd: '<%= vars.dev %>img/svgdefs',
                    src: ['*.svg'],
                    dest: '<%= vars.tmp %>min/',
                    ext: '.svg'
                }]
            }
        }

    });
        

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-svgmin');

    grunt.registerTask("default", [ "svgmin", "svgstore", "insert-svgdefs", "clean" ] );

    grunt.registerTask("insert-svgdefs", function() {
        
        // Would be good to work out how to do this via git subtree
        // attachSVGDefs( 'CORE', grunt.config.get("vars.appdist") + "img/core/svgdefs.min.svg" );

        attachSVGDefs( 'GFLIP', grunt.config.get("vars.svgdefsmin") );
    });


    /**
     * Helper methods
     */

    function attachSVGDefs( tagId, src ) {

        var tagStart = "<!--GRUNT_INSERT_SVG_"+tagId+"_START-->"
            ,tagEnd = "<!--GRUNT_INSERT_SVG_"+tagId+"_END-->"
            ,filePath = "../_dist/index.html"
            ,svgContents = grunt.file.read( src )
            ,devIndex = grunt.file.read( filePath );

        svgContents = svgContents.replace("<svg><defs>", "<svg class='svgdefs'><defs>");

        var rx = new RegExp( tagStart + "[\\d\\D]*?" + tagEnd, "g");
        devIndex = devIndex.replace( rx, tagStart + "\n" + svgContents + "\n        " + tagEnd );

        grunt.file.write( filePath, devIndex );
    }
};
