
require 'animate-sass'

# Makes paths relative to Gruntfile.js
project_path = "./"

# pop the .sass-cache in the deps dir
cache_dir = "deps/.sass-cache"

# this is where the generated sprite will get saved
images_dir = "_dist/img/sprites-uncompressed/"

# this is where your original images will get fetched from
imagesPath = "../_dev/img/"

# this is the absolute path that the sprites will be for production
http_generated_images_path = "/_instance/img/sprites/"

# disable_warnings = true

# sass_options = { :cache_location => "./deps/.sass-cache", :quiet => true }
