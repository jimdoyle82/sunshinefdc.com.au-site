
define( function() {
    'use strict';

    /**
     * Utility methods for animations.
     */

    var NAME = "animationUtil"
        ,prevEl;

    return {
        
        $get: ["$timeout", function($timeout) {
    
            return {

                name: NAME

                // if $scope is passed, function will wrap callback in a $scope.$apply
                ,prefixedEvent: function(element, type, callback, $scope, allowChildEvents) {

                    if( !element.addEventListener ) {
                        $timeout( function() {
                            if( console && console.warn )
                                console.warn(NAME + " -> prefixedEvent() 'element' does not have property 'addEventListener'.");
                            callback( null );
                        }, 0);
                        return;
                    }

                    var pfx = ["webkit", "moz", "MS", "o", ""];
                    for (var p = 0; p < pfx.length; p++) {
                        if (!pfx[p]) type = type.toLowerCase();

                        // needs to be true for FF and IE. Something to do with bubbling.
                        var useCapture = true;

                        element.addEventListener(pfx[p]+type, function(evt) {

                            // stops children events
                            if( !allowChildEvents && evt.target !== element ) return;

                            if( $scope ) $scope.$apply(function() {callback(evt)});
                            else         callback(evt);

                        }, useCapture);

                    }

                    // so they can be tested
                    return pfx;
                }

                ,removeClassOnAnimationComplete: function( $el, aniClasses, isTransition, $scope, callback, allowChildEvents, removeFromTarget ) {

                    // Using $el, instead of evt.target because event.target could be a child hit area

                    // removes the class from the target or throws warning if doesn't exist
                    var removeCls = function(cls, tgt) {
                        if( tgt.classList.contains(cls) ) {
                            tgt.classList.remove(cls);
                            
                            if( callback ) callback( cls );
                        } else if( console && console.warn ) {

                            // returns 'false' for 'removed class', then tells you which class it means
                            if( callback ) callback( false, cls );
                        }
                    }

                    var tgt;

                    this.prefixedEvent( $el, (isTransition ? "TransitionEnd" : "AnimationEnd"), function(evt) {

                        tgt = ( removeFromTarget === true ? evt.target : $el )

                        if( typeof aniClasses === "string" ) {
                            // if 'aniClasses'is a string, then we assume there is a single class to remove
                            removeCls( aniClasses, tgt );
                        } else {
                            // if 'aniClasses'is an array, then we assume there are multiple classes to remove
                            _.forEach( aniClasses, function(cls, tgt) {
                                removeCls( cls );
                            });
                        }

                    }, $scope, allowChildEvents);
                }
            }

            /*,animationStartAddClass: function() {

            }*/
        }]
    }
});