
define( ["angular"], function(ng) {
    'use strict';

    /**
     * Utility methods for selectors.
     */

    var NAME = "selectorUtil"
        ,prevEl;

    return {
        
        $get: function() {

            return {

                name: NAME

                /**
                 * Using HTML5 "querySelectorAll", creates a convenience method similar to jQuery.find.
                 * Stores 'prevEl' so that you can omit it if you want to keep using tha same root element.
                 * Defaults to just show you the first item in the selector's array result, but you can override this with a true value on 'allSel' arg.
                 */
                ,find: function(qu, el, allSel, noNGWrap) {

                    prevEl = el;

                    if(!el) {
                        if(console && console.warn)
                            console.warn("Sorry, but you must provide an element as 2nd parameter. " + NAME + " -> find");
                        return;
                    }

                    var mainEl = el[0] || el
                        ,sel = mainEl.querySelectorAll( qu );
                    if( allSel === true ) {
                        
                        // Return the HTML elements
                        if( noNGWrap === true ) return sel;

                        // Wrap HTML elements in an Angular jQuery Lite object
                        var selArr = [];
                        ng.forEach(sel, function(el) {
                            selArr.push( ng.element(el) );
                        });
                        return selArr;
                    } else {

                        // Return the HTML element
                        if( noNGWrap === true ) return sel[0];

                        // Wrap HTML element in an Angular jQuery Lite object
                        return ng.element( sel[0] );
                    }
                }

                ,getScrollableWindowEl: function() {
                    var docTop = document.documentElement.scrollTop
                        ,docLeft = document.documentElement.scrollLeft
                        ,bodyTop = document.documentElement.scrollTop
                        ,bodyLeft = document.documentElement.scrollLeft;

                    if( docLeft === 0 && bodyLeft > 0 ||
                        docTop  === 0 && bodyTop  > 0 )
                        return document.body;
                    
                    if( docLeft > 0 && bodyLeft === 0 || 
                        docTop  > 0 && bodyTop  === 0 )
                        return document.documentElement;

                    document.documentElement.scrollTop = 1;
                    if( document.documentElement.scrollTop === 1 ) {
                        document.documentElement.scrollTop = 0;
                        return document.documentElement
                    }

                    document.body.scrollTop = 1;
                    if( document.body.scrollTop === 1 )
                        document.body.scrollTop = 0;
                    
                    return document.body;
                }
            }
        }
    }
});