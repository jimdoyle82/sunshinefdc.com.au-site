'use strict';

/**
 * This file is for example purposes only.
 * All functionality in this config should also be included in the parent app.
 */


// Define the RequireJS Config with all our deps and directories
require.config({
  baseUrl: '',
  paths: {
      // Directories (UPPER_CASE)
      EX_SERVICES            : deps()+'externals/angular-services-amd/services'
      ,D_GARDEN_FLIP          : "../../_dev"
      
      // libs
      ,"text"                 : deps()+"bower_components/requirejs-text/text"
      ,"modernizr"            : deps()+"libs/mdzr81686"
      ,"domReady"             : deps()+"libs/domready"
      ,"lodash"               : deps()+"bower_components/lodash/dist/lodash"
      ,"angular"              : deps()+"bower_components/angular/angular"
      ,"angular-route"        : deps()+"bower_components/angular-route/angular-route"
      ,"angular-animate"      : deps()+"bower_components/angular-animate/angular-animate"
      ,"angular-sanitize"     : deps()+"bower_components/angular-sanitize/angular-sanitize"
      ,"angular-gestures"     : deps()+"bower_components/angular-gestures/gestures"
      // ,"jquery"             : deps()+"libs/jquery-1.10.2"
  },
  shim: {
       "angular": { exports: "angular" }
      ,"angular-route": { deps: ["angular"] }
      ,"angular-animate": { deps: ["angular"] }
      ,"angular-sanitize": { deps: ["angular"] }
      ,"angular-gestures": { deps: ["angular"] }
  }
});



// Example services module. Parent app should have its own.
define("ExampleServices", [ "angular", "lodash"
    ,"EX_SERVICES/ModuleNameFetcher"
    ,"EX_SERVICES/ValidateModel"
    ,"EX_SERVICES/Util"
    ,"EX_SERVICES/SelectorUtil"
    ,"EX_SERVICES/AnimationUtil"
    ,"EX_SERVICES/StringUtil"
    ,"EX_SERVICES/DebugUtil"
], function ( ng, _, ModuleNameFetcher ) {

    var services = ng.module( "ServicesModule", []);

    _.forEach( arguments, function(arg) {
        if( ModuleNameFetcher.$get().isService(arg) === true )
            services.provider( ModuleNameFetcher.$get().getServiceName(arg), arg );
    });

  return services;
});



// Example wrapper/app module. Simulates a parent "App" module, includes our Directive and includes common Services.
define("App", ["angular", "D_GARDEN_FLIP/js/Directive", 'ExampleServices']
    ,function( ng,         directiveModule,              Services) {

    // This is a our directive wrapper with controller. 
    ng.module("wrapper", [ directiveModule.name, "ServicesModule" ]).controller("wrapperCtrl", function($rootScope, $timeout) {

      // Triggers the initial event to start the game
      $timeout( function() {
        $rootScope.$broadcast( "gflipStart" );
      }, 0);
      
    }).config(function($animateProvider) {

        /**
         * 'ani-show' class must be added to any animations you want in this module.
         * Stops nested elements getting animated unintentionally.
         */
        $animateProvider.classNameFilter(/do-animate/);
    });
});


// Now we require the "App" AMD module and bootstrap the Angular wrapper/app
require([   "require",  "angular", "domReady", "App" ], function(require, ng, domReady ) {

    window.name = "NG_DEFER_BOOTSTRAP!";

    domReady(function() {

        // Here we include the Services from the App, but this could be substituted by Git Subtree deps in future
        ng.bootstrap(document, [ "wrapper" ]);
        ng.resumeBootstrap();

    });
});



/**
 * Helper methods
 */
function dev() {
  return "../../_dev/";
}

function deps() {
  return "../deps/";
}

function appDev( port ) {
  return "http://localhost:8001/_dev/";    
}