# Dependencies

## bower_components
These are downloaded by Bower CLI and defined n the root bower.json. 
Generally speaking, it is a good idea to keep these in the project as a reference only and to copy the version needed into the "libs" folder with a version number at the end. 


## libs
If these libraries are reusable outside of this app, they should be versioned, like "libraryname.min.v0.1.2.js".
The version allows the RequireJS setup to require these dependencies with a specified version and exlude them from the build automatically.
They will then be defined on the site, downloaded asynchronously and browser cached for future pages, also featuring an overridable version number.

## externals
These are for private external modules that may also be used in other projects, like jQuery plugins and Angular directives.
While in still development branches should be referenced and, when complete, they should be merged back into the "master/trunk" for that module and their reference substituted by tags.
They should each have thier own "example" folder that allows you to develop on it stand-alone. 
You can even develop stand-alone in a another folder outside of the app and, using RequireJS and a NodeJS local server, reference the localhost and portnumber for real-time updates without committing to the repo.