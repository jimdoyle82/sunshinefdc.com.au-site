
define([ "angular", "./Constants", "EX_SERVICES/ModuleNameFetcher"
  ,"D_GARDEN_FLIP/js/Directive"
  ,"D_PAGE_NAV/js/Directive"
  ,"D_CONTENT_OVERLAY/js/Directive"
  //{{GENERATOR_INSERT_DIRECTIVE_DEFS}}
], function ( ng, Constants, ModuleNameFetcher ) {

   return ng.module( Constants.baseName + "DirectivesModule", ModuleNameFetcher.$get().getModuleNames(arguments) );
});