
define(["angular", "./Constants", "EX_SERVICES/ModuleNameFetcher", "EX_SERVICES/Util", "CORE/js/AppCtrl"
        ,"PG_HOME/js/Ctrl", "text!PG_HOME/template.html"
        ,"PG_GAME/js/Ctrl", "text!PG_GAME/template.html"
        ,"PG_GALLERY/js/Ctrl", "text!PG_GALLERY/template.html"
        ,"PG_NEWSLETTERS/js/Ctrl", "text!PG_NEWSLETTERS/template.html"
        ,"PG_CONTACT/js/Ctrl", "text!PG_CONTACT/template.html"
        //{{GENERATOR_INSERT_PAGE_IMPORTS}}
        ,"angular-route", "angular-animate", "angular-sanitize", "angular-touch"
        ,"angular-carousel", "angular-google-maps", "angular-gestures"
        , './Services', './Directives'
		], 
	function( ng, Constants, ModuleNameFetcher, Util, AppCtrl
        ,HomeCtrl,          HomePartial
        ,GameCtrl,          GamePartial
        ,GalleryCtrl,       GalleryPartial
        ,NewslettersCtrl, NewslettersPartial
        ,ContactCtrl, ContactPartial
        //{{GENERATOR_INSERT_PAGE_ARGS}}
		) {

    /**
     * App Module
     */

    var ieVersion = ( navigator.userAgent.toLowerCase().indexOf( 'msie' ) != -1 // checks if it's IE
            ? document.documentMode // gets the version of IE from the docmode
            : false ); // not IE

	var app = ng.module( Constants.baseName + "Module", [ "ngRoute", "ngAnimate", "ngSanitize"
                ,"ngTouch" // Supposed to remove 'touch' delay on click. I think 'angular-gestures' overrides it anyway.
                ,"angular-carousel", "angular-gestures" ]

            // 'angular-google-maps' breaks in IE8. Wouldn't normally browser sniff, but dunno what to feature detect. 
            .concat( ieVersion === false || ieVersion > 8 ? ["google-maps"] : [] ) 

            // gets the remaining angular module names from the arguments
            .concat( ModuleNameFetcher.$get().getModuleNames(arguments) )
        )
        .controller( Constants.baseName + "Ctrl", AppCtrl )
        .value('$anchorScroll', angular.noop ) //Stops page from jumping on hash change.
        .config([ '$routeProvider', '$locationProvider', '$animateProvider', 
            function($routeProvider, $locationProvider, $animateProvider) {

                
            $locationProvider.html5Mode(false).hashPrefix("!");
            // $locationProvider.html5Mode(true);


            /**
             * App Router.
             * Please see main README.md under "Router -> Deep links" for useful tips.
             */
            var HOME = "/home";

            $routeProvider.when( HOME,      { template: HomePartial,    controller: HomeCtrl });
            $routeProvider.when( "/game",   { template: GamePartial,    controller: GameCtrl });
            $routeProvider.when( "/gallery", { template: GalleryPartial, controller: GalleryCtrl });
            $routeProvider.when( "/about-me", { template: NewslettersPartial, controller: NewslettersCtrl });
            $routeProvider.when( "/contact", { template: ContactPartial, controller: ContactCtrl });
            //{{GENERATOR_INSERT_PAGE_CONF}}

            $routeProvider.otherwise( { redirectTo: HOME } );


            /**
             * Custom 'do-animate' class must be added to any animations you want in this module.
             * Stops nested elements getting animated unintentionally.
             * Had this problem with Garden Flip intro panel.
             */
            $animateProvider.classNameFilter(/do-animate/);
        }]);

    return app;
});