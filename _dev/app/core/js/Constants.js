define(function() {

	var  nameCC = "sunshineFDC"
		,nameSC = "Sunshine FDC"
		,nameUC = "SUNSHINE_FDC"
		,nameLC = "sunshinefdc"
		,nameShort = "sfdc";

	return {
		 baseName: nameShort
		,rootPath: "/"
		,imgPath: "/deploy/img"
		,isLocalDev: (window.location.hostname.indexOf("localhost") != -1)
		,cssSelectors: {
			app: "#" + nameShort
		}
	}
});