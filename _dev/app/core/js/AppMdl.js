
// probably get rid of this, in favour of the AppModel service and combine the two
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {    
    "use strict";
    /**
     * Holds the non-extensible 'Model' for this page.
     * Nesting objects within 'Model' is also best practise for defining objects that will be declared as an 
     * 'inheritedModel' in child directives' scopes.
     */
    

    var Mdl = {

        pageState: ["is-from-game", "is-from-intro"]

        ,gflip: {
            paused: true // if true, will still show the overlay bg
            ,showIntroPanel: true // if false, will hide the intro panel, but still show the white overlay bg
            ,isActive: false // should be true when game is showing, regardless of paused state
            ,found: 0
            ,introComplete: false
            ,allTokensFound: false // set to true for debugging replay panel
            ,bgfallback: false
            ,countDownMax: 3
            ,isCounting: false
            ,countDownCurrent: -1
            ,hmTapRayOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 500 // kids aren't too acurate, so give them some slack
            }
            ,hmTapHandOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1200 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 400 // kids aren't too acurate, so give them some slack
            }
            ,hmTapBtnOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
            }
        }

        ,pagenav: {
            introComplete: false
            ,expanded: false
            ,navfallback: false
            ,notransforms3d: false
            ,hmTapCarriageOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
            }
            ,hmTapTitleOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 800 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 100 // kids aren't too acurate, so give them some slack
            }
        }

        ,contentOverlay: {
            gflipIsPlayable: true
            ,hmTapBgOpts: {
                prevent_default: true // this is needed to avoid double handler calls
                ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
                ,tap_max_distance: 500 // kids aren't too acurate, so give them some slack
            }
        }
    };


    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});