
define([ "angular", "./AppMdl", "modernizr" ], function( ng, mdl, Mdnzr ) {
    "use strict";

    return [ '$rootScope', '$scope', '$window', '$timeout', '$element', '$log', 'appModelSetter', 'selectorUtil', 
    	function( $rootScope, $scope, $window, $timeout, $element, $log, ams, su ) {
        var Mdl = mdl( $scope ); // A non-extensible $scope.Mdl.
        
        // Sets AppMdl, so that rest of the App can edit and watch it via this service
        ams.setMdl( $scope, Mdl );

        // on app resize, load and page change scroll to top vertically & centre horizontal
        var resize = function() {
            var wind = su.getScrollableWindowEl();

            wind.scrollTop = 0;

            if( $element[0].clientWidth > document.body.clientWidth )
                wind.scrollLeft = Math.round( ($element[0].clientWidth - document.body.clientWidth) / 2 );

            $element[0].style.height = wind.innerHeight + "px";
        }

        $window.addEventListener( "resize", resize );
        resize();

        /*$scope.$on('$routeChangeStart', function() {
        	$window.onresize();

        	Mdl.pageState = "is-from-game";
        });*/
        
        // Mdl.pageState = [];

        $rootScope.$on("$locationChangeStart", function(event, next, current) { 
        	resize();
	        $log.info("location changing to:" + next); 
	    });
    }];
});
