define([ "angular", "lodash", "CORE/js/Constants"
    ,"EX_SERVICES/ModuleNameFetcher"
    ,"SERVICES/AppModelSetter"
    ,"SERVICES/PageHelper"
    ,"EX_SERVICES/ValidateModel"
    ,"EX_SERVICES/Util"
    ,"EX_SERVICES/SelectorUtil"
    ,"EX_SERVICES/AnimationUtil"
    ,"EX_SERVICES/StringUtil"
    ,"EX_SERVICES/DebugUtil"
    // ,"HH_SERVICES/SnapShotService/Service"
    // ,"HH_SERVICES/MetaDataService/Service"
], function ( ng, _, Constants, ModuleNameFetcher ) {

    var services = ng.module( Constants.baseName + "ServicesModule", []);

	// services.provider allows us to use $provide in unit tests and write service arguments exactly the same as they are written
    // eg: MyProjectAppSnapShotService - however, this won't build in the rjs optimiser, so just used for unit tests.

    _.forEach( arguments, function(arg) {
        if( ModuleNameFetcher.$get().isService(arg) === true )
            services.provider( ModuleNameFetcher.$get().getServiceName(arg), arg );
    });

    // console.log( services._invokeQueue[0][2][0] );

	return services;
});