/**
 * This script gets used for both dev environment and Grunt RequireJS build.
 * File name must be "NGBootstrap.js".
 */

// Can't use relative paths on the bootstrap require
require( [ "domready", "angular", "CORE/js/Constants", "CORE/js/App"], 
	function( domReady, ng, 	   Constants, 		   App ) {
	'use strict';

    /**
     * Only run logic and load app if not looking at PhantomJS snapshot.
     * If _escaped_fragment_ is in url, the page is only meant for bots.
     */
    if( window.location.pathname.indexOf("_escaped_fragment_") != -1 ) return;

    domReady(function() {
        
        /**
         * Adding 'data-status' attribute for PhantomJS to detect.
         * Using standard jQuery for 'body' selector, as jQueryLite doesn't include selectors,
         * only has $.find(".myselector") which is fine for most cases, but not for anything outside of the app.
         */
        document.body.setAttribute('data-status', 'pending');

        ng.bootstrap(document, [ App.name ]);
        ng.resumeBootstrap();
    });
});