
define([ "./Mdl" ], function( mdl ) {
    "use strict";

    return [ '$rootScope', '$scope', 'appModelSetter', 'pageHelper'
        ,function ( $rootScope, $scope, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }
        
    }];
});
