
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    var Mdl = {
        hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});