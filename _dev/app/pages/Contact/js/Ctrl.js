
define([ "./Mdl" ], function( mdl ) {
    "use strict";

    return [ '$location', '$rootScope', '$scope', 'appModelSetter', 'pageHelper'
        ,function ( $location, $rootScope, $scope, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }

        $scope.map = {
		    center: {
		        latitude: -33.498643
		        ,longitude: 151.320778
		    }
		    ,zoom: 18
		};
        
    }];
});
