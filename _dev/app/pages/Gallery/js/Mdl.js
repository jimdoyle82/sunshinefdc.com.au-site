
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    var Mdl = {
		title: "gallery"		
		,slideIndex: 0
		// "loadedImages" doesn't get populated until the page transition has finished
		,loadedImages: []
		,imagesToLoad: [
			 {id:"bookcorner1"}
			,{id:"construction1"}
			,{id:"construction2"}
			,{id:"construction3"}
			,{id:"couches"}
			,{id:"deck1"}
			,{id:"kidstable"}
			,{id:"kitchen1"}
			,{id:"music1"}
			,{id:"music2"}
			,{id:"playmat1"}
			,{id:"playmat2"}
			,{id:"playmat3"}
			,{id:"shelf1"}
			,{id:"signin"}
			,{id:"valentinesday"}
		]
		,hmTapBtnOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            ,tap_max_touchtime: 1000 // kids can take awhile to click stuff, so give them plenty of time
            ,tap_max_distance: 200 // kids aren't too acurate, so give them some slack
        }
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});