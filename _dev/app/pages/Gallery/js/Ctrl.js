
define([ "./Mdl" ], function( mdl ) {
    "use strict";

    return [ '$rootScope', '$scope', '$timeout', 'appModelSetter', 'pageHelper', 
    function ( $rootScope, $scope, $timeout, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        $timeout( function() {
	        $scope.$apply( function() {
        		pgMdl.loadedImages = pgMdl.imagesToLoad;
	        });
        }, 900);

        $scope.closePage = function( isClick ) {
        	pageHelper.closePage( isClick );
        }
        
    }];
});
