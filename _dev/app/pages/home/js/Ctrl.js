
define([ "./Mdl", "CORE/js/Constants" ],
    function( mdl, Constants ) {
    "use strict";

    return [ '$rootScope', '$scope', '$timeout', 'appModelSetter', 'pageHelper', 
    function ( $rootScope, $scope, $timeout, ams, pageHelper ) {
        var pgMdl = mdl( $scope ); // A non-extensible $scope.Mdl.

        $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: false } );
        // ams.setVal( ams.types.GFLIP, "showIntroPanel", false );

        ams.setWatcher( ams.types.GFLIP, "bgfallback", function(val) {
            pgMdl.gflipBgFallback = val;
        }, true);

        $scope.closePage = function( isClick ) {

            stopGiggling();
            pageHelper.closePage( isClick );
        }

        startGiggling();

        function stopGiggling() {
            pgMdl.giggleState.stopGiggling = true;
            pgMdl.giggleState.loopCurrent = 0;
            pgMdl.giggleState.classes.length = 0;
        }

        function startGiggling( fromIntv ) {

            pgMdl.giggleState.classes.push( "is-giggling" );
            pgMdl.giggleState.loopCurrent++;

            if( pgMdl.giggleState.loopCurrent > pgMdl.giggleState.loopMax ) {
                stopGiggling();
                return;
            }
            $timeout( function() {
                if( pgMdl.giggleState.stopGiggling ) return;

                startGiggling();
            }, pgMdl.giggleState.resetTimeOut );
        }        
        
    }];
});
