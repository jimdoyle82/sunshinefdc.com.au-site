
define( function(_) {
    'use strict';

    /**
     * For functions that are the same across multiple pages
     */

    // constants
    var NAME = "pageHelper";

    return {
        
        $get: ['$location', 'appModelSetter', 'util', function($location, ams, util) {

            return {

                name: NAME
                
                ,closePage: function( isClick ) {
                    // clicks are there just for ie8
                    if( isClick === true ) {
                        if( !util.sniffIE() || util.sniffIE() > 8 ) return;
                    }

                    $location.path("game");
                    ams.setVal( ams.types.PAGE_NAV, "expanded", false );
                }
            }
        }]
    }
});