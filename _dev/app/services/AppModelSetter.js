
// change name to AppMdlManager
define( ["lodash"], function(_) {
    'use strict';

    /**
     * Service for injecting the AppModel.
     * Delierately has no getMdl method, as all properties should be set via setVal.
     * This also restricts the ability to add a property to the callee's scope, forcing
     * them to use the "setWatcher" method, which gives this service more control.
     */

    // constants
    var NAME = "appModelSetter"
        ,types = {
            GFLIP: "gflip"
            ,PAGE_NAV: "pagenav"
            ,CONTENT_OVERLAY: "contentOverlay"
        };

    return {
        
        $get: function() {

            var AppMdl
                ,watchers = [];

            var warning = function( methName, type, prop ) {
                    if( console && console.warn )
                        console.warn( "WARNING: Could not set property '" + prop + "' on AppMdl object '" + type + "' in method "+methName+"()." );
                }
    
            return {

                name: NAME
                ,types: types
                
                ,setMdl: function( appScope, mdl ) {
                    AppMdl = mdl;

                    // perhaps could try using $watchCollection, as may be less expensive, or have a seperate watcher or each type

                    // always watches the app model on the app controller scope
                    appScope.$watch( "Mdl", function( newVal, oldVal ) {
                        
                        _.forEach( watchers, function(w) {
                            
                            // console.log( newVal[ w.type ][ w.prop ], oldVal[ w.type ][ w.prop ] );

                            if( newVal[ w.type ][ w.prop ] !== oldVal[ w.type ][ w.prop ] ) {
                                w.callback( newVal[ w.type ][ w.prop ] );
                            }
                        });
                    }, true);
                }

                ,setVal: function( type, prop, val ) {

                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {
                        AppMdl[ type ][ prop ] = val;
                    } else {
                        warning( 'setVal', type, prop );
                    }
                }

                // Just for debugging. Don't to give access with a 'getVal' method, but sometimes need to log it.
                ,logVal: function( type, prop ) {

                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {

                        // gets passed grunt-remove-logging task
                        if( console && console.log )
                            console['log']( AppMdl[ type ][ prop ] );
                    } else {
                        warning( 'logVal', type, prop );
                    }
                }

                ,setWatcher: function( type, prop, callback, doSet ) {
                    if( AppMdl[ type ] && AppMdl[ type ][ prop ] != undefined ) {
                        watchers.push( { type: type, prop: prop, callback: callback } );
                        if( doSet ) callback( AppMdl[ type ][ prop ] );
                    } else {
                        warning( 'setWatcher', type, prop );
                    }
                }
            }
        }
    }
});