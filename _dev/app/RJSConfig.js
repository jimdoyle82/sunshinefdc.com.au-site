
/**
 * This config file gets used in 3 places. Local dev environment, Grunt RequireJS build and Karma unit tests.
 * File name must be "RJSConfig.js".
 * Object "requirejsConfig" must be declared globally, with object properties 
 * of "paths", "shim", "versions" and "config", or else build and tests will fail.
 */
var requirejsConfig = (function() {

  var appConfig = {}; // The main config object that will be returned

  /**
   * Versions defined here will get automatically excluded from the build.
   * When they get defined in the main RequireJS config for the whole site, the same 
   * version numbers should be used to override the the "rjsVersions" values.
   */
  setVersions({
    "modernizr": "81686" // added just so it gets excluded from build
    ,"lodash":  "2.4.1"
    ,"angular": "1.2.16"
    ,"angular-route": "1.2.16"
    ,"angular-animate": "1.2.16"
    ,"angular-sanitize": "1.2.16"
    ,"angular-touch": "1.2.16"
    ,"angular-google-maps": "1.0.18"
    ,"angular-carousel": "0.2.2"
    ,"angular-gestures": "0.2.2"
    ,"domready": "2.0.1"
    ,"text": "2.0.5"
  });

  appConfig.paths = {
    
    // Directories (UPPER_CASE)
     CORE                   : dev()+"app/core"
    ,SERVICES               : dev()+"app/services"
    ,DIRECTIVES             : dev()+"app/directives"
    ,LIBS                   : deps()+"libs"
    
    ,PG_HOME              : dev()+"app/pages/Home"
  ,PG_GAME: dev()+"app/pages/Game"
  ,PG_GALLERY: dev()+"app/pages/Gallery"
  ,PG_NEWSLETTERS: dev()+"app/pages/Newsletters"
  ,PG_CONTACT: dev()+"app/pages/Contact"
  //{{GENERATOR_INSERT_PAGE_DEFS}}

    ,D_GARDEN_FLIP          : deps()+"externals/ngui/GardenFlip/_dev"
    ,D_PAGE_NAV             : dev()+"app/directives/PageNav"
  ,D_CONTENT_OVERLAY: dev()+"app/directives/ContentOverlay"
  //{{GENERATOR_INSERT_DIRECTIVE_DEFS}}
    
    ,EX_NGUI                : deps()+'externals/ngui'
    ,EX_SERVICES            : deps()+'externals/angular-services-amd/services'
    ,EX_DIRECTIVES          : deps()+"externals/directives"

    ,DUMMY                  : ngui( "Dummy", "1000", false )
    
    // libs excluded from build
    ,"modernizr"            : getVersions( "modernizr" )
    ,"text"                 : getVersions( "text" )
    ,"domready"             : getVersions( "domready" )
    ,"lodash"               : getVersions( "lodash")
    ,"angular"              : getVersions( "angular", true )
    ,"angular-route"        : getVersions( "angular-route",     true, "angular" )
    ,"angular-animate"      : getVersions( "angular-animate",   true, "angular" )
    ,"angular-sanitize"     : getVersions( "angular-sanitize",  true, "angular" )
    ,"angular-touch"        : getVersions( "angular-touch",     true, "angular" )

    ,"angular-google-maps"  : getVersions( "angular-google-maps", true )
    ,"angular-carousel"     : getVersions( "angular-carousel", true )
    ,"angular-gestures"     : getVersions( "angular-gestures", "gestures" )

    // path overrides for older external directives or services 
    ,"APP/js/configs/MainConfig": dev()+"app/core/js/Constants"
  }

  appConfig.shim = {
       "angular": { exports: "angular" }
      ,"angular-route": { deps: ["angular"] }
      ,"angular-animate": { deps: ["angular"] }
      ,"angular-touch": { deps: ["angular"] }
      ,"angular-sanitize": { deps: ["angular"] }
      ,"angular-carousel": { deps: ["angular-touch" ] }
      ,"angular-google-maps": { deps: ["angular", "lodash" ] }
      ,"angular-gestures": { deps: ["angular"] }
      ,"classie": { exports : "classie" }
  }


  appConfig.config = {

    /**
     * This useXhr override is required to load RequireJS Text Plugin over different ports
     * REF: http://rockycode.com/blog/cross-domain-requirejs-text/
     */
    "text": {
        useXhr: function (url, protocol, hostname, port) {
            return true;
        }
    }
  };


  /**
   * Helper methods. May need to namespace these
   */

  function dev() {
    if( typeof window == "undefined" ) return "../../_dev/"; // Grunt RequireJS build
    return "";                                               // Browser & Karma testing
  }

  function deps() {
    if( typeof window == "undefined" ) return "../../_dev/deps/"; // Grunt RequireJS build
    return "deps/";                                               // Browser & Karma testing
  }


  function ngui( name, port, localHost ) {

      // Grunt RequireJS build
      if( typeof window == "undefined" ) return "../../_dev/deps/externals/ngui/"+name+"/";

      /**
        * Browser & Karma testing
        * Force "localHost" override for all ngui
        */
      // localHost = true;

      if( localHost && typeof window != "undefined" )
        return "http://localhost:"+port+"/";
      
      return "deps/externals/ngui/"+name+"/";
  }


  function getVersions( id, nested, parentId, doGetMockInstead ) {
    var nestedDir = "";
    if( nested && typeof nested === "string" )  nestedDir = "/"+nested;
    else if( nested )                           nestedDir = "/"+id;
    return deps()+"libs/versioned/"+                        // get from the 'versioned' directory
          ( parentId || id)+".v" + appConfig.versions[id] + // get base name, plus version number
          nestedDir + // if true, will sit in directory named same as 'id'. If string, will use that as directory name
          ( doGetMockInstead === true ? "-mock" : "" );     // A boolean to get "-mock" version, which does nothing
  }

  /**
   * Sets "appConfig.versions" object, which tells RequireJS Optimiser that these modules should be excluded, as they are 
   * defined and versioned outside of this app.
   */
  function setVersions( versions ) { 

    var isNodeBuild = (typeof window == "undefined");
    if( isNodeBuild ) {
      /**
       * If doing a build, where "window" doesn't exist, this fills the "excludes" array.
       */

      appConfig.versions = { excludes:[] }      
    } else {
      /**
       * If using browser, checks to see if global "rjsVersions" is set, which it should be, 
       * then overrides values specified in arg object "versions".
       */

      if( window.rjsVersions )  appConfig.versions = window.rjsVersions;
      else                      appConfig.versions = {};
    }

    for( var modName in versions ) {
      appConfig.versions[ modName ] = versions[ modName ];
      if( isNodeBuild ) appConfig.versions.excludes.push( modName );
    }

  }

  return appConfig;
})();