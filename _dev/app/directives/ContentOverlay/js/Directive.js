
define([ "./Ctrl", "./DefMdl", "text!D_CONTENT_OVERLAY/template.html"]
	,function( Ctrl, Mdl, template ) {

	return angular.module( Mdl() + "Module", ["angular-gestures"] ).directive( Mdl(), function() {
	        return {
				 controller: Ctrl
				,template: template
				,replace: true
				,scope: {
					inhMdl: "&mdl" // inherited model - one-way binding "&"
				}
	        }
		});
});