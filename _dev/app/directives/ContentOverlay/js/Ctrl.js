
define([ "./DefMdl" ], function( DefMdl ) {
    "use strict";

    return [ '$scope', '$rootScope', '$element', '$location', 'appModelSetter', 
    function ($scope, $rootScope, $element, $location, ams ) {
        var Mdl = DefMdl( $scope, true ); // A non-extensible $scope.Model.
        
        ams.setWatcher( ams.types.GFLIP, "isActive", showOverlay, true );
        ams.setWatcher( ams.types.GFLIP, "bgfallback", enableGFlip, true );

        // makes it so the overlay is already visible on page load, so it doesn't animate into position
        $element.removeClass("onload-state");

        $scope.unpause = function() {
            $location.path("game");
            ams.setVal( ams.types.PAGE_NAV, "expanded", false );
            $rootScope.$broadcast( "gflipStart" );
            // ams.setVal( ams.types.GFLIP, "paused", false );
        }

        function showOverlay( val ) {
            $scope.ngClsShowing = ( val ? "" : "contentoverlay-is-showing" );
        }

        function enableGFlip( val ) {
            Mdl.gflipIsPlayable = !val;
        }
    }];
});
