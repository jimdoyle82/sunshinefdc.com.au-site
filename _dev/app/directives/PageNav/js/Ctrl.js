
define([ "./DefMdl", "CORE/js/Constants", "angular", "modernizr" ], function( DefMdl, Constants, ng, Mdnzr ) {
    "use strict";
    var $ = ng.element;

    return [ '$scope', '$rootScope', '$element', '$timeout', '$location', 'animationUtil', 'selectorUtil', 'appModelSetter', 'util',
        function ( $scope, $rootScope, $element, $timeout, $location, au, su, ams, util ) {
        
        var Mdl = DefMdl( $scope, true ); // A non-extensible $scope.Model.

        $scope.ngClsMovingTrain = [];

        if( !Mdnzr.svg )
            Mdl.navfallback = true;

        // IE9 rotation isn't supported in transform prop & neither is 3d, so detecting with 3d
        if( !Mdnzr.csstransforms3d )
            Mdl.notransforms3d = true;

        $scope.toggleExpanded = function() {
            console.log( "toggleExpanded" );
            // return
            Mdl.expanded = !Mdl.expanded;
        }

        $scope.openPage = function( url, isClick ) {
            
            // clicks are there just for ie8
            if( isClick === true ) {
                if( !util.sniffIE() || util.sniffIE() > 8 ) return;
            }

            $scope.toggleExpanded();

            if( Mdl.expanded ) {
                $rootScope.$broadcast( "gflipIntroPause", { showIntroPanel: true, skipPageNavCollapse: true } );
            } else {
                $location.path( url );
            }
        }

        $rootScope.$on("expandPageNav", function(evt, data) {

            Mdl.expanded = data.expandState;
        });

        /*ams.setWatcher( ams.types.GFLIP, "isActive", function(val) {
            if( val === true ) Mdl.expanded = false;
        }, false );*/


        // $scope.ngClsMovingTrain = "intro-ani";
        $timeout( function() {
            $scope.$apply( startTrainShowHead );

            $scope.$watch( "Mdl.expanded", function( val ) {

                var arr = ['train-is-moving'];
                arr.push( val ? "train-is-showing-full" : "train-is-showing-head" );
                
                $scope.ngClsMovingTrain = arr;
            });
        }, 500);

        /**
         * Waits until the main transition has ended and removes the class "train-is-moving", which
         * affects the wheels. 
         * Only problem here is that it seems to listen to all events for child transitions as well. Should
         * be some solution for stopping propagation/bubbling somewhere, somehow.
         */
        var group = su.find( ".l-pagenav-pos", $element, false, true );
        au.prefixedEvent( group, "TransitionEnd", function(evt) {

            // for some reason doesn't get removed from "ngClsMovingTrain" array.
            if( !Mdl.introComplete )
                group.classList.remove("intro-ani");

            Mdl.introComplete = true;

            $scope.$apply(function() {

                var index = $scope.ngClsMovingTrain.indexOf('train-is-moving');
                if( index !== -1 ) $scope.ngClsMovingTrain.splice( index, 1 );
            });
        });

        function startTrainCont() {
            $scope.ngClsMovingTrain = ['train-is-moving', 'train-is-moving-continuously'];
        }

        function startTrainShowHead() {
            $scope.ngClsMovingTrain = ['intro-ani', 'train-is-moving', 'train-is-showing-head'];
            $scope.ngClsPagenavShowing = "";
        }
    }];
});
