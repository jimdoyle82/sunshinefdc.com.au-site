
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "pageNav"; // must be camelCase
    
    var Mdl = {
        introComplete: false
        ,expanded: false
        ,navfallback: false
        ,notransforms3d: false
        ,hmTapCarriageOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            // ,tap_max_touchtime: 1500 // kids can take awhile to click stuff, so give them plenty of time
            // ,tap_max_distance: 300 // kids aren't too acurate, so give them some slack
        }
        ,hmTapTitleOpts: {
            prevent_default: true // this is needed to avoid double handler calls
            // ,tap_max_touchtime: 800 // kids can take awhile to click stuff, so give them plenty of time
            // ,tap_max_distance: 100 // kids aren't too acurate, so give them some slack
        }
    };

    // If no args, just return BASE_NAME, otherwise returns a non-extensible copy of Mdl (ES5 only)
    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME;
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});