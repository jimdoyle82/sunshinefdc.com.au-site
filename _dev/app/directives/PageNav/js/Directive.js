
define([ "./Ctrl", "./DefMdl", "text!D_PAGE_NAV/template.html"]
	,function( Ctrl, Mdl, template ) {

	return angular.module( Mdl() + "Module", ["angular-gestures"] ).directive( Mdl(), function() {
	        return {
				 controller: Ctrl
				,template: template
				,replace: true
				,scope: {
					inhMdl: "&mdl" // inherited model - one-way binding "&"
				}
	        }
		});
});