
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json")

        ,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,ngui:          "<%= vars.dev %>deps/externals/ngui/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
        }


        ,clean: {
            options: { force: true }
            ,homeSVGs:  [ "<%= vars.dist %>img/home/*" ]
            ,coreSVGs:  [ "<%= vars.dist %>img/core/*" ]
        }


        ,svgstore: {
            core: {
                options: {
                    prefix : 'core-'
                }
                ,files: {
                    "<%= vars.dist %>img/core/svgdefs.min.svg": "<%= vars.dist %>img/core/min/*.svg"
                }
            }
            ,home: {
                options: {
                    prefix : 'page-home-'
                }
                ,files: {
                    "<%= vars.dist %>img/home/svgdefs.min.svg": "<%= vars.dist %>img/home/min/*.svg"
                }
            }
        }


        ,svgmin: {
            core: {
                files: [{
                    expand: true,
                    cwd: '<%= vars.app %>core/img/svgdefs',
                    src: ['*.svg'],
                    dest: '<%= vars.dist %>img/core/min/',
                    ext: '.svg'
                }]
            }
            ,home: {
                files: [{
                    expand: true,
                    cwd: '<%= vars.app %>pages/home/img/svgdefs',
                    src: ['*.svg'],
                    dest: '<%= vars.dist %>img/home/min/',
                    ext: '.svg'
                }]
            }
        }

    });
        

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-svgstore');
    grunt.loadNpmTasks('grunt-svgmin');

    grunt.registerTask("default", [ "clean", "svgmin", "svgstore", "insert-svgdefs" ] );


    grunt.registerTask("insert-svgdefs", function() {
        
        var distName = "core";
        attachSVGDefs( 'CORE', false, grunt.config.get("vars.dist") + "img/"+distName+ "/svgdefs.min.svg" );
        attachSVGDefs( 'CORE', true, grunt.config.get("vars.dist") + "img/"+distName+ "/svgdefs.min.svg" );

        distName = "home";
        attachSVGDefs( 'HOME', false, grunt.config.get("vars.dist") + "img/"+distName+ "/svgdefs.min.svg" );
        attachSVGDefs( 'HOME', true, grunt.config.get("vars.dist") + "img/"+distName+ "/svgdefs.min.svg" );

        attachSVGDefs( 'GFLIP', false, grunt.config.get("vars.ngui") + "GardenFlip/_dev/img/svgdefs.min.svg" );
        attachSVGDefs( 'GFLIP', true, grunt.config.get("vars.ngui") + "GardenFlip/_dev/img/svgdefs.min.svg" );
    });


    /**
     * Helper methods
     */

     function attachSVGDefs( tagId, isDist, src ) {

        tagStart = "<!--GRUNT_INSERT_SVG_"+tagId+"_START-->"
        tagEnd = "<!--GRUNT_INSERT_SVG_"+tagId+"_END-->"
        
        var filePath = ( isDist ? grunt.config.get("vars.deploy") : grunt.config.get("vars.app") ) + "index.html"
            ,svgContents = grunt.file.read( src )
            ,devIndex = grunt.file.read( filePath );

        svgContents = svgContents.replace("<svg><defs>", "<svg class='svgdefs'><defs>");

        var rx = new RegExp( tagStart + "[\\d\\D]*?" + tagEnd, "g");
        devIndex = devIndex.replace( rx, tagStart + "\n" + svgContents + "\n        " + tagEnd );

        grunt.file.write( filePath, devIndex );
     }
};
