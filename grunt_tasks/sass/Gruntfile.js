module.exports = function(grunt) {


    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json")

        ,vars: {
            root:   "../../"
            ,dev:   "<%= vars.root %>_dev/"
            ,dist:  "<%= vars.root %>_dist/"
            ,deploy:"<%= vars.dist %>deploy/"
            ,app:   "<%= vars.dev %>app/"
            ,externals:         "<%= vars.dev %>deps/externals/"
            ,dist_css_debug:    "<%= vars.deploy %>css/debug/"
            ,dist_css_min:      "<%= vars.deploy %>css/min/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= pkg.author.name %>'

            // if changing sprite directory names, they must also be changed in config.rb, next to Gruntfile.js
            ,dist_sprites_prep: "<%= vars.dist %>img/appsprite/"
            ,dist_sprites_uncom:"<%= vars.dist %>img/sprites-uncompressed/"
            ,deploy_sprites:    "<%= vars.deploy %>img/sprites/"
        }


        ,clean: {
            options: { force: true }
            ,img:   [ "<%= vars.dist_sprites_prep %>*", "<%= vars.dist_sprites_uncom %>*", "<%= vars.deploy_sprites %>*" ]
            ,debug: [ "<%= vars.dist_css_debug %>*" ]
            ,min:   [ "<%= vars.dist_css_min %>*" ]
        }


        ,watch: {
            debug: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:debug", "forcereload" ]
            }
            ,debugapp: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:debugapp", "forcereload" ]
            }
            ,min: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:min" ]
            }
            ,both: {
                files: [ "<%= vars.app %>**/scss/**/*.scss" ]
                ,tasks: [ "sass:debug", "sass:min" ]
            }
        }


        // compass sprite setting are in config.rb, next to Gruntfile.js
        ,sass: {
            options: {
                /**
                  * Without noCache or cacheLocation changed, you can get issues on Windows with deep nested file paths over 260 characters
                  * noCache slows things down, so best option is to save to computer's root "tmp" folder
                  */
                // noCache: true  // disabled caching
                cacheLocation: "/tmp/sass-cache"
                ,banner: "/* <%= vars.banner_content %> */"
                ,compass: true
            }

            ,debug: {
                //"nested, expanded, compact, compressed"
                options: { style: "expanded" }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: "<%= vars.app %>"
                    ,src: [   "core/scss/*.scss" ]
                    ,dest: "<%= vars.dist_css_debug %>"
                    ,ext: '.css'
                }]
            }

            // Quick debug of app.scss only
            ,debugapp: {
                options: { style: "expanded" }
                ,files: [{
                    src: "<%= vars.app %>core/scss/app.scss"
                    ,dest: "<%= vars.dist_css_debug %>app.css"
                }]
            }

            ,min: {
                options: { style: "compressed" }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: "<%= vars.app %>"
                    ,src: [ "core/scss/*.scss" ]
                    ,dest: "<%= vars.dist_css_min %>"
                    ,ext: '.css'
                }]
            }
        }


        // Copies all the images marked for sprites into dist directory first so that sprites can be made from a single location
        ,copy: {
            img: {
                expand: true
                ,flatten: true
                ,cwd: "<%= vars.dev %>"
                ,src: [  "app/core/img/appsprite/*.png"
                        ,"app/pages/*/img/appsprite/*.png"
                        ,"app/directives/*/img/appsprite/*.png"
                        ,"externals/ngui/_dev/*/img/appsprite/*.png" ]
                ,dest: "<%= vars.dist_sprites_prep %>"
            }
        }


        ,imagemin: {
            sprites: {
                options: {
                    pngquant: true
                    ,optimizationLevel: 3
                }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: '<%= vars.dist_sprites_uncom %>'
                    ,src: ['*.png']
                    ,dest: '<%= vars.deploy_sprites %>'
                }]
            }
        }

    });

    
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks("grunt-contrib-imagemin");



    grunt.registerTask("forcereload", function() {

        console.log( "Changing watched file: " + grunt.config("vars.dist") + "/forcereload.txt" );
        grunt.file.write( grunt.config("vars.dist") + "/forcereload.txt", Math.random() );
    });


    grunt.registerTask("default", function( option ) {

        /**
         * Options to configure your scss compile, using Grunt Option.
         * Don't use more than 1 option.
         * Eg `grunt --min`.
         * The 'option' argument may also be used like this `grunt default:min`.
         */

        var opts = {}

        if( option ) {
            opts[ option ] = true;
        } else {
            
            // straight compiles
            opts["debug"]  = !!grunt.option("Debug")  // just debug (Cap "D" because --debug is reserved by Grunt)
            opts["min"]    = !!grunt.option("min")    // just min
            opts["both"]   = !!grunt.option("both");  // both

            opts["debugapp"]  = !!grunt.option("debugapp")  // quick scss compile without any other crap
            opts["wdebugapp"]  = !!grunt.option("debugapp")  // quick scss compile without any other crap

            // watches
            opts["wbug"] = !!grunt.option("watch.debug")   // just watch debug
            opts["wmin"]   = !!grunt.option("watch.min")   // just watch min
            opts["wboth"]  = !!grunt.option("watch.both"); // watch both
        }

        var tasks = [];

        if( opts["debugapp"] )  tasks = ["sass:debugapp" ];
        if( opts["wdebugapp"] )  tasks = ["sass:debugapp", "watch:debugapp" ];

        if( opts["debug"] )    tasks = [ "clean:img", "clean:debug", "copy", "sass:debug" ];
        if( opts["min"] )      tasks = [ "clean:img", "clean:min",   "copy", "sass:min", "imagemin" ];
        if( opts["both"] )     tasks = [ "clean:img", "clean:min", "clean:debug", "copy", "sass:min", "sass:debug", "imagemin" ];

        if( opts["wbug"] )      tasks = [ "clean:img", "clean:debug", "copy", "sass:debug", "watch:debug" ];
        if( opts["wmin"] )      tasks = [ "clean:img", "clean:min",   "copy", "sass:min",   "watch:min", "imagemin" ];
        if( opts["wboth"] )     tasks = [ "clean:img", "clean:min", "clean:debug", "copy", "sass:debug", "sass:min", "watch:both", "imagemin" ];

        grunt.task.run( tasks );


        // only write out list if no options passed
        if( tasks.length > 0 ) return;

        /**
         * Writes out a list of options for this project to the command-line.
         */

        grunt.log.header( "Hi there. What would you like to run?".yellow.bold );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --Debug".cyan );
        grunt.log.writeln( "Will compile to debug/expanded css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --min".cyan );
        grunt.log.writeln( "Will compile to minified css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --both".cyan );
        grunt.log.writeln( "Will compile to both debug & minified css.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.debug".cyan );
        grunt.log.writeln( "Will run ".grey + "--Debug".yellow + ", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.min".cyan );
        grunt.log.writeln( "Will run ".grey +"--min".yellow +", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "grunt --watch.both".cyan );
        grunt.log.writeln( "Will run ".grey + "--both".yellow + ", then watch.".grey );


        grunt.log.writeln( ""); // LINE BREAK


        grunt.log.writeln( "NB:".yellow );
        grunt.log.writeln( "Don't use more than 1 option.".yellow );
        grunt.log.writeln( "--debug".yellow + " is a reserved keyword, so using ".grey + "--Debug".yellow +" (Cap 'D') instead.".yellow );

    });
};
