
module.exports = function(grunt) {

    var fs = require('fs');
    
    // var "requirejsConfig" gets decalred in this file and used for dev as well grunt build
    eval(fs.readFileSync( '../../_dev/app/RJSConfig.js')+'');


    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-remove-logging');

    // grunt.registerTask('default', [ "clean", "requirejs:debug", "removelogging", "uglify" ] );
    grunt.registerTask('default', [ "clean", "requirejs" ].concat( grunt.option("bug") ? [] : ["removelogging", "uglify"]) );


    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json")

        ,vars: {
             root:       "../../"
            ,dev:       "<%= vars.root %>_dev/"
            ,dist:      "<%= vars.root %>_dist/"
            ,deploy:    "<%= vars.dist %>deploy/"
            ,dist_js_debug:    "<%= vars.deploy %>js/debug/"
            ,dist_js_min:      "<%= vars.deploy %>js/min/"
            ,banner_content:    '<%= pkg.name %> - <%= pkg.version %> - ' + grunt.template.today("yyyy-mm-dd") + ' - <%= pkg.author.name %>'
        }



        ,clean: {
            options: { force: true }
            ,debug: [ "<%= vars.dist_js_debug %>*" ]
            ,min:   [ "<%= vars.dist_js_min %>*" ]
        }


        ,requirejs: {
            debug: {
                options: {
                     baseUrl:     "./"
                    ,paths:       requirejsConfig.paths
                    ,shim:        requirejsConfig.shim
                    ,name:        "<%= vars.dev %>app/NGBootstrap.js"
                    ,exclude:
                        /**
                         * Excludes an array of modules, plus any that are defined in "requirejsConfig.versions".
                         * This allows us to automatically exclude any modules that have had a specific version requested,
                         * as these will be defined in the global config separately.
                         */
                        [].concat(requirejsConfig.versions.excludes)
                    ,inlineText:  true
                    ,optimize:    "none"
                    // ,out:         "<%= vars.dist_js_debug %>build-debug.js"
                    ,out: function( concat ) {

                        concat = concat.split("/_dev/app/pages/Gallery/img/singles/").join("/_dist/deploy/img/");
                        grunt.file.write( grunt.config.get("vars.dist_js_debug") + "build-debug.js", concat );
                    }
                    ,done:        debugOut
                }
            }
        }


        ,uglify: {
            options: {
                 banner: '/* <%= vars.banner_content %> */\n'
                ,preserveComments: "some"
                ,compress: {
                    global_defs: { "DEBUG": false }
                    ,dead_code: true
                    // ,drop_console: true // removes all console statements, but want to keep "console.warn", so using "removelogging"
                }
            },
            min: {
                files: [{ src: '<%= vars.dist_js_min %>build.js',
                        dest: '<%= vars.dist_js_min %>build.js' }]
            }
        }


        // make sure you only remove console methods from the debug version (before minification) or else it won't work
        ,removelogging: {
            dist: {
                options: {
                    replaceWith: 0 // replaces with 0 so that "if" statements have something to compare
                    ,methods: [ "log" ] // keeps all console statements except for "log"
                }
                ,src: '<%= vars.dist_js_debug %>build-debug.js'
                ,dest: '<%= vars.dist_js_min %>build.js'
            }
        }
    });



    function debugOut( done, output ) {
        
        /**
         * TODO: 
         * - Strip out html comments (mainly for AngularJS partials)
         * - Hook into git and grab build number
         */
        // grunt.file.write( grunt.config.get( "vars.dist_js_debug" ) + "build-debug.js", output );
        
        var duplicates = require('rjs-build-analysis').duplicates(output);
        
        if (duplicates.length > 0) {
          grunt.log.subhead('Duplicates found in requirejs build:');
          grunt.log.warn(duplicates);
          done(new Error('r.js built duplicate modules, please check the excludes option.'));
        }


        done();
    }
    
};
