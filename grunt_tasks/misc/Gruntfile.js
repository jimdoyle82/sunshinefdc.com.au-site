
module.exports = function(grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON("package.json")

        ,vars: {
            root:           "../../"
            ,dev:           "<%= vars.root %>_dev/"
            ,dist:          "<%= vars.root %>_dist/"
            ,deploy:        "<%= vars.dist %>deploy/"
            ,app:           "<%= vars.dev %>app/"
            ,deps:          "<%= vars.dev %>deps/"
            ,bowcomp:       "<%= vars.deps %>bower_components/"
            ,versioned:     "<%= vars.deps %>libs/versioned/"
            ,dist_img_singles: "<%= vars.dist %>img/singles-uncompressed/"
        }


        ,copy: {
            fonts: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.dev %>app/core/fonts/"
                ,src: [  "**/*" ]
                ,dest: "<%= vars.deploy %>fonts/"
            }

            // copies deps into _dist/deploy that DO already have minified versions
            ,libs: {
                expand: true
                ,flatten: true
                ,cwd: "<%= vars.deps %>"
                ,src: [ "libs/backgroundsize.min.htc"
                        ,"libs/array-every-polyfill/array-every-polyfill.min.js"
                        ,"libs/array-indexof-polyfill/array-indexof-polyfill.min.js"
                        ,"libs/classlist-polyfill/classlist.min.js"
                        ,"libs/eventlistener-polyfill/eventlistener.polyfill.min.js"
                        ,"libs/respondjs/respond.min.js"
                        ,"bower_components/html5shiv/dist/html5shiv-printshiv.js"
                        ,"bower_components/html5shiv/dist/html5shiv.js"
                    ]
                ,dest: "<%= vars.deploy %>libs/"
            }
            ,versioned: {
                expand: true
                ,flatten: false
                ,cwd: "<%= vars.deps %>libs/versioned/"
                ,src: ["**/*"]
                ,dest: "<%= vars.deploy %>libs/versioned/"
            }
            // copies all non-sprite images
            ,appimg: {
                 expand: true
                ,flatten: true
                ,cwd: "<%= vars.dev %>"
                ,src: [  "app/pages/*/img/singles/*.{png,gif,jpg}"
                        ,"app/directives/*/img/singles/*.{png,gif,jpg}" ]
                ,dest: "<%= vars.deploy %>img/"
            }

            ,depsimg: {
                 expand: true
                ,flatten: true
                ,cwd: "<%= vars.deps %>"
                ,src: [ "externals/ngui/*/_dev/img/singles/*.{png,gif,jpg}" ]
                ,dest: "<%= vars.deploy %>img/"
            }
        }


        ,clean: {
            options: { force: true }
            ,libs:      [ "<%= vars.deploy %>libs/*" ]
            ,homeSVGs:  [ "<%= vars.dist %>img/home/*" ]
            ,homeSVGs:  [ "<%= vars.dist %>img/core/*" ]
        }


        ,uglify: {
            options: {
                 preserveComments: "some"
                ,compress: {
                    global_defs: { "DEBUG": false }
                }
            },
            libs: {
                // minify deps that DON'T currently have minified versions
                files: [{    src: '<%= vars.deps %>libs/require.js'
                            ,dest: '<%= vars.deploy %>libs/require.min.js'
                        },/*{
                            src: '<%= vars.versioned %>text.v2.0.5.js'
                            ,dest: '<%= vars.deploy %>libs/text.min.v2.0.5.js'
                        },{
                            src: '<%= vars.versioned %>domready.v2.0.1.js'
                            ,dest: '<%= vars.deploy %>libs/domready.min.v2.0.5.js'
                        },{
                            src: '<%= vars.versioned %>lodash.v2.4.1.js'
                            ,dest: '<%= vars.deploy %>libs/lodash.v2.4.1.js'
                        },*/{
                            src: '<%= vars.deps %>libs/rafpolyfill.js'
                            ,dest: '<%= vars.deploy %>libs/rafpolyfill.min.js'
                        },{
                            src: '<%= vars.bowcomp %>json2/json2.js'
                            ,dest: '<%= vars.deploy %>libs/json2.min.js'
                        }]
            }
        }


        ,imagemin: {

            // sprites already get compressed in "sass" task

            singles: {
                options: {
                    pngquant: true
                    ,optimizationLevel: 3
                }
                ,files: [{
                     expand: true
                    ,flatten: true
                    ,cwd: '<%= vars.dist_img_singles %>'
                    ,src: ['*.{png,jpg,gif}']
                    ,dest: '<%= vars.deploy %>img/'
                }]
            }
        }

    });
        

    grunt.loadNpmTasks("grunt-contrib-copy");
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask("default", [ "clean", "copy", "uglify", "imagemin" ] );

};
