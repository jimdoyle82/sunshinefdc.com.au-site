# Directive setup



## DefMdl.js (Default Model)
This holds the default model for this directive and an AMD Module.

In most cases you will want to replace it with an object from the parent scope when defining it in your markup. First level of object keys must be identical and is enforced in ES5 browsers with "ValidateModel" service. This is done so that you have a clear idea of what to expect when examining and debugging the directive.

See below, where '<%= baseName %>' is from the parent scope's Model.

```html
<div <%= baseNameSC %> one-way-model="Mdl.<%= baseNameCC %>"></div>
```

You may also call the AMD Module without any args when you need to fetch the directive's base name. Useful when defining your directive. See below.

```js
define(["./DefMdl"], function(Mdl) {
     angular.module( Mdl() + "Module", []).directive( Mdl(), function() {
       // directive contents here
     });
});
```

When passed a 'scope' 1st property, 'DefMdl.js' returns a non-extensible copy of it's 'Model' variable (ES5 only), which gets swapped-out with the 'inhMdl' (inherited model) if it exists.

The 2nd arg 'oneWay' is a boolean and is needed to handle the differences in syntax between properties passed into the isolated scope of the directive, which use '&' for one-way and '=' for two-way bindings. Omitting it assumes two-way binding.



## template.html
This contains the markup for the directive. Because the model gets attached as 'scope.Mdl' in the 'ValidateModel' service and is also defined in 'Ctrl.js' as
```js
var Mdl = mdl(scope,true);
```
you can use it in both your markup and controller simply as 'Mdl.myValue', knowing that it's 1st level of keys is non-extensible.



## Ctrl.js
This is the controller for the directive. 'Mdl' is defined at the top, like so.
```js
var Mdl = mdl(scope,true);
```
This is useful for any data that you want to inherit from the parent scope and easily swap-out, keeping any exsting bindings and object keys. Any values that are 'private' to this directive can still be added to the '$scope' as usual, as below.
```js
$scope.myValue = 10;
```
