
define([ "./DefMdl" ], function( DefMdl ) {
    "use strict";

    return [ '$scope', '$element', 'selectorUtil', function ( $scope, $element, su ) {
        var Mdl = DefMdl( $scope, true ); // A non-extensible $scope.Model.

        // example of how to use "SelectorUtil -> find"
        var h1 = su.find(".heading", $element);

        console.log( "<%= baseName %> Directive init", Mdl, h1 );
    }];
});
