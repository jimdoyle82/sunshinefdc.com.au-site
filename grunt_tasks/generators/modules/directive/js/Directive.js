
define([ "./Ctrl", "./DefMdl", "text!D_<%= baseNameUC %>/template.html"], function( Ctrl, Mdl, template ) {

	return angular.module( Mdl() + "Module", [] ).directive( Mdl(), function() {
	        return {
				 controller: Ctrl
				,template: template
				,replace: true
				,scope: {
					inhMdl: "&mdl" // inherited model - one-way binding "&"
				}
	        }
		});
});