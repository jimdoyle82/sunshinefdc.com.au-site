
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    /**
     * This holds the default model and base name for this directive. 
     * See README.md for more details.
     */

	var BASE_NAME = "<%= baseNameCC %>"; // must be camelCase
    
    var Mdl = {
            title:"<%= baseName %> Directive"
        };

    // If no args, just return BASE_NAME, otherwise returns a non-extensible copy of Mdl (ES5 only)
    return function( scope, oneWay ) {
        if( !scope ) return BASE_NAME;
        return ValMdlServ.$get().directiveModel( Mdl, BASE_NAME, scope, oneWay );
    }
});