
define(['angular-mocks', "CORE/js/App", "<%= amddef %>/js/Ctrl", "<%= amddef %>/js/Mdl"]
    ,function(ngMocks, App, Ctrl, Mdl) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('<%= baseNameSC %> page tests', function() {

        var $scope;

        // loads the app's main module
        beforeEach(module( App.name ));
        
        // makes sure the $scope for the page is available
        beforeEach(inject(function($controller, $rootScope) {
            $controller( Ctrl, { $scope: $rootScope });
            $scope = $rootScope;
        }));


        it('should check that <%= baseNameSC %> page Ctrl has a Mdl attached to it', function() {
            expect( $scope.Mdl ).toBeDefined();
        });

        it('should check that <%= baseNameSC %> page Mdl returns an object', function() {
            expect( Mdl($scope) ).toEqual( jasmine.any(Object) );
        });
    });

});
