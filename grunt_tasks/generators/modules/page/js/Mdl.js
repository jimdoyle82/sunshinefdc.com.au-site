
define(["EX_SERVICES/ValidateModel"], function( ValMdlServ ) {
    "use strict";

    var Mdl = {
		
    };

    
    // Returns a non-extensible copy of Mdl (ES5 only)
    return function( scope ) {
    	return ValMdlServ.$get().pageModel( Mdl, scope );
    }
});