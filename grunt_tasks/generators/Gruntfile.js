
module.exports = function(grunt) {


	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json")


		,vars: {
			baseName: "pg nav" // Title Case (with spaces)
			,baseNameTC: "PgNav" // TitleCase (no spaces)
			,baseNameSC: "pg-nav" // snake-case
			,baseNameCC: "pgNav" // camelCase
			,baseNameUC: "PG_NAV" // UPPER_CASE
			,baseNameLC: "pgnav" // lowercase (no spaces)
			,root:   "../../"
            ,dev:   "<%= vars.root %>_dev/"
			,pages: "<%= vars.dev %>app/pages/"
			,directives: "<%= vars.dev %>app/directives/"
			,specs: "<%= vars.root %>test/specs/"
		}

        ,copy: {
            page: {
                expand: true
                ,cwd: "./modules/page/"
                ,src: [  "**" ]
                ,dest: "<%= vars.pages %>/<%= vars.baseNameTC %>"
                ,filter: function( content ) {
                	// excludes test dir
                	if( content.split("\\").join("/").indexOf("page/test") != -1 ) return false;
                	return true;
                }
            }

            ,directive: {
                expand: true
                ,cwd: "./modules/directive/"
                ,src: [  "**" ]
                ,dest: "<%= vars.directives %>/<%= vars.baseNameTC %>"
                ,filter: function( content ) {
                	// excludes test dir
                	if( content.split("\\").join("/").indexOf("page/test") != -1 ) return false;
                	return true;
                }
            }

            ,spec: {
            	src: "./modules/page/test/CtrlSpec.js"
                ,dest: "<%= vars.specs %>Pg<%= vars.baseNameTC %>CtrlSpec.js" 
            }
        }
	});


	grunt.registerTask("default", function() {

		grunt.log.header( "What shall we generate?".yellow.bold );


        grunt.log.writeln(""); // LINE BREAK


        grunt.log.writeln( 'grunt page --name="my ludicous page"'.cyan);
        // grunt.log.writeln("This will create a new page in your app. Has a mandatory argument, which must \nbe all lowercase with no spaces or special characters.".grey + " Example ".grey + "grunt page:home".cyan );
        grunt.log.writeln("This will create a new page in your app. Has a mandatory grunt option 'name', which must \nbe all lowercase ".grey + "WITH".cyan + " spaces, but ".grey + "NO".cyan + " special characters.".grey );


        grunt.log.writeln(""); // LINE BREAK


        grunt.log.writeln( 'grunt directive --name="my amazing directive"'.cyan);
        grunt.log.writeln("This will create a new directive in your app. Has a mandatory grunt option 'name', which must \nbe all lowercase ".grey + "WITH".cyan + " spaces, but ".grey + "NO".cyan + " special characters.".grey );
	});



	grunt.registerTask("page", function() {

		var name = grunt.option("name");

		if( !name ) {
			grunt.log.error( "Error: You must specify a name!" );
			return;
		}

		// name = name.toLowerCase().split(" ").join("");
		// grunt.config.set( "vars.baseName", name );

		name = name.toLowerCase();

		grunt.config.set( "vars.baseName", name.toCamelCase(true, true) );
		grunt.config.set( "vars.baseNameSC", name.split(" ").join("-") );
		grunt.config.set( "vars.baseNameCC", name.toCamelCase() );
		grunt.config.set( "vars.baseNameTC", name.toCamelCase(true) );
		grunt.config.set( "vars.baseNameUC", name.toUpperCase().split(" ").join("_") );
		grunt.config.set( "vars.baseNameLC", name.split(" ").join("") );

		// console.log( grunt.config.get( "vars.baseNameTC") );
		// return
		// grunt.task.run( ["add-page-spec"] );
		grunt.task.run( ["copy:page", "parse-page", "copy:spec", "add-page-spec"] );
	});




	grunt.registerTask("directive", function() {

		var name = grunt.option("name");

		if( !name ) {
			grunt.log.error( "Error: You must specify a name!" );
			return;
		}

		name = name.toLowerCase();

		grunt.config.set( "vars.baseName", name.toCamelCase(true, true) );
		grunt.config.set( "vars.baseNameSC", name.split(" ").join("-") );
		grunt.config.set( "vars.baseNameCC", name.toCamelCase() );
		grunt.config.set( "vars.baseNameTC", name.toCamelCase(true) );
		grunt.config.set( "vars.baseNameUC", name.toUpperCase().split(" ").join("_") );
		grunt.config.set( "vars.baseNameLC", name.split(" ").join("") );

		grunt.task.run( ["copy:directive", "parse-directive"] );//, "copy:spec", "add-page-spec:" + name] );
	});




	grunt.registerTask("parse-page", "Creates a new page in your app", function() {

		var baseName = grunt.config.get("vars.baseName")
			,baseNameTC = grunt.config.get("vars.baseNameTC")
			,baseNameSC = grunt.config.get("vars.baseNameSC")
			,baseNameCC = grunt.config.get("vars.baseNameCC")
			,baseNameLC = grunt.config.get("vars.baseNameLC")
			,baseNameUC = grunt.config.get("vars.baseNameUC");
		// name = name.toLowerCase().split(" ").join("");
		// grunt.config.set( "vars.baseName", name );

		var pageDir = grunt.config.get("vars.pages") + baseNameTC;

		// if doesn't exist, creates it and runs the task again
		if( !grunt.file.exists( pageDir ) ) {
			grunt.task.run([ "copy:page", "parse-page" ]);
			return;
		}


		var names = ['baseName', 'baseNameTC', 'baseNameSC', 'baseNameCC', 'baseNameUC', 'baseNameLC']
			,vals = [ baseName,   baseNameTC,   baseNameSC,   baseNameCC,   baseNameUC,   baseNameLC ];

		// parse template files for grunt vars
		parseGruntVar( grunt.config.get("vars.pages") + baseNameTC + "/template.html", names, vals );
		parseGruntVar( grunt.config.get("vars.pages") + baseNameTC + "/scss/_main.scss", names, vals );
		

		// parse template for grunt vars
		// parseGruntVar( grunt.config.get("vars.pages") + baseNameLC+"/template.html", 'name', baseNameLC );

		// parse scss for grunt vars
		// parseGruntVar( grunt.config.get("vars.pages") + baseNameLC+"/scss/_main.scss", 'name', baseNameLC );


		// add new page to RJSConfig.js
		insertBlock( "{{GENERATOR_INSERT_PAGE_DEFS}}", "app/RJSConfig.js", "  ",
			function() {
				return ',PG_'+ baseNameUC +': dev()+"app/pages/'+ baseNameTC +'"';
			});

		var appJS = "app/core/js/App.js"
			,appSCSS = "app/core/scss/app.scss";

		// add router imports
		insertBlock( "{{GENERATOR_INSERT_PAGE_IMPORTS}}", appJS, "        ",
			function() {
				return ',"PG_'+ baseNameUC +'/js/Ctrl", "text!PG_'+ baseNameUC +'/template.html"';
			});


		// add router import args
		insertBlock( "{{GENERATOR_INSERT_PAGE_ARGS}}", appJS, "        ",
			function() {
				return ','+baseNameTC+'Ctrl, '+baseNameTC+'Partial';
			});

		// add router configs
		insertBlock( "{{GENERATOR_INSERT_PAGE_CONF}}", appJS, "        ",
			function() {
				return '$routeProvider.when( "/'+baseNameLC+'", { template: '+baseNameTC+'Partial, controller: '+baseNameTC+'Ctrl });';
			});

		// add page styles to app
		insertBlock( "{{GENERATOR_INSERT_PAGE_STYLES}}", appSCSS, "",
			function() {
				return '@import "../../pages/'+baseNameTC+'/scss/main";';
			});


		grunt.log.writeln( "New page has been added, called '".yellow.bold + baseNameTC.cyan.bold + "' and a definition added to RequireJSConfig.js. Please check your routes in '_dev/app/core/App.js'.".yellow.bold );
	});


	grunt.registerTask("parse-directive", "Creates a new directive in your app", function() {

		var baseName = grunt.config.get("vars.baseName")
			,baseNameTC = grunt.config.get("vars.baseNameTC")
			,baseNameSC = grunt.config.get("vars.baseNameSC")
			,baseNameCC = grunt.config.get("vars.baseNameCC")
			,baseNameLC = grunt.config.get("vars.baseNameLC")
			,baseNameUC = grunt.config.get("vars.baseNameUC");

		// if doesn't exist, creates it and runs the task again
		if( !grunt.file.exists( grunt.config.get("vars.directives") + baseNameTC ) ) {
			grunt.task.run([ "directive" ]);
			return;
		}

		var names = ['baseName', 'baseNameTC', 'baseNameSC', 'baseNameCC', 'baseNameUC', 'baseNameLC']
			,vals = [ baseName,   baseNameTC,   baseNameSC,   baseNameCC,   baseNameUC,   baseNameLC ];

		// parse template files for grunt vars
		parseGruntVar( grunt.config.get("vars.directives") + baseNameTC + "/template.html", names, vals );
		parseGruntVar( grunt.config.get("vars.directives") + baseNameTC + "/README.md", names, vals );
		parseGruntVar( grunt.config.get("vars.directives") + baseNameTC + "/js/Ctrl.js", names, vals );
		parseGruntVar( grunt.config.get("vars.directives") + baseNameTC + "/js/DefMdl.js", names, vals );
		parseGruntVar( grunt.config.get("vars.directives") + baseNameTC + "/js/Directive.js", names, vals );

		// no scss for app directives

		// add new page to RJSConfig.js
		insertBlock( "{{GENERATOR_INSERT_DIRECTIVE_DEFS}}", "app/RJSConfig.js", "  ",
			function() {
				return ',D_'+ baseNameUC +': dev()+"app/directives/'+ baseNameTC +'"';
			});


		// add new page to RJSConfig.js
		insertBlock( "{{GENERATOR_INSERT_DIRECTIVE_DEFS}}", "app/core/js/Directives.js", "  ",
			function() {
				return ',"D_'+ baseNameUC + '/js/Directive"';
			});

		grunt.log.writeln( "New directive has been added, called '".yellow.bold + baseNameSC.cyan.bold + "' and a definition added to RequireJSConfig.js.".yellow.bold );
	});



	grunt.registerTask("add-page-spec", "Creates a karma test spec for the page specified", function() {

		var baseNameTC = grunt.config.get("vars.baseNameTC")
			,baseNameSC = grunt.config.get("vars.baseNameSC")
			,baseNameUC = grunt.config.get("vars.baseNameUC");

		var specFile = grunt.config.get("vars.specs") + baseNameTC +"CtrlSpec.js";

		// deletes the original if there
		// if( grunt.option("force") && grunt.file.exists( specFile ) )
			// grunt.file.delete( specFile, { force: true } )


		// if doesn't exist, creates it and runs the task again
		if( !grunt.file.exists( specFile ) ) {
			grunt.task.run([ "copy:spec", "add-page-spec" ]);
			return;
		}

		var names = ['baseNameSC', 'amddef']
			,vals = [baseNameSC, 'PG_'+ baseNameUC ];

		parseGruntVar( specFile, names, vals );

		grunt.log.writeln( "New page spec has been added, called '".yellow.bold + (baseNameTC + "CtrlSpec.js").cyan.bold + "', in 'test/specs/'.".yellow.bold );
	});
	

	grunt.loadNpmTasks("grunt-contrib-copy");


	/**
	 * Helper methods
	 */

	function parseGruntVar( filePath, names, vals ) {

		var _ = require("lodash-node")
			,str = grunt.file.read( filePath )
			,data = {}
			,nameArr = []
			,valArr = [];

		if( typeof names == "string" )	nameArr.push( names );
		else							nameArr = names;

		if( typeof vals == "string" )	valArr.push( vals );
		else							valArr = vals;

		_.forEach( nameArr, function( varName, i ) {
			data[ varName ] = valArr[i];
		});

		str = grunt.template.process( str, { data: data });

		grunt.file.write( filePath, str );
	}

	function insertBlock( separater, filePath, indent, callback ) {

		filePath = grunt.config("vars.dev")+ filePath;

	 	var data = grunt.file.read( filePath )
			,splitArr = data.split( "//" + separater )
			,str = callback();

	 	grunt.file.write( filePath, splitArr[0] + str + "\n"+indent+"//" + separater + splitArr[1] );
	}


	String.prototype.toCamelCase = function(makeTitleCase, includeSpaces){
	    return this.split(/\s+/).map(function(word, index){
	        word = word.toLowerCase();
	        if(index !== 0 || makeTitleCase === true){
	            return word.split('').map(function(char, i){
	                        if(i===0){
	                            return char.toUpperCase();
	                        }
	                        return char;
	                        
	                    }).join('')
	        }
	        return word;
	        
	    }).join(includeSpaces === true ? ' ' : '');
	};
}