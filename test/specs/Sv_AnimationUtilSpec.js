
define(['angular-mocks', "CORE/js/Constants", "CORE/js/Services" ]
    ,function(ngMocks, Constants, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('animationUtil service tests', function() {

        var service;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );
        
        beforeEach( inject(function( animationUtil ) {
            service = animationUtil;
        }) );

        it('should check AnimationUtil exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });


        it('should check that events are added to the element passed and the callback listener works', function() {

            var EVT_NAME = "AnimationStart"
                ,events = [];

            var el = document.createElement("div");

            var pfxArr = service.prefixedEvent(el, EVT_NAME, function(evt) {

                // console.log( evt );

                // checks that the events fired match the pfxArr returned array values
                expect( evt.type ).toEqual( events[0] + EVT_NAME );
                events.splice( 0, 1 );
            });

            for( var i=0; i<pfxArr.length; i++ ) {

                // keeps a reference to the prefixes for the callback
                events.push( pfxArr[i] );

                // dispatches the event manually
                var evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
                evt.initCustomEvent( pfxArr[i] + EVT_NAME , false, false, null);
                el.dispatchEvent(evt);
            }
        });
    });

});
