
define(['angular-mocks', "CORE/js/App", "PG_HOME/js/Ctrl", "PG_GALLERY/js/Ctrl", "PG_CONTACT/js/Ctrl", "PG_NEWSLETTERS/js/Ctrl" ]
    ,function(ngMocks, App, HomeCtrl, GalleryCtrl, ContactCtrl, NewslettersCtrl ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('home page tests', function() {

        var homeScope, galleryScope, contactScope, newslettersScope;

        // loads the app's main module
        beforeEach(module( App.name ));
        
        beforeEach(inject(function($controller, $rootScope) {
            homeScope = $rootScope.$new();
            galleryScope = $rootScope.$new();
            contactScope = $rootScope.$new();
            newslettersScope = $rootScope.$new();

            $controller( HomeCtrl, { $scope: homeScope });
            $controller( GalleryCtrl, { $scope: galleryScope });
            $controller( ContactCtrl, { $scope: contactScope });
            $controller( NewslettersCtrl, { $scope: newslettersScope });
        }));


        it('should check that each page Ctrl has a Mdl attached to it', function() {
            // console.log( homeScope.Mdl );
            // console.log( galleryScope.Mdl );

            expect( homeScope.Mdl ).toEqual( jasmine.any(Object) );
            expect( galleryScope.Mdl ).toEqual( jasmine.any(Object) );
            expect( contactScope.Mdl ).toEqual( jasmine.any(Object) );
            expect( newslettersScope.Mdl ).toEqual( jasmine.any(Object) );
        });

        it('should check that each page Ctrl has a close handler called "closePage".', function() {
            expect( homeScope.closePage ).toEqual( jasmine.any(Function) );
            expect( galleryScope.closePage ).toEqual( jasmine.any(Function) );
            expect( contactScope.closePage ).toEqual( jasmine.any(Function) );
            expect( newslettersScope.closePage ).toEqual( jasmine.any(Function) );
        });
    });

});
