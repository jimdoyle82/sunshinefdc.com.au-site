
define(['angular-mocks', "CORE/js/Constants", "EX_SERVICES/ModuleNameFetcher", "CORE/js/Services" ]
    ,function(ngMocks, Constants, ServiceExample, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('moduleNameFetcher service tests', function() {

        var service;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );
        
        beforeEach( inject(function( moduleNameFetcher ) {
            service = moduleNameFetcher;
        }));
        

        it('should check ModuleNameFetcher exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });


        it('should check that getModuleNames() returns an array of strings for use as module names', function() {

            expect( service.getModuleNames ).toEqual( jasmine.any(Function) );
            
            var moduleNames = service.getModuleNames( [ServiceExample], true );
            expect( moduleNames ).toEqual( jasmine.any(Array) );
            
            expect( moduleNames.length ).toEqual(1);

            expect( moduleNames[0] ).toEqual( jasmine.any(String) );
        });


        it('should check that getServiceName() returns a string', function() {
            expect( service.getServiceName ).toEqual( jasmine.any(Function) );

            var serviceName = service.getServiceName( ServiceExample );
            expect( serviceName ).toEqual( jasmine.any(String) );
        });


        it('should check that isService() returns a true value', function() {
            expect( service.isService ).toEqual( jasmine.any(Function) );

            var isService = service.isService( ServiceExample );
            expect( isService ).toEqual( true );
        });
        
    });

});
