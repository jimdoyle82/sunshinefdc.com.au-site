
define(['angular-mocks', 'lodash', "CORE/js/Constants", "PG_HOME/js/Ctrl", "CORE/js/Services" ]
    ,function(ngMocks, _, Constants, HomePageCtrl, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('ValidateModel service tests', function() {

        var service, scope;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );

        // makes sure the $scope for the page is available
        beforeEach(inject(function($controller, $rootScope) {
            $controller( HomePageCtrl, { $scope: $rootScope });
            scope = $rootScope.$new();
        }));

        
        beforeEach( inject(function( validateModel ) {
            service = validateModel;
        }) );

        it('should check ValidateModel exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });

        it('should check that the scope has the value Mdl and that it is non-extensible', function() {

            // here we add a new "Mdl" obect to the scope using "pageModel" method and make sure it is defined
            service.pageModel( { test: "home test" }, scope );
            expect( scope.Mdl ).toBeDefined();

            // next we test that the new value "addedParam" does NOT get added to the Mdl, as the Mdl is non-extensible
            scope.Mdl.addedParam = "home test";
            expect( scope.Mdl.addedParam ).not.toBeDefined();
        });


    });

});
