
define(['angular', 'angular-mocks', "CORE/js/Constants", "CORE/js/AppCtrl", "CORE/js/Services" ]
    ,function(ng, ngMocks, Constants, AppCtrl, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('AppModelSetter service tests', function() {

        var service, scope;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );

        // makes sure the $scope for the page is available
        beforeEach(inject(function($controller, $rootScope) {
            $controller( AppCtrl, { $scope: $rootScope, $element: ng.element('<div></div>') });
            scope = $rootScope.$new();
        }));

        
        beforeEach( inject(function( appModelSetter ) {
            service = appModelSetter;
        }));

        it('should check AppModelSetter exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });

        it('should check that the scope has the value Mdl and that it is non-extensible', function() {

            var testMdl = {
                test: "test mdl"
            }
            service.setMdl( scope, testMdl );
            expect( scope.Mdl ).toBeDefined();

            // next we test that the new value "addedParam" does NOT get added to the Mdl, as the Mdl is non-extensible
            scope.Mdl.addedParam = "home test";
            expect( scope.Mdl.addedParam ).not.toBeDefined();
        });


        it('should check that method "setMdl" adds a value to the specified property.', function() {

            // defines the garden flip model
            var testMdl = {}
            testMdl[ service.types.GFLIP ] = {
                paused: true
            }

            // sets the app model
            service.setMdl( scope, testMdl );
            
            // sets a value on the garden flip model
            service.setVal( service.types.GFLIP, "paused", false );

            // checks the "paused" value has changed
            expect( testMdl[ service.types.GFLIP ].paused ).toEqual( false );
        });


        it('should check that method "setWatcher" triggers the callback when value changes.', function() {

            /**
             * This test is incomplete and may not be possible, as the $watch directive doesn't seem to trigger.
             */

            // defines the garden flip model
            var testMdl = {}
            testMdl[ service.types.GFLIP ] = {
                paused: true
            }

            // sets the app model
            service.setMdl( scope, testMdl );
            
            service.setWatcher( service.types.GFLIP, "paused", function( val ) {

                console.log( val );
            }, false );

            setTimeout( function() {
                // sets a value on the garden flip model
                service.setVal( service.types.GFLIP, "paused", false );
            }, 10);

        });
        
    });

});
