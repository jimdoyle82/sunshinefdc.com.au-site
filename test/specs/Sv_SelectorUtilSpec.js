
define([ 'angular', 'angular-mocks', "CORE/js/Constants", "PG_HOME/js/Ctrl", "CORE/js/Services" ]
    ,function(ng, ngMocks, Constants, HomePageCtrl, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('SelectorUtil service tests', function() {

        var service, scope;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );

        // makes sure the $scope for the page is available
        beforeEach(inject(function($controller, $rootScope) {
            $controller( HomePageCtrl, { $scope: $rootScope });
            scope = $rootScope.$new();
        }));

        
        beforeEach( inject(function( selectorUtil ) {
            service = selectorUtil;
        }) );

        it('should check SelectorUtil exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });

        it('should check that the method "find" returns a valid element under all scenarios', function() {

            // first create a new element with some child elements to search for
            var newEl = document.createElement('div')
                ,newElInner1 = document.createElement('div')
                ,newElInner2 = document.createElement('div');

            newElInner1.setAttribute('class', 'test-class');
            newElInner2.setAttribute('class', 'test-class');
            
            newEl.appendChild( newElInner1 );
            newEl.appendChild( newElInner2 );

            
            var allSel = false
                ,noNGWrap = true
                ,el;

            el = service.find( '.test-class', newEl, allSel, noNGWrap )
            
            // 'allSel' is false, therefore only expect the first selector to be returned
            expect( el ).toBeDefined();

            // result should be an HTML Dom element, which doesn't have a length property
            expect( el.length ).not.toBeDefined();


            // 'noNGWrap' is false, which means we expect an jQuery Lite element to be returned, which has a length property
            noNGWrap = false;
            el = service.find( '.test-class', newEl, allSel, noNGWrap )
            expect( el ).toBeDefined();
            expect( el.length ).toEqual( 1 );


            // 'allSel' is true, which means we expect all elements with the included class name to be returned
            allSel = true;
            el = service.find( '.test-class', newEl, allSel, noNGWrap )
            expect( el ).toBeDefined();
            expect( el.length ).toEqual( 2 );
        });
        
    });

});
