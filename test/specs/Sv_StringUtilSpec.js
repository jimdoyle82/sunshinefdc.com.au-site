
define([ 'angular', 'angular-mocks', "CORE/js/Constants", "PG_HOME/js/Ctrl", "CORE/js/Services" ]
    ,function(ng, ngMocks, Constants, HomePageCtrl, Services ) {
    // angular-mocks allows us to use 'inject' and 'module'

    describe('StringUtil service tests', function() {

        var service, scope;

        beforeEach( module( Constants.baseName + "ServicesModule" ) );

        // makes sure the $scope for the page is available
        beforeEach(inject(function($controller, $rootScope) {
            $controller( HomePageCtrl, { $scope: $rootScope });
            scope = $rootScope.$new();
        }));

        
        beforeEach( inject(function( stringUtil ) {
            service = stringUtil;
        }));

        
        it('should check StringUtil exists', function() {
            expect(service.name).toEqual( jasmine.any(String) );

            // loops through all services added the sfdcServicesModule and checks ours is there
            var arr = [];
            _.forEach( Services._invokeQueue, function(item) {
                arr.push( item[2][0] );
            });;

            expect( arr.indexOf( service.name ) ).not.toEqual(-1);
        });


        it('should check that the method "getSeparateChars" returns markup with individual span elements around each character', function() {

            // first create a new element with some child elements to search for
            var el = document.createElement('div');
            el.innerHTML = "Test";

            var resultEl;
            
            // Standard HTML element
            resultEl = service.getSeparateChars( el );
            expect( resultEl.indexOf('<span class="ch1') ).toEqual( 0 );

            // jQuery Lite element
            resultEl = service.getSeparateChars( ng.element( el ) );
            expect( resultEl.indexOf('<span class="ch1') ).toEqual( 0 );


            // add a nested element
            var elInner = document.createElement('div');
            elInner.setAttribute("class", "test-class")
            elInner.innerHTML = "Test Inner";
            el.appendChild( elInner );

            // Test nested standard HTML element, with selector
            resultEl = service.getSeparateChars( el, ".test-class" );
            expect( resultEl.indexOf('<span class="ch1') ).toEqual( 0 );

            // Test nested jQuery Lite element, with selector
            resultEl = service.getSeparateChars( ng.element(el), ".test-class" );
            expect( resultEl.indexOf('<span class="ch1') ).toEqual( 0 );
        });


        it('should check that method "toSentenceCase" returns sentence case string', function() {

            var str = "testSicle";
            var resultStr = service.toSentenceCase( str );
            expect( resultStr.indexOf("T") ).toEqual( 0 );
            expect( resultStr.indexOf("S") ).toEqual( -1 );
        });


        it('should check that method "toCamelCase" returns camel case string', function() {

            var str = "Test string";
            var resultStr = service.toCamelCase( str );
            
            expect( resultStr.indexOf("t") ).toEqual( 0 );
            expect( resultStr.indexOf("S") ).toEqual( 4 );
            expect( resultStr.indexOf(" ") ).toEqual( -1 );
        });
        
    });

});
