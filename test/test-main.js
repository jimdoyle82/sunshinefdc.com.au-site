
/**
 * Docs on setting up karma with requirejs:
 * http://karma-runner.github.io/0.10/plus/requirejs.html
 */


// makes sure only grabs files ending in Spec.js
var tests = [];
for (var file in window.__karma__.files) {
  if (window.__karma__.files.hasOwnProperty(file)) {
    if (/Spec\.js$/.test(file)) {
      tests.push(file);
    }
  }
}

// add some test specific paths to the config
// requirejsConfig.paths['angular-mocks'] = "deps/bower_components/angular-mocks/angular-mocks";
requirejsConfig.paths['angular-mocks'] = "deps/libs/versioned/angular.v1.2.16/angular-mocks";
requirejsConfig.shim['angular-mocks'] = { deps : ["angular" ]};

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/_dev'
    
    // Use same config as our app
    ,paths: requirejsConfig.paths
    ,shim: requirejsConfig.shim

    // ask Require.js to load these files (all our tests)
    ,deps: tests

    // start test run, once Require.js is done
    ,callback: window.__karma__.start
});