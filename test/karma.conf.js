// Karma configuration

module.exports = function(config) {
  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '../',


    // frameworks to use
    frameworks: ['jasmine', 'requirejs'],


    // list of files / patterns to load in the browser
    files: [
      '_dev/app/RJSConfig.js' // allows us to use the app's RequireJS Config for tests as well. Must come before 'test-main.js'.
      ,'test/test-main.js'
      ,{pattern: '_dev/deps/**/*.js', included: false}
      ,{pattern: '_dev/app/**/*.js', included: false}
      ,{pattern: 'test/specs/**/*Spec.js', included: false}
      // Needed for Angular .html templates. Stops html2js preprocessor. See https://github.com/karma-runner/karma/issues/740
      ,{pattern: '_dev/app/pages/**/*.html', included: false} 
      ,{pattern: '_dev/app/directives/**/*.html', included: false} 
      ,{pattern: '_dev/deps/externals/ngui/**/*.html', included: false}
    ],


    // list of files to exclude
    exclude: [
      // '_dev/app/RJSConfig.js',
      '_dev/app/NGBootstrap.js'
    ],


    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: ['PhantomJS'],//, 'Chrome', 'Firefox'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true,


     // Needed for Angular .html templates. Stops html2js preprocessor. See https://github.com/karma-runner/karma/issues/740
    preprocessors: {}
  });
};
